#include "keysplugin.h"
#include "kindlekeys.h"

KeysPlugin::KeysPlugin(QObject* parent) 
    : QKbdDriverPlugin(parent)
{
}

QStringList KeysPlugin::keys() const
{
    QStringList list;
    list << QLatin1String("KindleKeys");
    return list;
}

QWSKeyboardHandler* KeysPlugin::create(const QString & key, const QString & device)
{
    if (key.toLower() == QLatin1String("kindlekeys"))
    {
        return new KindleKeys(key, device);
    }

    return NULL;
}

Q_EXPORT_PLUGIN2(KindleKeys, KeysPlugin)

