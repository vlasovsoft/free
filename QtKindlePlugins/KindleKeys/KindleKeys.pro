TARGET = KindleKeys
TEMPLATE = lib
CONFIG += plugin
SOURCES += keysplugin.cpp kindlekeys.cpp
HEADERS += keysplugin.h kindlekeys.h
INCLUDEPATH += $$PWD/../../SuspendManager $$PWD/../../common
DEPENDPATH  += $$PWD/../../SuspendManager
LIBS        += -L../../SuspendManager -lSuspendManager
