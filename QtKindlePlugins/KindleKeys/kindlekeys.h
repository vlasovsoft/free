#ifndef KINDLEKEYS_H
#define KINDLEKEYS_H

#include <sys/socket.h>
#include <linux/netlink.h>

#include <QWSKeyboardHandler>
#include <QSocketNotifier>

#include "KindleDevice.h"

class KindleKeys : public QObject, public QWSKeyboardHandler
{
    Q_OBJECT

public:
    KindleKeys(const QString & driver = QString(), const QString & device = QString(), QObject* parent = 0);
    virtual ~KindleKeys();

private slots:
    void activityKoa2Keys(int);
    void activityKvKeys(int);
    void activityNetLink(int);

private:
    KindleDevice dev;

    int fdKoa2Keys;
    QSocketNotifier* snKoa2Keys;

    int fdKvKeys;
    QSocketNotifier* snKvKeys;

    int fdNetLink;
    QSocketNotifier* snNetLink;
    sockaddr_nl src_addr, dest_addr;
    nlmsghdr* nlh;
    msghdr msg;
    iovec iov;

    bool isDebug;
    bool isInputCaptured;
    bool useSuspendManager;

private:
    void capture_input();
    void release_input();
    bool isKoa2Rotated();
};

#endif // KINDLEKEYS_H
