#ifndef KEYSPLUGIN_H
#define KEYSPLUGIN_H

#include <QKbdDriverPlugin>
#include <QWSKeyboardHandler>

class KeysPlugin : public QKbdDriverPlugin 
{
    Q_OBJECT

public:
    KeysPlugin(QObject* parent = 0);

    QStringList keys() const;
    QWSKeyboardHandler* create(const QString & key, const QString & device);
};

#endif // KEYSPLUGIN_H

