CONFIG += plugin
TARGET = KindleTSv2
TEMPLATE = lib
SOURCES += tsplugin.cpp kindlets.cpp
HEADERS += tsplugin.h kindlets.h
INCLUDEPATH += $$PWD/../../SuspendManager $$PWD/../../common
DEPENDPATH  += $$PWD/../../SuspendManager
LIBS        += -L../../SuspendManager -lSuspendManager
