#ifndef KINDLE_FB_H
#define KINDLE_FB_H

#include <QTimer>
#include <QLinuxFbScreen>

#include "KindleDevice.h"
#include "mxcfb.h"

class QPoint;
class QRegion;
class QRect;

QT_BEGIN_NAMESPACE

QT_MODULE(Gui)

class KindleFb : public QTimer, public QLinuxFbScreen
{
    Q_OBJECT

public:
    KindleFb();
    virtual ~KindleFb();
    virtual bool connect(const QString& displaySpec);
    virtual void setDirty ( const QRect& rect );

private slots:
    void update();

private:
    KindleDevice dev;
    int fb;
    Mxcfb einkFb;
    bool isDebug;
    QRect updateRect;
};

QT_END_NAMESPACE

#endif // KINDLE_FB_H
