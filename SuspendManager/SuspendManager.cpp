#if defined(POCKETBOOK) || defined(OBREEY)
#include <inkview.h>
#endif

#include "SuspendManager.h"

SuspendManager* SuspendManager::pManager = NULL;

#if defined(POCKETBOOK) || defined(OBREEY)
void timerproc()
{
    SuspendManager::instanse()->timeout();
}
#endif

SuspendManager::SuspendManager()
    : state(ssActive)
    , suspendTimeout_(0)
{
    transitions.push_back(Transition(ssActive,ssSuspended,evPowerBtn));
    transitions.push_back(Transition(ssActive,ssSuspended,evSleepCoverPressed));
    transitions.push_back(Transition(ssActive,ssSuspended,evFifo));
    transitions.push_back(Transition(ssActive,ssSuspended,evTimeout));
    transitions.push_back(Transition(ssSuspended,ssActive,evPowerBtn));
    transitions.push_back(Transition(ssSuspended,ssActive,evTouchScreen));
    transitions.push_back(Transition(ssSuspended,ssActive,evSleepCoverReleased));
#if !defined(POCKETBOOK) && !defined(OBREEY)
    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));
#endif
}

bool SuspendManager::active()
{
#if defined(KINDLE)
    return 0 == qstrcmp(qgetenv("USE_SUSPEND_MANAGER").constData(), "1");
#else
    return true;
#endif
}

void SuspendManager::timeout()
{
    event( evTimeout );
}

SuspendManager* SuspendManager::instanse()
{
    if ( !pManager )
        pManager = new SuspendManager();
    return pManager;
}

void SuspendManager::cleanup()
{
    if ( pManager )
    {
        delete pManager;
        pManager = NULL;
    }
}

void SuspendManager::event( SuspendManager::Event e )
{
    for ( int i=0; i<transitions.size(); ++i )
    {
        Transition& t = transitions[i];
        if ( t.oldState == state && t.event == e )
        {
            state = t.newState;
            emit stateChanged( t );
            break;
        }
    }
}

void SuspendManager::setSuspendTimeout( int val )
{
    suspendTimeout_ = val;
    if ( 0 == suspendTimeout_ )
        stopTimer();
    else
        startTimer();
}

void SuspendManager::activity()
{
    if ( suspendTimeout_ != 0 && ssActive == state )
        startTimer();
}

void SuspendManager::startTimer()
{
#if defined(POCKETBOOK) || defined(OBREEY)
    ::SetHardTimer("timer", timerproc, suspendTimeout_);
#else
    timer.start( suspendTimeout_ );
#endif
}

void SuspendManager::stopTimer()
{
#if defined(POCKETBOOK) || defined(OBREEY)
    ::ClearTimer(timerproc);
#else
    timer.stop();
#endif
}

