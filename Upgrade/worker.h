#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QFile>
#include <QSet>
#include <QLinkedList>
#include <QStringList>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QProcess>

#include "versioninfo.h"

class Worker : public QObject
{
    QFile currFile;

    QString buffer;
    VersionInfo version;

    char downloaded;
    char extracted;

    bool test;

    QProcess* proc;

Q_OBJECT
    QNetworkAccessManager* nm;

public:
    Worker(QObject *parent = NULL);

    bool isDownloaded() const
    { return downloaded != 0; }
    bool isExtracted() const
    { return extracted != 0; }

signals:
    void progress(const QString&);
    void activity();
    void finished();
    void newBuildFound();
    void noLocalBuild();

public slots:
    void checkLocalVersion();
    void checkRemoteVersion();
    void upgrade();
    void readyReadDefault();
    void checkVersionFinished();
    void downloadFileReadyRead();
    void downloadFileFinished();
    void error(QNetworkReply::NetworkError code);
    void processReadyReadStandardOutput();
    void processReadyReadStandardError();
    void processFinished(int);

private:    
    QString platform() const;
    QString upgradePath() const;
    QString zipFilePath() const;
    QString zipFileName() const;
    QString zipFileFullName() const;
    void setupReply(QNetworkReply* reply);
};

#endif // DOWNLOADER_H
