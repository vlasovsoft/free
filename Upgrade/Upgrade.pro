TARGET = upgrade
TEMPLATE = app
QT += network
COMMON = ../common

include(../common/common.pri)
include(../common/i18n.pri)
include(quazip.pri)

SOURCES += main.cpp  widget.cpp settingsdialog.cpp worker.cpp
HEADERS += widget.h settingsdialog.h worker.h params.h
FORMS    += widget.ui settingsdialog.ui
RESOURCES += images/images.qrc
TOOL=/home/sergv/toolchain

kobo {
    INCLUDEPATH += $$TOOL/kobo/zlib/include
    LIBS += -L$$TOOL/kobo/zlib/lib
}
kindle {
    INCLUDEPATH += $$TOOL/kindle/zlib/include
    LIBS += -L$$TOOL/kindle/zlib/lib
}
obreey {
    INCLUDEPATH += $$TOOL/obreey/zlib/include
    LIBS += -L$$TOOL/obreey/zlib/lib
}
pb {
    INCLUDEPATH += $$TOOL/pb/zlib/include
    LIBS += -L$$TOOL/pb/zlib/lib
}

LIBS += -lz
