#include <QDir>

#include "Platform.h"
#include "Config.h"
#include "messagebox.h"
#include "params.h"

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    init();

    ui->chkAllowTestBuilds->setChecked( g_pConfig->readBool(CFG_ALLOW_TEST_BUILDS, false) );
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::accept()
{
    g_pConfig->writeBool(CFG_ALLOW_TEST_BUILDS, ui->chkAllowTestBuilds->isChecked());
    Dialog::accept();
}

void SettingsDialog::on_buttonBox_accepted()
{
    accept();
}

void SettingsDialog::on_buttonBox_rejected()
{
    reject();
}
