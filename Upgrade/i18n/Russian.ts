<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ru">
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Allow test builds</source>
        <translation>Разрешить тестовые сборки</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="31"/>
        <source>Current build: %1
Click the 'next' button to check a new build...</source>
        <translation>Текущая сборка: %1
Нажмите кнопку 'дальше' для проверки обновления...</translation>
    </message>
</context>
<context>
    <name>Worker</name>
    <message>
        <location filename="../worker.cpp" line="34"/>
        <location filename="../worker.cpp" line="64"/>
        <source>Checking a new build...</source>
        <translation>Проверка новой сборки...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="86"/>
        <source>Downloading...</source>
        <translation>Загружаю...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="101"/>
        <source>Extracting...</source>
        <translation>Распаковываю...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="105"/>
        <source>Extract success!</source>
        <translation>Архив успешно распакован!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="106"/>
        <source>Upgrading...</source>
        <translation>Обновляю...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="116"/>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="209"/>
        <source>Upgrade success!</source>
        <translation>Обновление завершено!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="210"/>
        <source>Please restart launcher.</source>
        <translation>Пожалуйста перезапустите лаунчер.</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="213"/>
        <source>Upgrade failed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="50"/>
        <location filename="../worker.cpp" line="146"/>
        <source>New build is found: %1</source>
        <translation>Найдена новая сборка: %1</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="51"/>
        <location filename="../worker.cpp" line="147"/>
        <source>Click the 'next' button to upgrade...</source>
        <translation>Для обновления нажмите кнопку 'дальше'...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="152"/>
        <source>No new builds are available!</source>
        <translation>Новых сборок не найдено!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="175"/>
        <source>Download success!</source>
        <translation>Обновление успешно загружено!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="192"/>
        <source>Error: %1 - %2</source>
        <translation>Ошибка %1 - %2</translation>
    </message>
</context>
</TS>