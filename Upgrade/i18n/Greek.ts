<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1">
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Allow test builds</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="31"/>
        <source>Current build: %1
Click the 'next' button to check a new build...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Worker</name>
    <message>
        <location filename="../worker.cpp" line="34"/>
        <location filename="../worker.cpp" line="64"/>
        <source>Checking a new build...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="86"/>
        <source>Downloading...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="101"/>
        <source>Extracting...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="105"/>
        <source>Extract success!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="106"/>
        <source>Upgrading...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="116"/>
        <source>Error!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="209"/>
        <source>Upgrade success!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="210"/>
        <source>Please restart launcher.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="213"/>
        <source>Upgrade failed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="50"/>
        <location filename="../worker.cpp" line="146"/>
        <source>New build is found: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="51"/>
        <location filename="../worker.cpp" line="147"/>
        <source>Click the 'next' button to upgrade...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="152"/>
        <source>No new builds are available!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="175"/>
        <source>Download success!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="192"/>
        <source>Error: %1 - %2</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>