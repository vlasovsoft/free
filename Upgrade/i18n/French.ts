<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="fr_FR">
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Allow test builds</source>
        <translation>Autoriser les builds-test</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="31"/>
        <source>Current build: %1
Click the 'next' button to check a new build...</source>
        <translation>Le build actuel: %1
Cliquez le bouton 'next' pour vérifier un nouveau build...</translation>
    </message>
</context>
<context>
    <name>Worker</name>
    <message>
        <location filename="../worker.cpp" line="34"/>
        <location filename="../worker.cpp" line="64"/>
        <source>Checking a new build...</source>
        <translation>Vérifier un nouveau build...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="86"/>
        <source>Downloading...</source>
        <translation>Téléchargement...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="101"/>
        <source>Extracting...</source>
        <translation>Extraction...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="105"/>
        <source>Extract success!</source>
        <translation>Extraction réussie!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="106"/>
        <source>Upgrading...</source>
        <translation>Mise à niveau...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="116"/>
        <source>Error!</source>
        <translation>Erreur!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="209"/>
        <source>Upgrade success!</source>
        <translation>Mise à niveau réussie!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="210"/>
        <source>Please restart launcher.</source>
        <translation>Veuillez relancer le launcher.</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="213"/>
        <source>Upgrade failed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../worker.cpp" line="50"/>
        <location filename="../worker.cpp" line="146"/>
        <source>New build is found: %1</source>
        <translation>Nouveau build trouvé: %1</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="51"/>
        <location filename="../worker.cpp" line="147"/>
        <source>Click the 'next' button to upgrade...</source>
        <translation>Cliquez le bouton 'next' pour mettre à niveau...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="152"/>
        <source>No new builds are available!</source>
        <translation>Pas de nouveau builds disponibles!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="175"/>
        <source>Download success!</source>
        <translation>Téléchargement réussi!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="192"/>
        <source>Error: %1 - %2</source>
        <translation>Erreur: %1 - %2</translation>
    </message>
</context>
</TS>