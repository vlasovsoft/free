#ifndef WIDGET_H
#define WIDGET_H

#include <QFileSystemModel>
#include <QFileIconProvider>
#include <QMenu>
#include <QSet>

#include "WidgetCommon.h"

namespace Ui {
class Widget;
}

class Widget : public WidgetCommon, public QFileIconProvider
{
    Q_OBJECT
    
    Ui::Widget *ui;

    QFileSystemModel model;
    QIcon iconFile;
    QIcon iconFolder;
    QIcon iconFileChecked;
    QIcon iconFolderChecked;
    QMenu* mainMenu;
    QTimer idleTimer;
    QSet<QString> files;
    bool isCut;

public:
    Widget();
    ~Widget();

    // QFileIconProvider
    virtual QIcon icon ( IconType type ) const;
    virtual QIcon icon ( const QFileInfo & info ) const;

private slots:
    void on_btnCancel_clicked();
    void onClicked( const QModelIndex& index );
    void on_btnUp_clicked();
    void on_btnMode_toggled(bool checked);
    void on_btnMenu_clicked();
    void on_createFolder_triggered();
    void on_actionDelete_triggered();
    void onIdle();
    void on_actionSelectAll_triggered();
    void on_actionClearSelection_triggered();
    void on_actionCopy_triggered();
    void on_actionCut_triggered();
    void on_actionPaste_triggered();
    void on_actionRename_triggered();
    void on_actionClone_triggered();
    void on_btnHome_clicked();
    void on_cbBookmarks_currentIndexChanged(int index);
    void on_actionAddBookmark_triggered();
    void on_actionRemoveBookmark_triggered();
    void on_actionSetAsHome_triggered();
    void clearSelection();
    void updatePathText();

    void on_btnSettings_clicked();

protected:
    bool event(QEvent* e);
    void resizeEvent ( QResizeEvent * event );

private:
    QString iconPath( const QString& fn );
    void makeFileList( QSet<QString>& files );
    QMenu* createMainMenu();
    void runCommand( const QString& cmd );
    void home();
    void changeDir( const QModelIndex& path, bool keepBookmarks = false );
    void changeDir( const QString& path, bool keepBookmarks = false );
    QString getBookmarksFilePath() const;
    void storeBookmarks();
    void restoreBookmarks();
    void setupModelFilter();
};

#endif // WIDGET_H
