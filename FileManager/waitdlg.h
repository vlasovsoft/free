#ifndef WAITDLG_H
#define WAITDLG_H

#include <QDialog>

namespace Ui {
class WaitDlg;
}

class WaitDlg : public QDialog
{
    Q_OBJECT

public:
    explicit WaitDlg(QWidget *parent = 0);
    ~WaitDlg();

private:
    Ui::WaitDlg *ui;

private slots:
    void finished(int);
};

#endif // WAITDLG_H
