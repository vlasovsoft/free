#include "waitdlg.h"
#include "ui_waitdlg.h"

WaitDlg::WaitDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WaitDlg)
{
    ui->setupUi(this);

    setWindowFlags( windowFlags() | Qt::FramelessWindowHint );
}

WaitDlg::~WaitDlg()
{
    delete ui;
}

void WaitDlg::finished(int)
{
    sender()->deleteLater();
    accept();
}
