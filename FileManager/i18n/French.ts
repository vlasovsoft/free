<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="fr_FR">
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settingsdlg.ui" line="14"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="20"/>
        <source>Show hidden files</source>
        <translation>Montrer fichiers cachés</translation>
    </message>
</context>
<context>
    <name>WaitDlg</name>
    <message>
        <location filename="../waitdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../waitdlg.ui" line="20"/>
        <source>Please wait</source>
        <translation>Patientez svp</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="17"/>
        <source>Open file</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="180"/>
        <location filename="../widget.cpp" line="285"/>
        <source>Create folder</source>
        <translation>Créer dossier</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="185"/>
        <location filename="../widget.ui" line="188"/>
        <location filename="../widget.cpp" line="294"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <source>Select all</source>
        <translation>Tout selectionner</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="198"/>
        <source>Clear selection</source>
        <translation>Effacer selection</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="203"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="208"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="213"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="218"/>
        <location filename="../widget.cpp" line="396"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="223"/>
        <location filename="../widget.cpp" line="416"/>
        <source>Clone</source>
        <translation>Cloner</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="228"/>
        <source>Add bookmark</source>
        <translation>Ajouter marque-page</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="233"/>
        <source>Remove bookmark</source>
        <translation>Retirer marque-page</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="238"/>
        <source>Set as home</source>
        <translation>Définir comme Accueil</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="233"/>
        <source>Bookmarks</source>
        <translation>Marque-pages</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="294"/>
        <source>Are you sure?</source>
        <translation>Etes vous sûr?</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="447"/>
        <source>Bookmark</source>
        <translation>Marque-page</translation>
    </message>
</context>
</TS>