<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="it_IT">
<context>
    <name>SettingsDlg</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">Finestra di dialogo</translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="14"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="20"/>
        <source>Show hidden files</source>
        <translation>Mostra file nascosti</translation>
    </message>
</context>
<context>
    <name>WaitDlg</name>
    <message>
        <location filename="../waitdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Finestra di dialogo</translation>
    </message>
    <message>
        <location filename="../waitdlg.ui" line="20"/>
        <source>Please wait</source>
        <translation>Attendere prego</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="17"/>
        <source>Open file</source>
        <translation>Apri documento</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="180"/>
        <location filename="../widget.cpp" line="285"/>
        <source>Create folder</source>
        <translation>Crea una cartella</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="185"/>
        <location filename="../widget.ui" line="188"/>
        <location filename="../widget.cpp" line="294"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <source>Select all</source>
        <translation>Seleziona tutto</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="198"/>
        <source>Clear selection</source>
        <translation>Deseleziona tutto</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="203"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="208"/>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="213"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="218"/>
        <location filename="../widget.cpp" line="396"/>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="223"/>
        <location filename="../widget.cpp" line="416"/>
        <source>Clone</source>
        <translation>Clona</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="228"/>
        <source>Add bookmark</source>
        <translation>Aggiungi segnalibro</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="233"/>
        <source>Remove bookmark</source>
        <translation>Elimina segnalibro</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="238"/>
        <source>Set as home</source>
        <translation>Imposta come pagina principale</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="233"/>
        <source>Bookmarks</source>
        <translation>Segnalibri</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="294"/>
        <source>Are you sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="447"/>
        <source>Bookmark</source>
        <translation>Segnalibro</translation>
    </message>
</context>
</TS>