#include "Config.h"

#include "settingsdlg.h"
#include "ui_settingsdlg.h"

SettingsDlg::SettingsDlg(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::SettingsDlg)
{
    ui->setupUi(this);
    init();

    ui->chkHidden->setChecked(g_pConfig->readBool("hidden_files", true));
}

SettingsDlg::~SettingsDlg()
{
    delete ui;
}

void SettingsDlg::accept()
{
    g_pConfig->writeBool("hidden_files", ui->chkHidden->isChecked());
    Dialog::accept();
}
