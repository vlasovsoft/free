#include <unistd.h>

#include <iostream>

#include <QScreen>

#include "SuspendManager.h"

#include "kobots.h"

KoboTS::KoboTS(const QString & driver, const QString & device, QObject* parent) 
    : QObject(parent)
    , QWSMouseHandler(driver, device)
    , dev(get_kobo_device())
    , buttons(Qt::NoButton)
    , trId(0)
    , _debug(false)
    , isInputCaptured(false)
{
    _debug = device.contains("debug", Qt::CaseInsensitive);

    if (_debug)
        std::cout << "KoboTS " << driver.toLatin1().constData() << " " << device.toLatin1().constData() << std::endl;

    _fd = open("/dev/input/event1", O_RDONLY);

    _sn = new QSocketNotifier(_fd, QSocketNotifier::Read);

    connect(_sn, SIGNAL(activated(int)), this, SLOT(activity(int)));

    _sn->setEnabled(true);

    captureInput();
}

KoboTS::~KoboTS()
{
    releaseInput();
    delete _sn;
    close(_fd);
}

void KoboTS::suspend()
{ 
    _sn->setEnabled(false);
    if ( _debug ) std::cout << "KoboTS::suspend()" << std::endl;
}

void KoboTS::resume()
{ 
    _sn->setEnabled(true); 
    if ( _debug ) std::cout << "KoboTS::resume()" << std::endl;
}

void KoboTS::xPosition(int val)
{
    switch(dev)
    {
    case KoboAuraH2O2_v1:
    case KoboAuraH2O2_v2:
        state.setX( qt_screen->deviceWidth()-val );
        break;
    case KoboLibra:
        state.setY( val );
        break;
    default:
        state.setX( val );
        break;
    }
}

void KoboTS::yPosition(int val)
{
    switch(dev)
    {
    case KoboAuraH2O2_v2:
        state.setY( qt_screen->deviceHeight()-val );
        break;
    case KoboLibra:
        state.setX( qt_screen->deviceWidth()-val );
        break;
    default:
        state.setY( val );
        break;
    }
}

void KoboTS::activity(int)
{
    _sn->setEnabled(false);

    input_event in; 

    unsigned size = 0;

    while ( size < sizeof(input_event) )
    {
        ssize_t s = read(_fd, ((char*)&in)+size, sizeof(input_event)-size);
        if ( -1 == s || 0 == s ) return;
        size += s;
    }

    if (_debug) 
        std::cout << "TS data: type=" << in.type << " code=" << in.code << " value=" << in.value << std::endl;

    switch ( in.type )
    {
    case EV_KEY:
        if ( in.code == BTN_TOUCH )
            state.setPressed( in.value > 0 );
        break;        
    case EV_ABS:
        switch ( in.code )
        {
        case ABS_MT_POSITION_X:
            if ( 0 == trId )
                xPosition(in.value);
            break;
        case ABS_MT_POSITION_Y:
            if ( 0 == trId )
                yPosition(in.value);
            break;
        case ABS_MT_TRACKING_ID:
            trId = in.value;
            break;
        default:
            break;
        }
        break;
    case EV_SYN:
        if ( state.isActive() )
        {
            mouseChanged(QPoint(state.getX(),state.getY()), state.isPressed() ? Qt::LeftButton : Qt::NoButton, 0);
            SuspendManager::instanse()->activity();
            if ( _debug ) 
                std::cout << "Mouse changed: x=" << state.getX() << " y=" << state.getY() << " pressed=" << state.isPressed() << std::endl;
            state.setChanged(false);
            if ( !state.isPressed() )
                state.reset();
        }    
        break;
    default:
        break ;
    }

    _sn->setEnabled(true);
}

void KoboTS::captureInput(void)
{
        int on = 1 ;
        if ( !isInputCaptured )
        {
            if (_debug) std::cout << "Attempting to capture input..." << std::endl;
            if ( _fd != -1 )
            {
                if ( ioctl(_fd, EVIOCGRAB, on) )
                {
                    if ( _debug ) std::cout << "Capture touch input: error" << std::endl;
                }
                else
                {
                    if ( _debug ) std::cout << "Capture touch input: success" << std::endl;
                }
            }
            isInputCaptured = true;
      }
}

void KoboTS::releaseInput()
{
    int off = 0;
    if ( isInputCaptured )
    {
        if (_debug) std::cout << "attempting to release input..." << std::endl;
        if (_fd != -1)
        {
            if (ioctl(_fd, EVIOCGRAB, off)) 
            {
                if (_debug) std::cout << "Release touch input: error" << std::endl;
            }
        }
        isInputCaptured = false;
    }
}

