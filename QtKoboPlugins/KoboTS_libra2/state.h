#ifndef STATE_H
#define STATE_H 

class State {
    int x;
    int y;
    bool pressed;
    bool changed;
public:
    State() 
    {
        reset();
    }
    void reset() 
    {
        x = y = -1;
        pressed = false;
        changed = false;
    }
    bool isActive() const 
    {
        return x > 0 && y > 0 && changed;
    }
    void setChanged(bool val)
    {
        changed = val;
    }
    int getX() const
    {
        return x;
    }
    void setX(int val) 
    {
        if ( x != val ) 
        {
            x = val;
            changed = true;
        }
    }
    int getY() const
    {
        return y;
    }
    void setY(int val) 
    {
        if ( y != val )
        {
            y = val;
            changed = true;
        }
    }
    bool isPressed() const
    {
        return pressed;
    }
    void setPressed( bool val )
    {
        if ( pressed != val )
        {
            pressed = val;
            changed = true;
        }
    }  
};

#endif
