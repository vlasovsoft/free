#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>

#include <QTime>

#include <iostream>

#include "KoboFb.h"

KoboFb::KoboFb()
    : QLinuxFbScreen(0)
    , dev(get_kobo_device())
    , fb(-1)
    , einkFb(dev)
    , isDebug(false)
{
    setSingleShot(true);
    QObject::connect(this, SIGNAL(timeout()), this, SLOT(update()));
}

KoboFb::~KoboFb()
{
}

bool KoboFb::connect(const QString& displaySpec)
{
    const QStringList args = displaySpec.split(':');
    isDebug = einkFb.debug = args.contains("debug");

    fb = einkFb.fbd = open("/dev/fb0", O_RDWR);

    if ( -1 == fb )
    {
        if ( isDebug )
            std::cerr << "FAILED to open framebuffer." << std::endl;
    }

    bool res = QLinuxFbScreen::connect("");

    int dpi = 0;
    switch ( get_kobo_device() )
    {
        case KoboTouch:
            dpi = 167;
        case KoboGlo:
            dpi = 213;
            break;
        case KoboMini:
            dpi = 200;
            break;
        case KoboAura:
            dpi = 212;
            break;
        case KoboAuraHD:
            dpi = 265;
            break;
        case KoboAuraH2O:
            dpi = 265;
            break;
        case KoboGloHD:
            dpi = 300;
            break;
        case KoboTouch2:
            dpi = 167;
            break;
        case KoboAura2_v1:
        case KoboAura2_v2:
            dpi = 212;
            break;
        case KoboAuraH2O2_v1:
        case KoboAuraH2O2_v2:
            dpi = 265;
            break;
        case KoboAuraOne:
        case KoboForma:
        case KoboLibra:
        case KoboLibra2:
            dpi = 300;
            break;
        case KoboNia:
            dpi = 212;
            break;
        default:
            dpi = 200;
            break;
    }

    if ( dpi > 0 )
    {
        physWidth  = qRound(w * 25.4 / dpi); // mm
        physHeight = qRound(h * 25.4 / dpi); // mm 
    }
        
    dw = w;
    dh = h;

    if ( 8 == depth() && QImage::Format_Invalid == pixelFormat() ) {
        QScreen::setPixelFormat(QImage::Format_Indexed8);
    }

    if ( isDebug ) {
        std::cout << "Framebuffer info:" << std::endl;
        std::cout << " size: " << width() << " x " << height() << std::endl;
        std::cout << " physical size: " << physicalWidth() << " x " << physicalHeight() << std::endl;
        std::cout << " device size: " << deviceWidth() << " x " << deviceHeight() << std::endl;
        std::cout << " depth: " << depth() << std::endl;
        std::cout << " linestep: " << linestep() << std::endl;
        std::cout << " pixel format: " << pixelFormat() << std::endl;
        std::cout << " grayscale: " << (int)grayscale << std::endl;
    }

    einkFb.width = width();
    einkFb.height = height();

    return res;
}

void KoboFb::setDirty ( const QRect& r )
{
    if ( isDebug )
        std::cout << "KiboFb::setDirty: " 
                  << r.x() << "," 
                  << r.y() << "," 
                  << r.width() << "," 
                  << r.height() 
                  << std::endl;

    updateRect = updateRect.united(r);
    start(10);
}

void KoboFb::update()
{
    if ( -1 != fb && updateRect.width() > 0 && updateRect.height() > 0 )
    {
        bool fullUpdate = updateRect.width() == width() && updateRect.height() == height();
        if ( isDebug ) {
            std::cout << "KoboFb::update(): "
                      << (fullUpdate? "full":"partial") << ","
                      << updateRect.x() << ","
                      << updateRect.y() << ","
                      << updateRect.width() << ","
                      << updateRect.height()
                      << std::endl;
        }
        if ( fullUpdate )
            einkFb.refreshFull(updateRect.x(), updateRect.y(), updateRect.width(), updateRect.height(), false);
        else
            einkFb.refreshUI(updateRect.x(), updateRect.y(), updateRect.width(), updateRect.height(), false);
    }
    updateRect = QRect();
}

QT_END_NAMESPACE
