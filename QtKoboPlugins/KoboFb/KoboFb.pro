QT += core gui
TARGET = KoboFb
TEMPLATE = lib
CONFIG += plugin
SOURCES += KoboFb.cpp screenplugin.cpp
HEADERS += KoboFb.h screenplugin.h ScreenManager.h ScreenManagerCallback.h
INCLUDEPATH += $$PWD/../../common
LIBS        += -L../../common -lcommon
