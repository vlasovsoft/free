#include <unistd.h>

#include "kobokb.h"

KoboKb::KoboKb(const QString & device)
    : debug(false)
    , dev(get_kobo_device())
    , fd_kbd(-1)
    , fd_pwr(-1)
    , sn_kbd(NULL)
    , sn_pwr(NULL)
    , isInputCapturedKbd(false)
    , isInputCapturedPwr(false)
{
    if (device.contains("debug", Qt::CaseInsensitive))
        debug = true;

    std::cout << "KoboKb(" << (const char*)device.toAscii() << ")" << std::endl;

    openKbdDevice();
    openPwrDevice();
}

KoboKb::~KoboKb()
{
    releaseInputKbd();
    releaseInputPwr();
    delete sn_kbd;
    delete sn_pwr;
    close(fd_kbd);
    close(fd_pwr);
}

void KoboKb::activity(int fd)
{
    QSocketNotifier* sn = static_cast<QSocketNotifier*>(sender());
    sn->setEnabled(false);

    input_event in;
    read(fd, &in, sizeof(input_event));

    int code = Qt::Key_unknown;

    switch ( in.code )
    {
    case KEY_POWER:
        code = Qt::Key_PowerDown;
        break;
    case KEY_LIGHT:
        code = Qt::Key_BrightnessAdjust;
        break;
    case KEY_HOME:
        code = Qt::Key_Home;
        break;
    case KEY_SLEEP_COVER:
        code = Qt::Key_PowerOff;
        break;
    case KEY_PAGE_UP:
        code = Qt::Key_PageUp;
        break;
    case KEY_PAGE_DOWN:
        code = Qt::Key_PageDown;
        break;
    default:
        break;
    } 

    if ( code != Qt::Key_unknown )
    {
	bool press  = ( 1 == in.value || 2 == in.value );
	bool repeat = ( 2 == in.value );
        processKeyEvent(0, code, Qt::NoModifier, press, repeat);
    }

    if ( debug )
        std::cout << "in.type:" << in.type << " | in.code: " << in.code << " | code:" << code << " | " << (in.value? "pressed":"released") << std::endl;

   sn->setEnabled(true);
}

int KoboKb::probeDevice( const char* paths[], unsigned size )
{
    int result = -1;
    for ( unsigned i=0; i<size && result < 0; ++i )
    {
        result = open(paths[i], O_RDONLY);
        if ( debug ) {
            std::cout << "Trying device [" << paths[i] << "]: " << (result >= 0 ? "SUCCESS":"FAIL") << std::endl;
        }
    }
    return result;
}

int KoboKb::probeKbdDevice()
{
   static const char* probe[] = {
        "/dev/input/by-path/platform-gpio-keys-event",
        "/dev/input/by-path/platform-ntx_event0-event",
        "/dev/input/by-path/platform-mxckpd-event",
        "/dev/input/event0"
    };
    static const char* def[] = {
        "/dev/input/event0"
    };
    if ( debug ) std::cout << "Trying out kbd device:" << std::endl;
    return automagic_sysfs(dev) ?
        probeDevice(probe, sizeof(probe)/sizeof(probe[0])) :
        probeDevice(def, sizeof(def)/sizeof(def[0]));
}

int KoboKb::probePwrDevice()
{
    const char* probe[] = {
        "/dev/input/by-path/platform-bd71828-pwrkey-event",
        "/dev/input/by-path/platform-bd71828-pwrkey.4.auto-event",
    };
    if ( debug ) std::cout << "Trying out pwr device:" << std::endl;
    return automagic_sysfs(dev) ?
        probeDevice(probe, sizeof(probe)/sizeof(probe[0])) :
        -1;
}

void KoboKb::openKbdDevice()
{
    fd_kbd = probeKbdDevice();
    if ( fd_kbd >= 0 )
    {
        sn_kbd = new QSocketNotifier(fd_kbd, QSocketNotifier::Read);
        connect(sn_kbd, SIGNAL(activated(int)), this, SLOT(activity(int)));
        sn_kbd->setEnabled(true);
        captureInputKbd();
    }
}

void KoboKb::openPwrDevice()
{
    fd_pwr = probePwrDevice();
    if ( fd_pwr >= 0 )
    {
        sn_pwr = new QSocketNotifier(fd_pwr, QSocketNotifier::Read);
        connect(sn_pwr, SIGNAL(activated(int)), this, SLOT(activity(int)));
        sn_pwr->setEnabled(true);
        captureInputPwr();
    }
}

bool KoboKb::captureInput(int fd)
{
    int on = 1;
    return 0 == ioctl(fd, EVIOCGRAB, on);
}

void KoboKb::releaseInput(int fd)
{
    int off = 0;
    ioctl(fd, EVIOCGRAB, off);
}

void KoboKb::captureInputKbd()
{
    if (!isInputCapturedKbd && fd_kbd >= 0)
    {
        isInputCapturedKbd = captureInput(fd_kbd);
        if ( debug ) std::cout << "captureInputKbd(): " << (isInputCapturedKbd? "SUCCESS":"FAILED") << std::endl;
    }
}

void KoboKb::captureInputPwr()
{
    if (!isInputCapturedPwr && fd_pwr >= 0)
    {
        isInputCapturedPwr = captureInput(fd_pwr);
        if ( debug ) std::cout << "captureInputPwr(): " << (isInputCapturedPwr ? "SUCCESS":"FAILED") << std::endl;
    }
}

void KoboKb::releaseInputKbd()
{
    if (isInputCapturedKbd && fd_kbd >= 0)
    {
        releaseInput(fd_kbd);
        isInputCapturedKbd = false;
        if ( debug ) std::cout << "releaseInputKbd()" << std::endl;
    }
}

void KoboKb::releaseInputPwr()
{
    if (isInputCapturedPwr && fd_pwr >= 0)
    {
        releaseInput(fd_pwr);
        isInputCapturedPwr = false;
        if ( debug ) std::cout << "releaseInputPwr()" << std::endl;
    }
}

