#ifndef KOBOKB_H
#define KOBOKB_H

#include <QWSKeyboardHandler>
#include <QSocketNotifier>
#include <QWSServer>

#include <iostream>
#include <fcntl.h>
#include <linux/input.h>

#include "KoboDevice.h"

class KoboKb : public QObject, public QWSKeyboardHandler
{
    Q_OBJECT

public:
    KoboKb(const QString& device);
    virtual ~KoboKb();

private slots:
    void activity(int);

private:
    bool debug;
    KoboDevice dev;
    int fd_kbd;
    int fd_pwr;
    QSocketNotifier* sn_kbd;
    QSocketNotifier* sn_pwr;
    bool isInputCapturedKbd;
    bool isInputCapturedPwr;

private:
    int probeDevice(const char* paths[], unsigned size);
    int probeKbdDevice();
    int probePwrDevice();
    void openKbdDevice();
    void openPwrDevice();
    bool captureInput(int fd);
    void releaseInput(int fd);
    void captureInputKbd();
    void captureInputPwr();
    void releaseInputKbd();
    void releaseInputPwr();
};

#define KEY_LIGHT 90
#define KEY_HOME  102
#define KEY_POWER 116
#define KEY_SLEEP_COVER 59
#define KEY_PAGE_UP 193
#define KEY_PAGE_DOWN 194

#define EVENT_PRESS 1
#define EVENT_RELEASE 0

#endif // KOBOKB_H
