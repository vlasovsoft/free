QT += core gui
TARGET = KoboTS_h2o
TEMPLATE = lib
CONFIG += plugin

SOURCES += tsplugin.cpp kobots.cpp
HEADERS += tsplugin.h kobots.h
INCLUDEPATH += $$PWD/../../SuspendManager
LIBS        += -L../../SuspendManager -lSuspendManager
