#include <QTimer>

#include "Application.h"

class CrApplication : public Application
{
    Q_OBJECT

    const char* argv0;

#if defined(OBREEY)
    QTimer idleTimer;
#endif

public:
    CrApplication(int& argc, char** argv);
    ~CrApplication();

protected:
    void customInit();

#if defined(OBREEY)
    bool notify(QObject * receiver, QEvent * event);
#endif

private slots:
#if defined(OBREEY)
    void onIdle();
#endif
};

