#ifndef QUOTEDIALOG_H
#define QUOTEDIALOG_H

#include "Dialog.h"

namespace Ui {
class QuoteDialog;
}

class QuoteDialog : public Dialog
{
    Q_OBJECT

public:
    QuoteDialog(QWidget* parent, const QString& text);
    ~QuoteDialog();

    QString getText() const;

private:
    Ui::QuoteDialog *ui;
};

#endif // QUOTEDIALOG_H
