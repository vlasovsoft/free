#ifndef SELECTION_H
#define SELECTION_H

#include <QWidget>

class QCloseEvent;
class CR3View;

namespace Ui {
class Selection;
}

class Selection : public QWidget
{
    Q_OBJECT

public:
    Selection(CR3View* v);
    ~Selection();

protected:
    void closeEvent(QCloseEvent *event);

signals:
    void citation();

private slots:
    void on_btnClose_clicked();
    void on_btnNextLine_clicked();
    void on_btnPrevLine_clicked();
    void onSelected();

    void on_btnCite_clicked();

private:
    Ui::Selection *ui;
    CR3View* view;
};

#endif // SELECTION_H
