<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1">
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.ui" line="20"/>
        <location filename="../bookmarklistdlg.ui" line="26"/>
        <source>Bookmarks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="86"/>
        <source>Text</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="434"/>
        <source>Error while opening document </source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.ui" line="20"/>
        <source>Document properties</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.ui" line="64"/>
        <source>Ok</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="132"/>
        <source>Archive name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="133"/>
        <source>Archive path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="134"/>
        <source>Archive size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="135"/>
        <source>File name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="136"/>
        <source>File path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="137"/>
        <source>File size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="138"/>
        <source>File format</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="140"/>
        <source>File info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="142"/>
        <source>Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="143"/>
        <source>Author(s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="144"/>
        <source>Series name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="145"/>
        <source>Series number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="146"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="147"/>
        <source>Genres</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="148"/>
        <source>Translator</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="149"/>
        <source>Book info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="151"/>
        <source>Document author</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="152"/>
        <source>Document date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="153"/>
        <source>Document source URL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="154"/>
        <source>OCR by</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="155"/>
        <source>Document version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="156"/>
        <source>Document info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="158"/>
        <source>Publication name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="159"/>
        <source>Publisher</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="160"/>
        <source>Publisher city</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="161"/>
        <source>Publication year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="162"/>
        <source>ISBN</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="163"/>
        <source>Publication info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="165"/>
        <source>Custom info</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <location filename="../gotodialog.ui" line="14"/>
        <location filename="../gotodialog.ui" line="20"/>
        <source>Position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="52"/>
        <source>Page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="96"/>
        <source>Go</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="103"/>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="498"/>
        <source>Navigation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Bookmark created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Search pattern is not found in document</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Delete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Do you really want to delete the current document?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>Removed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>Document was removed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="857"/>
        <source>Citation is saved</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="894"/>
        <source>Pages remain: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="902"/>
        <source>Auto paging is off.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="907"/>
        <source>Auto paging is on.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="41"/>
        <source>Open file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <location filename="../mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Next page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>Previous page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="67"/>
        <source>Back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="72"/>
        <source>Forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="77"/>
        <source>Table of contents</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>Recent books</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="90"/>
        <source>Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="95"/>
        <location filename="../mainwindow.ui" line="98"/>
        <source>Add bookmark</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Bookmarks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <source>File properties</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <location filename="../mainwindow.ui" line="116"/>
        <source>Find text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="121"/>
        <source>Show menu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Front light</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <source>Screen rotation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="153"/>
        <source>Toggle frontlight</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="158"/>
        <source>Frontlight &gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="163"/>
        <source>Frontlight &lt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="168"/>
        <source>First page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Last page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="178"/>
        <source>Next chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>Previous chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="188"/>
        <source>Next 10 pages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="193"/>
        <source>Previous 10 pages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="198"/>
        <source>Zoom In</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Zoom Out</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="208"/>
        <location filename="../mainwindow.ui" line="211"/>
        <source>Position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="216"/>
        <source>Toggle inversion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Toggle header</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <source>Suspend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>Screen rotation 0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>Screen rotation 90</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="241"/>
        <source>Screen rotation 180</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="246"/>
        <source>Screen rotation 270</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="251"/>
        <source>Screen rotation +90</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Screen rotation -90</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="261"/>
        <source>Screen rotation +180</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>Delete current document</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="271"/>
        <source>Dictionary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="276"/>
        <source>Refresh screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="281"/>
        <source>Screen shot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Select</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="291"/>
        <source>Pages remain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="296"/>
        <source>Auto paging</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <source>Open last book</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../navigationbar.cpp" line="92"/>
        <location filename="../navigationbar.cpp" line="109"/>
        <source>%1-%2 (total %3)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenFileDlg</name>
    <message>
        <location filename="../openfiledlg.ui" line="20"/>
        <location filename="../openfiledlg.ui" line="26"/>
        <source>Open file</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.ui" line="26"/>
        <location filename="../recentdlg.ui" line="32"/>
        <source>Recent books</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="221"/>
        <source>Removed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="221"/>
        <source>Document was removed</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RecentRemoveDlg</name>
    <message>
        <location filename="../recentremovedlg.ui" line="14"/>
        <source>Delete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="20"/>
        <source>Delete record only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="27"/>
        <source>Delete record and file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="47"/>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.ui" line="20"/>
        <location filename="../searchdlg.ui" line="26"/>
        <source>Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="51"/>
        <source>Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="61"/>
        <source>Case sensitive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="68"/>
        <source>Search forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="81"/>
        <source>Search backward</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="42"/>
        <source>Window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="83"/>
        <source>Recent book</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="88"/>
        <source>Recent books list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="93"/>
        <source>Open file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="98"/>
        <source>Do nothing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="114"/>
        <source>Disable txt auto formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="121"/>
        <source>Internal styles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="128"/>
        <source>Embedded fonts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="138"/>
        <source>Launcher settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <source>Page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="191"/>
        <source>Header</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="197"/>
        <location filename="../settings.ui" line="369"/>
        <source>Show</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="598"/>
        <source>Font</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="612"/>
        <source>Face</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="236"/>
        <location filename="../settings.ui" line="635"/>
        <source>Size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="268"/>
        <source>Elements</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="274"/>
        <source>Chapter marks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="281"/>
        <source>Book name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="288"/>
        <source>Clock</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="295"/>
        <source>Position percent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="302"/>
        <source>Page number/count</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="317"/>
        <source>Battery status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="324"/>
        <source>In percent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="363"/>
        <source>Footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="390"/>
        <source>Misc</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="407"/>
        <source>View Mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="421"/>
        <source>One page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="426"/>
        <source>Two pages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="434"/>
        <source>Update interval</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="521"/>
        <source>Auto paging, sec</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="542"/>
        <source>Negative</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="569"/>
        <source>Styles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="671"/>
        <source>Gamma</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="716"/>
        <source>Aa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="730"/>
        <location filename="../settings.ui" line="762"/>
        <source>Disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="735"/>
        <source>On for large fonts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="740"/>
        <source>On for all fonts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="748"/>
        <source>Hinting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="767"/>
        <source>Bytecode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="772"/>
        <source>Auto</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="790"/>
        <source>Bold</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="803"/>
        <source>Kerning</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="813"/>
        <source>Layout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="821"/>
        <source>Hyphenation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="841"/>
        <source>Spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="880"/>
        <location filename="../settings.ui" line="1390"/>
        <source>Space</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="921"/>
        <source>Floating punctuation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="942"/>
        <source>Margins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="948"/>
        <source>Top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="974"/>
        <source>Bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1000"/>
        <source>Left</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1026"/>
        <source>Right</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1073"/>
        <source>Control</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1102"/>
        <source>Tap zones</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1115"/>
        <source>Short tap</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1120"/>
        <source>Long tap</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1149"/>
        <source>Swipes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1156"/>
        <source>Left to right</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1161"/>
        <source>Right to left</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1166"/>
        <source>Top to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1171"/>
        <source>Bottom to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1196"/>
        <source>Keys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1439"/>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.ui" line="1446"/>
        <source>Ok</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.cpp" line="179"/>
        <source>Off</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.cpp" line="181"/>
        <source>Algorythmic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settings.cpp" line="189"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.ui" line="20"/>
        <location filename="../tocdlg.ui" line="26"/>
        <source>Table of contents</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>