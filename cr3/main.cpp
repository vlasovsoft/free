#include "application.h"

int z_verbose=0;
extern "C" void z_error(char * msg);
void z_error(char * msg) {
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    CrApplication app(argc, argv);
    if ( !app.init() ) return 1;
    return app.exec();
}

