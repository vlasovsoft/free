#include "quotedialog.h"
#include "ui_quotedialog.h"

QuoteDialog::QuoteDialog(QWidget* parent, const QString& text)
    : Dialog(parent)
    , ui(new Ui::QuoteDialog)
{
    ui->setupUi(this);
    init();
#if defined(Q_WS_QWS)
    ui->verticalLayout->insertWidget(1, getVirtualKeyboard());
#endif
    ui->plainTextEdit->document()->setPlainText(text);
}

QuoteDialog::~QuoteDialog()
{
    delete ui;
}

QString QuoteDialog::getText() const
{
    return ui->plainTextEdit->document()->toPlainText();
}
