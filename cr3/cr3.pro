TARGET = cr3
TEMPLATE = app
COMMON = ../common

CR=/home/sergv/workspace/crengine

include($$PWD/../common/common.pri)
include($$PWD/../common/i18n.pri)

DEFINES += USE_FONTCONFIG=0 USE_FREETYPE=1 LDOM_USE_OWN_MEM_MAN=1 COLOR_BACKBUFFER=1 USE_DOM_UTF8_STORAGE=1 NDEBUG=1 QT_KEYPAD_NAVIGATION

INCLUDEPATH += $$(QTDIR)/include/freetype2 $$CR/crengine/include

SOURCES += main.cpp \
    mainwindow.cpp \
    searchdlg.cpp \
    filepropsdlg.cpp \
    cr3widget.cpp \
    crqtutil.cpp \
    tocdlg.cpp \
    recentdlg.cpp \
    settings.cpp \
    bookmarklistdlg.cpp \
    openfiledlg.cpp \
    orientation.cpp \
    actionlist.cpp \
    mainmenu.cpp \
    gotodialog.cpp \
    progbareventfilter.cpp \
    navigationbar.cpp \
    keymapper.cpp \
    actiondisplayer.cpp \
    recentremovedlg.cpp \
    dictionary.cpp \
    application.cpp \
    quotedialog.cpp \
    selection.cpp

HEADERS += mainwindow.h \
    cr3widget.h \
    crqtutil.h \
    tocdlg.h \
    recentdlg.h \
    settings.h \
    bookmarklistdlg.h \
    searchdlg.h \
    filepropsdlg.h \
    openfiledlg.h \
    orientation.h \
    actionlist.h \
    mainmenu.h \
    gotodialog.h \
    progbareventfilter.h \
    navigationbar.h \
    props.h \
    keymapper.h \
    actiondisplayer.h \
    recentremovedlg.h \
    dictionary.h \
    application.h \
    quotedialog.h \
    selection.h

FORMS += mainwindow.ui \
    tocdlg.ui \
    recentdlg.ui \
    settings.ui \
    bookmarklistdlg.ui \
    searchdlg.ui \
    filepropsdlg.ui \
    openfiledlg.ui \
    gotodialog.ui \
    navigationbar.ui \
    recentremovedlg.ui \
    dictionary.ui \
    quotedialog.ui \
    selection.ui

RESOURCES += cr3.qrc

LIBS += -lQtGui -lQtCore -lQtNetwork -lpthread -ldl

LIBS += -L$$CR/build/qtbuild_$$ARCH/crengine \
        -L$$CR/build/qtbuild_$$ARCH/thirdparty/antiword \
        -L$$CR/build/qtbuild_$$ARCH/thirdparty/chmlib

kobo|kindle|pb|obreey|qvfb {
LIBS += -L$$CR/build/qtbuild_$$ARCH/thirdparty/zlib \ 
        -L$$CR/build/qtbuild_$$ARCH/thirdparty/libpng \
        -L$$CR/build/qtbuild_$$ARCH/thirdparty/libjpeg \
        -L$$CR/build/qtbuild_$$ARCH/thirdparty/freetype
}

LIBS += -lcrengine -lz -lantiword -lchmlib -lpng -ljpeg -lfreetype -lcommon

