#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class Widget : public QWidget
{
    Q_OBJECT

    QImage img;
    
public:
    Widget(QWidget* parent, int time, const QString& fn);
    ~Widget();

private:
    virtual void paintEvent( QPaintEvent* );
};

#endif // WIDGET_H
