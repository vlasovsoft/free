#include <QPainter>
#include <QTimer>
#include <QScreen>
#include <QMatrix>

#include "widget.h"

Widget::Widget(QWidget *parent, int time, const QString& fn)
    : QWidget(parent)
    , img(fn)
{

    int trans = QScreen::instance()->transformOrientation();
    if ( trans != 0 && !img.isNull() )
    {
        QMatrix rm;
        rm.rotate(90*trans);
        img = img.transformed(rm);
    }
    QTimer::singleShot( 1000*time, this, SLOT(close()) );
}

Widget::~Widget()
{
}

void Widget::paintEvent(QPaintEvent*)
{
    QPainter p(this);
    p.drawImage( rect(), img, img.rect() );
}
