TEMPLATE = app
TARGET = calculator
COMMON = ../common

include(../common/common.pri)

SOURCES += calculator.cpp \ 
    main.cpp
HEADERS += calculator.h
FORMS += Calculator.ui
