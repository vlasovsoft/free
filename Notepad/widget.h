#ifndef WIDGET_H
#define WIDGET_H

#include <QTextCodec>
#include <QPlainTextEdit>

#include "QtUtils.h"
#include "WidgetCommon.h"
#include "WidgetEventFilter.h"

namespace Ui {
class Widget;
}

class QKeyEvent;
class QCloseEvent;

class DocumentWidget : public QWidget
{
public:
    QString fileName;
    QPlainTextEdit* edit;

    DocumentWidget( QWidget* parent )
        : QWidget( parent )
        , edit( new QPlainTextEdit(this) )
    {}
};

class Widget : public WidgetCommon
{
    Q_OBJECT
    
    QTextCodec* pCodec;

public:
    Widget();
    ~Widget();

private:
    void load( const QString& fileName );
    virtual void closeEvent(QCloseEvent *event);
    void addTab( const QString& fileName = QString() );
    DocumentWidget* docWidget( int index );
    DocumentWidget* currentDocWidget();

private slots:
    void onNew();
    void onOpen();
    void onSave();
    void onUndo();
    void onRedo();
    void onCopy();
    void onCut();
    void onPaste();
    void onZoomOut();
    void onZoomIn();
    bool onCloseTab( bool keepLast = true );
    void onCurrentTabChanged( int index );
    void openFiles();
    
private:
    Ui::Widget *ui;
    bool isDocumentChanged;
};

#endif // WIDGET_H
