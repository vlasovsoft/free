#include <Application.h>

#include "widget.h"

int main(int argc, char *argv[])
{
    Application app(argc,argv);
    app.setApplicationName("notepad");
    if ( !app.init() ) return 1;

    Widget w;

    return app.exec();
}

