package com.vlasovsoft.runner;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KOptionPane;

import ixtab.jailbreak.Jailbreak;
import ixtab.jailbreak.SuicidalKindlet;

import javax.swing.*;
import java.awt.*;
import java.security.AllPermission;

public class RunnerKindlet extends SuicidalKindlet
{
  private KindletContext ctx;

  boolean canRun = false;

  protected void onCreate(final KindletContext context) 
  {
      ctx = context;
      new Thread() 
      {
          public void run() 
          {
              try 
              {
                  while (    !context.getRootContainer().isValid() 
                          || !context.getRootContainer().isVisible()) 
                 {
                     Thread.sleep(100);
                 }
              } 
              catch (Exception ignored) {}

              SwingUtilities.invokeLater(new Runnable() 
              {
                  public void run() 
                  {
                      switch (checkJailBreak())
                      {
                      case 0:
                          canRun = true;
                          suicide();
                          break;
                      case 1:
                          displayErrorMessage(context, "Failed to obtain all required permissions.");
                          break;
                      case 2:
                          displayErrorMessage(context, "This application requires the Kindlet Jailbreak to be installed.");
                          break;
                      default:
                          displayErrorMessage(context, "Unknown error.");
                          break;
                      }
                  }
              });
          }
      }.start();

      super.onCreate(context);
  }
/*
  protected void onStop() 
  {
      if ( canRun ) 
      {
          try 
          {
              Runtime.getRuntime().exec("/mnt/us/vlasovsoft/launcher.sh");
              Thread.sleep(175);
          } 
          catch (Exception ignored) {}
      }
      super.onStop();
  }
*/
  public void suicide() 
  {
      try 
      {
//          Runtime.getRuntime().exec("lipc-set-prop com.lab126.appmgrd stop app://com.lab126.booklet.kindlet");
          Runtime.getRuntime().exec("/mnt/us/vlasovsoft/launcher.sh");
      } 
      catch (Throwable ex) {}
  }

  private int checkJailBreak()
  {
      if ( jailbreak.isAvailable() ) 
      {
          if ( jailbreak.getContext().requestPermission(new AllPermission()) )
          {
              return 0;
          } 
          else 
          {
              return 1;
          }
      } 
      else 
      {
          return 2;
      }
  }

  private void displayErrorMessage(KindletContext ctx, String error)
  {
      try {
         KOptionPane.showMessageDialog(ctx.getRootContainer(), error, "Error");
      }
      catch ( Exception e ) {}
  }
}

