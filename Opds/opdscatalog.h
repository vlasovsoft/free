#ifndef OPDSCATALOG_H
#define OPDSCATALOG_H

#include <QString>

struct OpdsCatalog
{
    QString name;
    QString  url;
    OpdsCatalog() {}
    OpdsCatalog( const QString& n, const QString& u )
        : name(n)
        , url(u)
    {}
};

#endif // OPDSCATALOG_H
