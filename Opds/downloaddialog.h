#ifndef DOWNLOADDIALOG_H
#define DOWNLOADDIALOG_H

#include "Dialog.h"
#include "download.h"

namespace Ui {
class DownloadDialog;
}

class DownloadDialog : public Dialog, public DownloadCallback
{
    Q_OBJECT

    Download downloader;

public:
    DownloadDialog(QWidget *parent, const QString& url);
    ~DownloadDialog();

    virtual void downloadFinished();
    virtual void downloadError();
    virtual void downloadProgress( int curr, int total );

    QString getFileNameHint() const
    { return downloader.getFileNameHint(); }

    QString getFileName() const
    { return downloader.getFileName(); }

private slots:
    void on_btnCancel_clicked();

private:
    Ui::DownloadDialog *ui;
};

#endif // DOWNLOADDIALOG_H
