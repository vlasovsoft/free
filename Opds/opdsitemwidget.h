#ifndef OPDSITEMWIDGET_H
#define OPDSITEMWIDGET_H

#include <QWidget>

#include "download.h"

namespace Ui {
class OpdsItemWidget;
}

class QListWidgetItem;
class OpdsCatalogItem;

class OpdsItemWidget : public QWidget, public DownloadCallback
{
    Q_OBJECT

    Download downloader;
    OpdsCatalogItem* pNext;
    bool success;
    Ui::OpdsItemWidget *ui;

public:
    OpdsItemWidget(QWidget* parent, const QString& url);
    ~OpdsItemWidget();

    virtual void downloadFinished();
    virtual void downloadError();
    virtual void downloadProgress( int curr, int total );

private slots:
    void listItemClicked(QListWidgetItem *item);

signals:
    void sigNavigate( OpdsCatalogItem* );

private:
    void download( const QString& url );
};

#endif // OPDSITEMWIDGET_H
