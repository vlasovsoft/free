TARGET = opds
TEMPLATE = app
COMMON = ../common

include(../common/common.pri)
include(../common/i18n.pri)

QT += xml

SOURCES += main.cpp widget.cpp \
    opdscatalog.cpp \
    editcatalog.cpp \
    opdsparser.cpp \
    opdscatalogitem.cpp \
    opdsitemwidget.cpp \
    opdsbookwidget.cpp \
    downloaddialog.cpp \
    download.cpp
HEADERS  += widget.h \
    opdscatalog.h \
    editcatalog.h \
    opdsparser.h \
    opdscatalogitem.h \
    opdsitemwidget.h \
    opdsbookwidget.h \
    downloaddialog.h \
    download.h
FORMS    += widget.ui \
    editcatalog.ui \
    opdsitemwidget.ui \
    opdsbookwidget.ui \
    downloaddialog.ui
