#ifndef OPDSPARSER_H
#define OPDSPARSER_H

#include <QUrl>
#include <QVector>
#include <QXmlDefaultHandler>

class OpdsParser : public QXmlDefaultHandler
{
public:
    struct Item
    {
        QString name;
        QString content;
        QString cType;
        QMap<QString,QString> urls;
    };

private:
    Item item;
    QVector<Item>& items;
    QUrl baseUrl;
    QUrl nextUrl;  

    bool inEntry;
    bool inContent;
    bool inTitle;

public:
    OpdsParser(QVector<Item>& items_, const QString& bu);
    bool startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &attributes);
    bool endElement(const QString &namespaceURI, const QString &localName, const QString &qName);
    bool characters(const QString &str);
    bool endDocument();
    bool fatalError(const QXmlParseException& exception);
    QString errorString() const;

private:
    QUrl makeAbsoluteUrl( const QUrl& u ) const;

};

#endif // OPDSPARSER_H

