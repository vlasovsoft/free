#ifndef EDITCATALOG_H
#define EDITCATALOG_H

#include "Dialog.h"

namespace Ui {
class EditCatalog;
}

class EditCatalog : public Dialog
{
    Q_OBJECT

    QString& name;
    QString& url;

public:
    EditCatalog(QWidget *parent, QString& n, QString& u);
    ~EditCatalog();

protected slots:
    void accept();

private:
    Ui::EditCatalog *ui;
};

#endif // EDITCATALOG_H
