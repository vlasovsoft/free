#include "editcatalog.h"
#include "ui_editcatalog.h"

EditCatalog::EditCatalog(QWidget *parent, QString& n, QString& u)
    : Dialog(parent)
    , name(n)
    , url(u)
    , ui(new Ui::EditCatalog)
{
    ui->setupUi(this);
    init();

    ui->leName->setText(name);
    ui->leUrl->setText(url);
}

EditCatalog::~EditCatalog()
{
    delete ui;
}

void EditCatalog::accept()
{
    if (    !ui->leName->text().isEmpty()
         && !ui->leUrl->text().isEmpty() )
    {
        name = ui->leName->text();
        url  = ui->leUrl->text();
        Dialog::accept();
    }
}
