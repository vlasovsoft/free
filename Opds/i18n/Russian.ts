<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ru">
<context>
    <name>DownloadDialog</name>
    <message>
        <location filename="../downloaddialog.ui" line="14"/>
        <source>Download</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <location filename="../downloaddialog.ui" line="53"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>EditCatalog</name>
    <message>
        <location filename="../editcatalog.ui" line="14"/>
        <source>Edit</source>
        <translation>Ред</translation>
    </message>
    <message>
        <location filename="../editcatalog.ui" line="20"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../editcatalog.ui" line="30"/>
        <source>Url</source>
        <translation>Адрес</translation>
    </message>
</context>
<context>
    <name>OpdsBookWidget</name>
    <message>
        <location filename="../opdsbookwidget.cpp" line="75"/>
        <source>Save file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../opdsbookwidget.cpp" line="97"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../opdsbookwidget.cpp" line="97"/>
        <source>Download error!</source>
        <translation>Ошибка загрузки!</translation>
    </message>
</context>
<context>
    <name>OpdsItemWidget</name>
    <message>
        <location filename="../opdsitemwidget.ui" line="21"/>
        <source>Loading...</source>
        <translation>Загружаю...</translation>
    </message>
    <message>
        <location filename="../opdsitemwidget.cpp" line="66"/>
        <location filename="../opdsitemwidget.cpp" line="76"/>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save as</source>
        <translation type="vanished">Сохранить как</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="44"/>
        <source>OPDS Catalogs</source>
        <translation>OPDS Каталоги</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="56"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="63"/>
        <source>Edit</source>
        <translation>Ред</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="70"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="87"/>
        <source>Home</source>
        <translation>Домой</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="94"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="101"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>opdsparser</name>
    <message>
        <location filename="../opdsparser.cpp" line="147"/>
        <source>===&gt; Next</source>
        <translation>====&gt; Больше</translation>
    </message>
</context>
</TS>