#ifndef OPDSCATALOGITEM_H
#define OPDSCATALOGITEM_H

#include <QMap>
#include <QString>
#include <QListWidgetItem>

class OpdsCatalogItem: public QListWidgetItem
{
public:
    QMap<QString,QString> urls;
    QString desc;
    QString content;
    QString cType;

public:
    OpdsCatalogItem ( const QString& str );
    OpdsCatalogItem ( const QString& n, const QString& u );
    OpdsCatalogItem ( const QString& n, const QMap<QString,QString>& u );
};

#endif // OPDSCATALOGITEM_H
