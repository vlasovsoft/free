#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include <QObject>
#include <QString>
#include <QNetworkReply>
#include <QNetworkAccessManager>

class QFile;

class DownloadCallback
{
public:
    virtual void downloadFinished() = 0;
    virtual void downloadError() = 0;
    virtual void downloadProgress( int curr, int total ) = 0;
};

class Download : public QObject
{
    Q_OBJECT

public:
    Download( DownloadCallback* pCb );
    void setTarget(const QString& val)
    { target = val; }
    QString getFileName() const
    { return fileName; }
    void setFileName(const QString& val)
    { fileName = val; }
    QString getFileNameHint() const
    { return fileNameHint; }
    void setUserAgent(const QString& val)
    { userAgent = val; }
    QString getContentType() const
    { return contentType; }
    QString getTarget() const
    { return target; }

private:
    DownloadCallback* pCallBack;
    QNetworkReply* pReply;
    QFile* pFile;
    QString target;
    QString fileName;
    QString fileNameHint;
    QString userAgent;
    QString contentType;

public slots:
    void download();
    void cancel();

private slots:
    void downloadFinished();
    void downloadError( QNetworkReply::NetworkError code );
    void downloadProgress(qint64 recieved, qint64 total);
    void downloadDataReady();
    void sslErrors(const QList<QSslError> &errors);

private:
    void downloadInternal( const QUrl& url );
    void deleteReply( QNetworkReply* r );
};
#endif // DOWNLOAD_H
