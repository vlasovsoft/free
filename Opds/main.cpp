#include <Application.h>

#include "widget.h"

QWidget* g_pMainWindow;

int main(int argc, char *argv[])
{
    Application app(argc,argv);
    app.setApplicationName("opds");
    if ( !app.init() ) return 1;

    Widget w;
    g_pMainWindow = &w;

    return app.exec();
}
