#include <QUrl>
#include <QNetworkRequest>
#include <QSslConfiguration>
#include <QFile>
#include <QFileInfo>
#include <QRegExp>

#include "Log.h"
#include "QtUtils.h"
#include "wifidialog.h"
#include "Platform.h"

#include "download.h"

static QNetworkAccessManager* g_pManager;

Download::Download( DownloadCallback* pCb )
    : pCallBack(pCb)
    , pReply(NULL)
    , pFile(NULL)
{
    if ( !g_pManager )
        g_pManager = new QNetworkAccessManager();
}

void Download::downloadFinished()
{
    QNetworkReply* r = qobject_cast<QNetworkReply*>(sender());

    if ( NULL == r ) return;

    deleteReply(r);

    if ( g_pLog->getLevel() >= 2 ) {
        g_pLog->write(2, Log::MT_I, QString("Download::downloadFinished(): %1").arg(r->url().toString()));
    }
    if ( r->error() )
    {
        g_pLog->write(1, Log::MT_E, QString("Reply error: %1").arg(r->errorString()));
        return;
    }

    int status = r->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if ( status >= 300 && status < 400 ) // redirection
    {
        QUrl redir;
        QVariant val = r->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        if ( QVariant::Url == val.type() )
        {
            QUrl redir( val.toUrl() );
            if ( redir.isRelative() )
                redir = r->url().resolved( redir );
            if ( redir.isValid() )
            {
                if ( g_pLog->getLevel() >= 2 ) {
                    g_pLog->write(2, Log::MT_I, "===> redirect: %s", redir.toEncoded().constData());
                }
                if ( redir != r->url() )
                {
                    downloadInternal( redir );
                    return;
                }
            }
        }
    }

    QString cd(r->rawHeader("Content-Disposition"));
    if ( !cd.isEmpty() )
    {
        QRegExp re("filename=(.*)");
        if ( re.indexIn( cd ) != -1 )
        {
            fileNameHint = re.cap(1);
        }
    }

    // get content type
    contentType = r->header(QNetworkRequest::ContentTypeHeader).toString();

    // close file
    if ( pFile )
    {
        pFile->close();
        delete pFile;
        pFile = NULL;
    }

    // callback
    if ( pCallBack )
        pCallBack->downloadFinished();
}

void Download::downloadError(QNetworkReply::NetworkError code)
{
    Q_UNUSED(code)
    if ( pFile )
    {
        pFile->close();
        pFile->remove();
        delete pFile;
        pFile = NULL;
    }
    if ( pCallBack )
        pCallBack->downloadError();
}

void Download::download()
{
    downloadInternal( QUrl::fromEncoded( target.toLocal8Bit() ) );
}

void Download::cancel()
{
    if ( pReply )
    {
        pReply->abort();
    }
}

void Download::downloadProgress(qint64 recieved, qint64 total)
{
    if ( pCallBack ) pCallBack->downloadProgress( static_cast<int>(recieved), static_cast<int>(total) );
}

void Download::downloadDataReady()
{
    QNetworkReply* pReply = qobject_cast<QNetworkReply*>(sender());
    if ( !pReply ) return;
    if ( pFile != NULL )
        pFile->write( pReply->readAll() );
    Platform::get()->networkActivity();
}

void Download::sslErrors(const QList<QSslError>&)
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    reply->ignoreSslErrors();
}

void Download::downloadInternal(const QUrl &url)
{
    if ( g_pLog->getLevel() >= 2 ) {
        g_pLog->write(2, Log::MT_I, "downloadInternal(%s)", url.toEncoded().constData());
    }
    if ( pFile != NULL )
    {
        delete pFile;
        pFile = NULL;
    }
    pFile = new QFile(fileName);
    if ( pFile->open( QFile::WriteOnly ) )
    {
        QFileInfo fi(url.path());
        fileNameHint = fi.fileName();
        QNetworkRequest request(url);
        if ( !userAgent.isEmpty() )
            request.setRawHeader("User-Agent", userAgent.toLatin1());
        pReply = g_pManager->get(request);

        QSslConfiguration conf = request.sslConfiguration();
        conf.setProtocol(QSsl::AnyProtocol);
        conf.setPeerVerifyMode(QSslSocket::QueryPeer);
        request.setSslConfiguration(conf);

        QObject::connect(pReply, SIGNAL(finished()), this, SLOT(downloadFinished()));
        QObject::connect(pReply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
        QObject::connect(pReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(downloadError(QNetworkReply::NetworkError)));
        QObject::connect(pReply, SIGNAL(readyRead()), this, SLOT(downloadDataReady()));
        QObject::connect(pReply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));
    }
    else
    {
        delete pFile;
        pFile = NULL;
    }
}

void Download::deleteReply(QNetworkReply *r)
{
    pReply = NULL;
    r->disconnect(this);
    r->deleteLater();
}

