#include <QDir>
#include <QDebug>

#include "Config.h"
#include "Platform.h"
#include "QtUtils.h"
#include "messagebox.h"
#include "FileSaveDialog.h"

#include "opdscatalogitem.h"
#include "downloaddialog.h"

#include "opdsbookwidget.h"
#include "ui_opdsbookwidget.h"

extern QWidget* g_pMainWindow;

OpdsBookWidget::OpdsBookWidget(QWidget *parent, OpdsCatalogItem* p)
    : QWidget(parent)
    , pItem(p)
    , ui(new Ui::OpdsBookWidget)
{
    ui->setupUi(this);
    initTopLevelWidget(this);
    QMap<QString,QString>::Iterator i( pItem->urls.begin() );
    for( ; i != pItem->urls.end(); ++i )
    {
        if ( i.key() != "atom" )
           ui->listWidget->addItem(i.key());
    }
    QObject::connect( ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(listItemClicked(QListWidgetItem*)) );
    QObject::connect( ui->listWidget, SIGNAL(itemActivated(QListWidgetItem*)), this, SLOT(listItemClicked(QListWidgetItem*)) );
}

OpdsBookWidget::~OpdsBookWidget()
{
    delete ui;
}

void OpdsBookWidget::setContent(const QString &type, const QString &content)
{
    if ( type == "text/plain" )
        ui->textBrowser->setPlainText( content );
    else
        ui->textBrowser->setHtml( content );
}

void OpdsBookWidget::listItemClicked(QListWidgetItem *item)
{
    QMap<QString,QString>::Iterator iter( pItem->urls.find( item->text() ) );
    if ( iter != pItem->urls.end() )
    {
        if ( !::activateNetwork(g_pMainWindow) )
            return;

        DownloadDialog ddlg(this, iter.value());
#if defined(Q_WS_QWS)
        ddlg.setFullScreen(true);
#endif
        if ( ddlg.exec() )
        {
            QString fn(ddlg.getFileNameHint().trimmed());
            bool error = false;
            if ( !fn.isEmpty() )
            {
                // remove quotes
                if ( fn.startsWith("\"") && fn.endsWith("\"") )
                {
                    fn = fn.mid( 1, fn.length()-2 );
                }

                if ( fn.endsWith(".zip") || fn.endsWith(".epub") || fn.endsWith(".fb2") )
                {
                    QString path( g_pConfig->readQString( "save_path", Platform::get()->getRootPath() ) );
                    QString saveFileName(::getSaveFileName(this, tr("Save file"), path + QDir::separator() + fn, "*.*"));
                    if ( !saveFileName.isEmpty() ) {
                        qDebug() << "tempFileName: " << ddlg.getFileName();
                        qDebug() << "saveFileName: " << saveFileName;
                        QFile::rename(ddlg.getFileName(), saveFileName);
                        QString savePath(QFileInfo(saveFileName).absolutePath() );
                        if ( savePath != path )
                            g_pConfig->writeQString( "save_path", savePath );
                    }
                }
                else
                {
                    error = true;
                }
            }
            else
            {
                error = true;
            }

            if ( error )
            {
                ::messageBox( this, QMessageBox::Warning, tr("Error"), tr("Download error!") );
            }
        }
    }
}
