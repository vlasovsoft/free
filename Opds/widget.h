#ifndef WIDGET_H
#define WIDGET_H

#include "gesturescontroller.h"
#include "WidgetCommon.h"
#include "WidgetEventFilter.h"

namespace Ui {
class Widget;
}

class QKeyEvent;
class QListWidgetItem;
class OpdsCatalogItem;

class Widget : public WidgetCommon
{
    Q_OBJECT
    Ui::Widget *ui;
    
public:
    Widget();
    ~Widget();

private slots:
    void on_btnAdd_clicked();
    void on_btnEdit_clicked();
    void on_listOpdsCatalogs_itemClicked(QListWidgetItem *item);
    void on_listOpdsCatalogs_itemActivated(QListWidgetItem *item);
    void navigate(OpdsCatalogItem* pItem);
    void on_btnBack_clicked();
    void on_btnHome_clicked();

    void on_btnRemove_clicked();

private:
    void loadCatalogs();
    void saveCatalogs();
    virtual void keyPressEvent( QKeyEvent* e );
};

#endif // WIDGET_H
