#include <QDir>
#include <QDebug>

#include "Platform.h"
#include "QtUtils.h"

#include "downloaddialog.h"
#include "ui_downloaddialog.h"

DownloadDialog::DownloadDialog(QWidget *parent, const QString& url)
    : Dialog(parent)
    , downloader(this)
    , ui(new Ui::DownloadDialog)
{
    ui->setupUi(this);
    init();

    qDebug() << "Downloading: " << url;

    downloader.setTarget(url);
    downloader.setFileName(Platform::get()->getRootPath() + QDir::separator() + "file");
    downloader.download();
}

DownloadDialog::~DownloadDialog()
{
    delete ui;
}

void DownloadDialog::downloadFinished()
{
    accept();
}

void DownloadDialog::downloadError()
{
    qDebug() << "downloadError()";
}

void DownloadDialog::downloadProgress(int curr, int total)
{
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(total);
    ui->progressBar->setValue(curr);
}

void DownloadDialog::on_btnCancel_clicked()
{
    downloader.cancel();
    reject();
}
