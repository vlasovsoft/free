TARGET = dictionary
TEMPLATE = app
COMMON = ../common

include(../common/common.pri)
include(../common/i18n.pri)

SOURCES += main.cpp widget.cpp settings.cpp
HEADERS  += widget.h settings.h
FORMS    += widget.ui settings.ui

RESOURCES += images/images.qrc
