#include <QKeyEvent>
#include <QTimer>
#include <QScrollBar>

#include "Config.h"
#include "QtUtils.h"
#include "settings.h"

#include "widget.h"
#include "ui_widget.h"

Widget::Widget()
    : ui(new Ui::Widget)
{
    ui->setupUi(this);
    initTopLevelWidget(this);
#if defined(Q_WS_QWS)
    ui->verticalLayout->insertWidget(2, getVirtualKeyboard());
#endif
    show();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnExit_clicked()
{
    close();
}

void Widget::on_btnSettings_clicked()
{
    Settings(this).exec();
}

void Widget::on_lineEdit_returnPressed()
{
    ui->textBrowser->translate( ui->lineEdit->text() );
    ui->textBrowser->setFocus();
}
