#ifndef KEYBOARDPLUGIN_H
#define KEYBOARDPLUGIN_H

#include <QKbdDriverPlugin>
#include <QWSKeyboardHandler>

class KeyboardPlugin : public QKbdDriverPlugin
{
    Q_OBJECT
    
public:
    KeyboardPlugin(QObject* parent = 0);

    QStringList keys() const;
    QWSKeyboardHandler* create(const QString & key, const QString & device);
};


#endif // KEYBOARDPLUGIN_H
