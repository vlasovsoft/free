QT += core gui
TARGET = IvKeys
TEMPLATE = lib
CONFIG += plugin

INCLUDEPATH += $$PWD/../IvInput
DEPENDPATH  += $$PWD/../IvInput

SOURCES += keyboardplugin.cpp IvKeys.cpp
HEADERS += keyboardplugin.h   IvKeys.h

LIBS += -L../IvInput -lIvInput
