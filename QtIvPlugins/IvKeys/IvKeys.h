#ifndef IV_KEYS_H
#define IV_KEYS_H

#include <QWSKeyboardHandler>

class QEvent;
class IvInputEvent;

class IvKeys : public QObject, public QWSKeyboardHandler
{
    Q_OBJECT

    int keyOkRepeatCount;
    int keyNextRepeatCount;
    int keyPrevRepeatCount;

public:
    IvKeys(const QString & driver = QString(), const QString & device = QString(), QObject* parent = 0);
    virtual ~IvKeys();

protected:
    virtual void customEvent ( QEvent* event );

private:
    void processNormalKey( IvInputEvent* pEvent );
    void processExtendedKey( IvInputEvent* pEvent );
    void processExtendedKey( int eventType, int* repeatCount, int codeShort, int codeLong );
};


#endif // IV_KEYS_H
