#ifndef IVINPUTEVENT_H_
#define IVINPUTEVENT_H_

#include <QEvent>

class IvInputEvent : public QEvent
{
public:
    enum Type
    {
        evtKeyPress   = QEvent::User,
        evtKeyRelease,
        evtKeyRepeat,
        evtPointerDown,
        evtPointerUp,
        evtPointerMove,
        evtQuit
    };

    int param1;
    int param2;

public:
    IvInputEvent( Type t, int p1, int p2 )
        : QEvent(static_cast<QEvent::Type>(t))
        , param1(p1)
        , param2(p2)
    {}
};

#endif /* IVINPUTEVENT_H_ */
