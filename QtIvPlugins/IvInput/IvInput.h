#ifndef IVINPUT_H_
#define IVINPUT_H_

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QTimer>
#include <QWSMouseHandler>

class IvInputEvent;

class IvInput : public QThread
{
    Q_OBJECT

private:
    QWaitCondition cond;
    QMutex mutex;

    QRect updateRect;

    QObject* pKeyboardHandler;
    QObject* pStylusHandler;
    QObject* pTimerHandler;

    QTimer idleTimer;

    volatile unsigned char quitReason; // 0-none, 1-qt, 2-iv

    IvInput();

public:
    static IvInput* instanse();
    static void destroy();

    virtual ~IvInput();

    void run();

    void waitReady()
    {
        mutex.lock();
        cond.wait(&mutex);
        mutex.unlock();
    }

    void setReady()
    { cond.wakeAll(); }

    void event( int type, int par1, int par2 );

    void setKeyboardHandler( QObject* pObj )
    { pKeyboardHandler = pObj; }

    void setStylusHandler( QObject* pObj )
    { pStylusHandler = pObj; }

    void setTimerHandler( QObject* pObj )
    { pTimerHandler = pObj; }

    void scheduleScreenUpdate(const QRect& rect);

    void startTimer(QObject* pOwner, int ms);
    void stopTimer();
    void onTimer();

    const char* getSerialNumber() const;

    unsigned char getQuitReason() const
    { return quitReason; }

    void setQuitReason( unsigned char val )
    { quitReason = val; }
};

#endif /* IVINPUT_H_ */
