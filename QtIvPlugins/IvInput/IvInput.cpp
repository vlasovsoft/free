#include <csetjmp>

#include <QTime>
#include <QScreen>
#include <QCoreApplication>
#include <QDebug>

#include <inkview.h>

#include "IvInput.h"
#include "IvInputEvent.h"

#define EVT_CLOSE 1000

namespace
{
    IvInput* pInput;
    jmp_buf jmp_env;
    int jmp_val = 0;
}

IvInput* IvInput::instanse()
{
    if ( !pInput )
    {
        pInput = new IvInput();
        pInput->start();
    }
    return pInput;
}

void IvInput::destroy()
{
    if ( pInput )
    {
        delete pInput;
        pInput = 0;
    }
}

namespace
{

void timerproc()
{
    IvInput::instanse()->onTimer();
}

int main_handler(int type, int par1, int par2)
{
    switch( type )
    {
        case EVT_SHOW:
            ::iv_sleepmode(0);
            break;

        case EVT_KEYPRESS:
        case EVT_KEYRELEASE:
        case EVT_KEYREPEAT:
        case EVT_POINTERDOWN:
        case EVT_POINTERUP:
        case EVT_POINTERMOVE:
            ::iv_sleepmode(0);
            IvInput::instanse()->event(type,par1,par2);
            break;

        case EVT_CLOSE:
            ::CloseApp();
            break;

        case EVT_EXIT:
            longjmp(jmp_env,1);
            break;

        default:
            break;
    }

    return 0;
}

}

IvInput::IvInput()
    : pKeyboardHandler(0)
    , pStylusHandler(0)
    , quitReason(0)
{
    connect(&idleTimer, SIGNAL(timeout()), this, SLOT(onIdleTimer()));
    idleTimer.setInterval(30000); // 30 sec
}

IvInput::~IvInput()
{
    if ( 0 == getQuitReason() )
    {
        setQuitReason(1); // qt
        ::SendEvent(main_handler,EVT_CLOSE,0,0);
        wait();
    }
}

void IvInput::run()
{
    jmp_val = setjmp(jmp_env);
    if ( 0 == jmp_val )
    {
        InkViewMain(main_handler);
    }
    if ( 0 == getQuitReason() )
    {
        setQuitReason(2);
        QCoreApplication::quit();
    }
}

void IvInput::event( int type, int par1, int par2 )
{
    switch(type)
    {
        case EVT_KEYPRESS:
            if (pKeyboardHandler)
                QCoreApplication::postEvent(pKeyboardHandler, new IvInputEvent(IvInputEvent::evtKeyPress,par1,par2));
            break;
        case EVT_KEYRELEASE:
            if (pKeyboardHandler)
                QCoreApplication::postEvent(pKeyboardHandler, new IvInputEvent(IvInputEvent::evtKeyRelease,par1,par2));
            break;
        case EVT_KEYREPEAT:
            if (pKeyboardHandler)
                QCoreApplication::postEvent(pKeyboardHandler, new IvInputEvent(IvInputEvent::evtKeyRepeat,par1,par2));
            break;
        case EVT_POINTERDOWN:
            if (pStylusHandler)
                QCoreApplication::postEvent(pStylusHandler, new IvInputEvent(IvInputEvent::evtPointerDown,par1,par2));
            break;
        case EVT_POINTERUP:
            if (pStylusHandler)
                QCoreApplication::postEvent(pStylusHandler, new IvInputEvent(IvInputEvent::evtPointerUp,par1,par2));
            break;
        case EVT_POINTERMOVE:
            if (pStylusHandler)
                QCoreApplication::postEvent(pStylusHandler, new IvInputEvent(IvInputEvent::evtPointerMove,par1,par2));
            break;
    }
}

void IvInput::scheduleScreenUpdate(const QRect& rect)
{
    QScreen::instance()->setDirty(rect);
}

void IvInput::startTimer( QObject* pOwner, int ms )
{
    setTimerHandler( pOwner );
    ::SetHardTimer("timer", timerproc, ms);
}

void IvInput::stopTimer()
{
    ::ClearTimer(timerproc);
}

void IvInput::onTimer()
{
    QTimer::singleShot(0, pTimerHandler, SLOT(onTimer()));
}

const char* IvInput::getSerialNumber() const
{
    return ::GetSerialNumber();
}
