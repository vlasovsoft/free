#include <inkview.h>

#include "IvInput.h"
#include "IvInputEvent.h"

#include "IvStylus.h"

IvStylus::IvStylus(const QString& /*driver*/, const QString & device, QObject* parent)
    : QObject(parent)
    , QWSMouseHandler(device)
{
    IvInput::instanse()->setStylusHandler(this);
}

IvStylus::~IvStylus()
{
    IvInput::destroy();
}

void IvStylus::customEvent( QEvent* event )
{
    int state = 0;
    int type = static_cast<IvInputEvent::Type>(event->type());
    IvInputEvent* pEvent = static_cast<IvInputEvent*>(event);
    if (    IvInputEvent::evtPointerDown == type
         || IvInputEvent::evtPointerMove == type )
    {
        state = Qt::LeftButton;
    }
    mouseChanged ( QPoint(pEvent->param1, pEvent->param2), state );
}

