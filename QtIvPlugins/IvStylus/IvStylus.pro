QT += core gui
TARGET = IvStylus
TEMPLATE = lib
CONFIG += plugin

INCLUDEPATH += $$PWD/../IvInput

SOURCES += stylusplugin.cpp IvStylus.cpp
HEADERS += stylusplugin.h   IvStylus.h

LIBS += -L../IvInput -lIvInput
