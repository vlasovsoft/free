#include "stylusplugin.h"
#include "IvStylus.h"

StylusPlugin::StylusPlugin(QObject* parent) 
    : QMouseDriverPlugin(parent)
{}

QStringList StylusPlugin::keys() const
{
    QStringList list;
    list << QLatin1String("IvStylus");
    return list;
}

QWSMouseHandler* StylusPlugin::create(const QString & key, const QString & device)
{
    if (key.toLower() == QLatin1String("ivstylus"))
    {
        return new IvStylus(key, device);
    }

    return 0;
}

Q_EXPORT_PLUGIN2(IvStylus, StylusPlugin)
