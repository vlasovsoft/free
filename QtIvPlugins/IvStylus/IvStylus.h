#ifndef IV_STYLUS_H
#define IV_STYLUS_H

#include <QWSMouseHandler>

class IvStylus : public QObject, public QWSMouseHandler
{
    Q_OBJECT

public:
    IvStylus(const QString & driver = QString(), const QString & device = QString(), QObject* parent = 0);
    virtual ~IvStylus();
    
    virtual void suspend() {}
    virtual void resume() {}

protected:
    virtual void customEvent ( QEvent* event );
};


#endif // IV_STYLUS_H
