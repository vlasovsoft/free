QT += core gui
TARGET = IvScreen
TEMPLATE = lib
CONFIG += plugin

INCLUDEPATH += $$PWD/../IvInput

SOURCES += screenplugin.cpp IvScreen.cpp
HEADERS += screenplugin.h   IvScreen.h  ScreenManager.h

LIBS += -L../IvInput -lIvInput
