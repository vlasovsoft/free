#include <QDebug>
#include <QTime>

#include <inkview.h>

#include "IvScreen.h"

IvScreen::IvScreen()
    : QScreen(0)
{
    setSingleShot(true);
    QObject::connect(this, SIGNAL(timeout()), this, SLOT(update()));
}

IvScreen::~IvScreen()
{
}

bool IvScreen::initDevice()
{
    return true;
}

void IvScreen::shutdownDevice()
{
    QScreen::shutdownDevice();
}

void IvScreen::setDirty ( const QRect& r )
{
    if ( isDebug ) qDebug("IvScreen::setDirty(%d, %d, %d, %d)", r.x(), r.y(), r.width(), r.height());
    updateRect = updateRect.united(r);
    start(50);
}

bool IvScreen::connect ( const QString& displaySpec )
{
    const QStringList args = displaySpec.split(QLatin1Char(':'));
    isDebug = args.contains("debug");

    ::OpenScreen();
    ::SetPanelType(0);
    ::PanelHeight(); 
    ::ClearScreen();

    icanvas* pCanvas = ::GetCanvas();

    grayscale = true;

    data = pCanvas->addr;
    lstep = pCanvas->scanline;

    dw = w = pCanvas->width;
    dh = h = pCanvas->height;

    // TODO: 9 inches devices
    physHeight = 120; // mm
    physWidth  = 90;  // mm

    d = pCanvas->depth;

    mapsize = lstep * h;

    setPixelFormat(QImage::Format_Indexed8);

    return true;
}

void IvScreen::update()
{
    bool full = isFullUpdate();

    if ( full )
       ::FullUpdate();
    else
        ::PartialUpdate( updateRect.x(),updateRect.y(),updateRect.width(),updateRect.height() );

    if ( isDebug )
        qDebug() << "IvScreen::update():" << updateRect << " full:" << full;

    updateRect = QRect();
}

bool IvScreen::isFullUpdate() const
{
    return 0 == updateRect.x()
        && 0 == updateRect.y()
        && width() == updateRect.width()
        && height() == updateRect.height();
}

QT_END_NAMESPACE
