#include <Application.h>
#include <QtUtils.h>
#include <messagebox.h>

int main(int argc, char *argv[])
{
    if ( argc < 3 ) return 1;
    Application app(argc, argv);
    app.setApplicationName("msgbox");
    if ( !app.init() ) return 1;

    ::messageBox( NULL, QMessageBox::Information, argv[1], argv[2] );

    return 0;
}
