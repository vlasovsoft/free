#include "widget.h"
#include "Application.h"

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.setApplicationName("konsole");
    if ( !app.init() ) return 1;
    Widget w;
    app.exec();
    return 0;
}
