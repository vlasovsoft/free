#ifndef WIDGET_H
#define WIDGET_H

#include <QProcess>

#include "WidgetCommon.h"

namespace Ui {
class Widget;
}

class QKeyEvent;
class QCloseEvent;

class Widget : public WidgetCommon
{
    Q_OBJECT
    
public:
    Widget();
    ~Widget();

private:
    void sendCommand( const QString& cmd );

private slots:
    void onNew();
    void onZoomOut();
    void onZoomIn();
    void readyReadStandardError();
    void readyReadStandardOutput();
    void finished(int);
    void on_btnExit_clicked();
    void on_lineEdit_returnPressed();

private:
    Ui::Widget *ui;
    QProcess process;
};

#endif // WIDGET_H
