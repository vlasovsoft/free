TARGET = konsole
TEMPLATE = app
COMMON = ../common

include(../common/common.pri)

SOURCES += main.cpp widget.cpp
HEADERS  += widget.h
FORMS    += widget.ui
RESOURCES += images/images.qrc
