#include <QDebug>
#include <QTextDocumentWriter>
#include <QLineEdit>
#include <QScrollBar>

#include "Config.h"
#include "QtUtils.h"
#include "Platform.h"

#include "FileOpenDialog.h"
#include "FileSaveDialog.h"

#include "widget.h"
#include "ui_widget.h"

Widget::Widget()
    : ui(new Ui::Widget)

{
    ui->setupUi(this);

    ui->plainTextEdit->setReadOnly(true);

    initTopLevelWidget(this);

    connect( ui->btnNew, SIGNAL(clicked()), this, SLOT(onNew()) );
    connect( ui->btnZoomOut, SIGNAL(clicked()), this, SLOT(onZoomOut()) );
    connect( ui->btnZoomIn, SIGNAL(clicked()), this, SLOT(onZoomIn()) );
    connect( ui->btnExit, SIGNAL(clicked()), this, SLOT(close()) );

    int iconSize = mm_to_px(10);
    ui->btnNew->setIconSize(QSize(iconSize,iconSize));
    ui->btnZoomOut->setIconSize(QSize(iconSize,iconSize));
    ui->btnZoomIn->setIconSize(QSize(iconSize,iconSize));
    ui->btnExit->setIconSize(QSize(iconSize,iconSize));

    QFont font(ui->plainTextEdit->font());
    font.setFamily("DejaVu Sans Mono");
    font.setPointSize( g_pConfig->readInt("font.size", 14) );
    ui->plainTextEdit->setFont(font);
    ui->plainTextEdit->setWordWrapMode( QTextOption::NoWrap );
    ui->plainTextEdit->setFocusPolicy( Qt::StrongFocus );

    QObject::connect( &process, SIGNAL(readyReadStandardError()), this, SLOT(readyReadStandardError()) );
    QObject::connect( &process, SIGNAL(readyReadStandardOutput()), this, SLOT(readyReadStandardOutput()) );
    QObject::connect( &process, SIGNAL(finished(int)), this, SLOT(finished(int)) );

#if defined(Q_WS_QWS)
    ui->verticalLayout->insertWidget(2, getVirtualKeyboard());
#endif

    process.start("/bin/sh");
    process.waitForStarted();

    show();
}

Widget::~Widget()
{
    process.kill();
    process.waitForFinished();
    delete ui;
}

void Widget::sendCommand(const QString &cmd)
{
    ui->plainTextEdit->appendPlainText(QString("#") + cmd);
    ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    process.write( cmd.toLocal8Bit() );
    process.write("\n");
    ui->lineEdit->clear();
}

void Widget::onNew()
{
    ui->plainTextEdit->clear();
}

void Widget::onZoomOut()
{
    int fontSize = g_pConfig->readInt("font.size", 14);
    if ( fontSize > 5 )
    {
        fontSize --;
        QFont f( ui->plainTextEdit->font() );
        f.setPointSize( fontSize );
        ui->plainTextEdit->setFont(f);
        g_pConfig->writeInt("font.size", fontSize);
    }
}

void Widget::onZoomIn()
{
    int fontSize = g_pConfig->readInt("font.size", 14);
    if ( fontSize < 100 )
    {
        fontSize ++;
        QFont f( ui->plainTextEdit->font() );
        f.setPointSize( fontSize );
        ui->plainTextEdit->setFont(f);
        g_pConfig->writeInt("font.size", fontSize);
    }
}

void Widget::readyReadStandardError()
{
    ui->plainTextEdit->appendPlainText(process.readAllStandardError());
}

void Widget::readyReadStandardOutput()
{
    ui->plainTextEdit->appendPlainText(process.readAllStandardOutput());
}

void Widget::finished(int)
{
    close();
}

void Widget::on_btnExit_clicked()
{
    if ( QProcess::Running == process.state() ) {
        process.kill();
        process.waitForFinished();
    }
}

void Widget::on_lineEdit_returnPressed()
{
    QString cmd( ui->lineEdit->text() );
    if ( !cmd.isEmpty() )
    {
        qDebug() << cmd;
        sendCommand(cmd);
    }
}
