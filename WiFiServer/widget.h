#ifndef WIDGET_H
#define WIDGET_H

#include <QTimer>

#include "WidgetCommon.h"

namespace Ui {
class Widget;
}

class Widget : public WidgetCommon
{
    Q_OBJECT

public:
    Widget();
    ~Widget();

private slots:
    void on_btnExit_clicked();
    void init();
    void activity();


private:
    Ui::Widget *ui;
    QTimer activityTimer;
    QString fifo;
};

#endif // WIDGET_H
