TARGET = wifi_server
TEMPLATE = app
COMMON = ../common

include(../common/common.pri)
include(../common/i18n.pri)

SOURCES += main.cpp widget.cpp
HEADERS += widget.h
FORMS += widget.ui

RESOURCES += images/images.qrc
