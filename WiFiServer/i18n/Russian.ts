<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ru">
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>WiFi Server</source>
        <translation>WiFi Сервер</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="20"/>
        <source>&lt;b&gt;WiFi Server&lt;/b&gt;</source>
        <translation>&lt;b&gt;WiFi Сервер&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="44"/>
        <source>WiFi Server is started...</source>
        <translation>WiFi Сервер запущен...</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="48"/>
        <source>IP Address: </source>
        <translation>IP Адрес: </translation>
    </message>
</context>
</TS>