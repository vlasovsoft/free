<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="nl">
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>WiFi Server</source>
        <translation>WiFi Server</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="20"/>
        <source>&lt;b&gt;WiFi Server&lt;/b&gt;</source>
        <translation>&lt;b&gt;WiFi Server&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="44"/>
        <source>WiFi Server is started...</source>
        <translation>WiFi Server gestart...</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="48"/>
        <source>IP Address: </source>
        <translation>IP Adres: </translation>
    </message>
</context>
</TS>