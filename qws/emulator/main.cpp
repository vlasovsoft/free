#include <QApplication>

#include "widget.h"

int main(int argc, char *argv[])
{    
    QApplication a(argc, argv);
    Widget w;
    if ( w.isInitialized() ) {
        w.show();
        return a.exec();
    } else {
        return 1;
    }
}
