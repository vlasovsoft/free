#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>

class QSocketNotifier;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    bool isInitialized() const { return init; }

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private slots:
    void activity(int);

private:
    int tsFd;
    int fbFd;
    int ffFd;
    QSocketNotifier* snFifo;
    uchar *mmapData;
    int mmapOffset;
    int mmapSize;
    QImage* img;
    bool init;
};

#endif // WIDGET_H
