#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/fb.h>

#include <QDebug>
#include <QPainter>
#include <QMouseEvent>
#include <QSocketNotifier>

#include "widget.h"

#define MULTI_TOUCH 1

static int determineDepth(const fb_var_screeninfo &vinfo)
{
    int depth = vinfo.bits_per_pixel;
    if (depth== 24) {
        depth = vinfo.red.length + vinfo.green.length + vinfo.blue.length;
        if (depth <= 0)
            depth = 24; // reset if color component lengths are not reported
    } else if (depth == 16) {
        depth = vinfo.red.length + vinfo.green.length + vinfo.blue.length;
        if (depth <= 0)
            depth = 16;
    }
    return depth;
}

static QRect determineGeometry(const fb_var_screeninfo &vinfo)
{
    int xoff = vinfo.xoffset;
    int yoff = vinfo.yoffset;
    int w, h;
    w = vinfo.xres;
    h = vinfo.yres;
    if (w == 0 || h == 0) {
        qWarning("Unable to find screen geometry, using 320x240");
        w = 320;
        h = 240;
    }
    return QRect(xoff, yoff, w, h);
}

static QImage::Format determineFormat(const fb_var_screeninfo &info, int depth)
{
    const fb_bitfield rgba[4] = { info.red, info.green,
                                  info.blue, info.transp };

    QImage::Format format = QImage::Format_Invalid;

    switch (depth) {
    case 32: {
        const fb_bitfield argb8888[4] = {{16, 8, 0}, {8, 8, 0},
                                         {0, 8, 0}, {24, 8, 0}};
        const fb_bitfield abgr8888[4] = {{0, 8, 0}, {8, 8, 0},
                                         {16, 8, 0}, {24, 8, 0}};
        if (memcmp(rgba, argb8888, 4 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_ARGB32;
        } else if (memcmp(rgba, argb8888, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB32;
        } else if (memcmp(rgba, abgr8888, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB32;
            // pixeltype = BGRPixel;
        }
        break;
    }
    case 24: {
        const fb_bitfield rgb888[4] = {{16, 8, 0}, {8, 8, 0},
                                       {0, 8, 0}, {0, 0, 0}};
        const fb_bitfield bgr888[4] = {{0, 8, 0}, {8, 8, 0},
                                       {16, 8, 0}, {0, 0, 0}};
        if (memcmp(rgba, rgb888, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB888;
        } else if (memcmp(rgba, bgr888, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_BGR888;
            // pixeltype = BGRPixel;
        }
        break;
    }
    case 18: {
        const fb_bitfield rgb666[4] = {{12, 6, 0}, {6, 6, 0},
                                       {0, 6, 0}, {0, 0, 0}};
        if (memcmp(rgba, rgb666, 3 * sizeof(fb_bitfield)) == 0)
            format = QImage::Format_RGB666;
        break;
    }
    case 16: {
        const fb_bitfield rgb565[4] = {{11, 5, 0}, {5, 6, 0},
                                       {0, 5, 0}, {0, 0, 0}};
        const fb_bitfield bgr565[4] = {{0, 5, 0}, {5, 6, 0},
                                       {11, 5, 0}, {0, 0, 0}};
        if (memcmp(rgba, rgb565, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB16;
        } else if (memcmp(rgba, bgr565, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB16;
            // pixeltype = BGRPixel;
        }
        break;
    }
    case 15: {
        const fb_bitfield rgb1555[4] = {{10, 5, 0}, {5, 5, 0},
                                        {0, 5, 0}, {15, 1, 0}};
        const fb_bitfield bgr1555[4] = {{0, 5, 0}, {5, 5, 0},
                                        {10, 5, 0}, {15, 1, 0}};
        if (memcmp(rgba, rgb1555, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB555;
        } else if (memcmp(rgba, bgr1555, 3 * sizeof(fb_bitfield)) == 0) {
            format = QImage::Format_RGB555;
            // pixeltype = BGRPixel;
        }
        break;
    }
    case 12: {
        const fb_bitfield rgb444[4] = {{8, 4, 0}, {4, 4, 0},
                                       {0, 4, 0}, {0, 0, 0}};
        if (memcmp(rgba, rgb444, 3 * sizeof(fb_bitfield)) == 0)
            format = QImage::Format_RGB444;
        break;
    }
    case 8:
        format = QImage::Format_Grayscale8;
        break;
    case 1:
        format = QImage::Format_Mono;
        break;
    default:
        break;
    }

    return format;
}

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , fbFd(-1)
    , mmapData(NULL)
    , mmapOffset(0)
    , mmapSize(0)
    , img(NULL)
    , init(false)
{
    QByteArray tsdev("/dev/vts");
    tsFd = ::open(tsdev.constData(), O_WRONLY);
    if ( tsFd == -1 ) {
        qErrnoWarning(errno, "Error opening %s", tsdev.constData());
        return;
    }

    QByteArray fbdev("/dev/fb1");
    fbFd = ::open(fbdev.constData(), O_RDWR);
    if ( fbFd == -1 ) {
        qErrnoWarning(errno, "Error opening %s", fbdev.constData());
        return;
    }

    QByteArray ffdev("/tmp/vlasovsoft_fifo");
    ffFd = ::open(ffdev.constData(), O_RDWR);
    if ( ffFd == -1 ) {
        qErrnoWarning(errno, "Error opening %s", ffdev.constData());
        return;
    }

    snFifo = new QSocketNotifier(ffFd, QSocketNotifier::Read);
    QObject::connect(snFifo, SIGNAL(activated(int)), this, SLOT(activity(int)));

    fb_fix_screeninfo finfo;
    fb_var_screeninfo vinfo;
    memset(&vinfo, 0, sizeof(vinfo));
    memset(&finfo, 0, sizeof(finfo));

    if (ioctl(fbFd, FBIOGET_FSCREENINFO, &finfo) != 0) {
        qErrnoWarning(errno, "Error reading fixed information");
        return;
    }

    if (ioctl(fbFd, FBIOGET_VSCREENINFO, &vinfo)) {
        qErrnoWarning(errno, "Error reading variable information");
        return;
    }

    int depth = ::determineDepth(vinfo);
    QRect geometry = ::determineGeometry(vinfo);
    QImage::Format format = ::determineFormat(vinfo, depth);
    mmapSize = finfo.smem_len;
    mmapOffset = geometry.y() * finfo.line_length + geometry.x() * depth / 8;

    uchar *data = (unsigned char *)mmap(0, mmapSize, PROT_READ | PROT_WRITE, MAP_SHARED, fbFd, 0);
    if ((long)data == -1) {
        qErrnoWarning(errno, "Failed to mmap framebuffer");
        return;
    }

    mmapData = data + mmapOffset;

    setFixedSize(geometry.size());
    img = new QImage(data, geometry.width(), geometry.height(), format);

    init = true;
}

Widget::~Widget()
{
    if (tsFd != -1) {
        ::close(tsFd);
    }
    if (fbFd != -1) {
        if (mmapData != NULL)
            munmap(mmapData - mmapOffset, mmapSize);
        ::close(fbFd);
    }
}

void Widget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *img);
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    char buf[128];
#if defined(MULTI_TOUCH)
    int size = snprintf(buf, 128, "s 0\nT 1\nX %d\nY %d\nS 0\n", event->x(), event->y());
#else
    int size = snprintf(buf, 128, "x %d\ny %d\nd 0\nS 0\n", event->x(), event->y());
#endif
    ::write(tsFd, buf, size);
}

void Widget::mouseReleaseEvent(QMouseEvent *event)
{
    char buf[128];
#if defined(MULTI_TOUCH)
    int size = snprintf(buf, 128, "s 0\nT -1\nX %d\nY %d\nS 0\n", event->x(), event->y());
#else
    int size = snprintf(buf, 128, "x %d\ny %d\nu 0\nS 0\n", event->x(), event->y());
#endif
    ::write(tsFd, buf, size);
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    char buf[128];
#if defined(MULTI_TOUCH)
    int size = snprintf(buf, 128, "s 0\nX %d\nY %d\nS 0\n", event->x(), event->y());
#else
    int size = snprintf(buf, 128, "x %d\ny %d\nS 0\n", event->x(), event->y());
#endif
    ::write(tsFd, buf, size);
}

void Widget::activity(int)
{
    QByteArray buf;
    char c = '\0';
    while ( 1 == read(ffFd, &c, 1) && c != '\n') {
        buf += c;
    }
    if ( buf == "update" ) {
        update();
    }
}
