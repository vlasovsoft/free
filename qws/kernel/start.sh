#!/bin/sh

sudo insmod vts.ko
sudo chown root:input /dev/vts
sudo chmod g+w /dev/vts
sudo insmod vfb.ko vfb_enable=1
mkfifo /tmp/vlasovsoft_fifo

sleep 1s

xinput disable "Virtual touchscreen"
fbset -fb /dev/fb1 --geometry 600 800 600 800 8

