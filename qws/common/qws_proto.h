#ifndef QWS_PROTO_H
#define QWS_PROTO_H

#define QWS_SERVER "vlasovsoft_qws"
#define QWS_FIFO   "/tmp/vlasovsoft_fifo"

class QRect;
class QPoint;
class QRegion;
class QLocalSocket;

enum QWSCommand {
    qwsNone,

    // client -> server
    qwsWindowCreate,
    qwsWindowVisible,
    qwsWindowGeometry,
    qwsWindowFlags,
    qwsWindowState,
    qwsWindowMask,
    qwsWindowRaise,
    qwsWindowLower,
    qwsWindowFlushRequest,

    // server -> client
    qwsWindowActivate,
    qwsMouseEvent,
    qwsKeyEvent,
    qwsRotationEvent,
    qwsWindowFlushResponse,
    qwsWindowExpose
};

void sock_read( QLocalSocket* sock, void* data, int size );
void sock_write( QLocalSocket* sock, const void* data, int size );

void sock_write_int( QLocalSocket* sock, int val );
void sock_read_int( QLocalSocket* sock, int& val );

void sock_write_point( QLocalSocket* sock, const QPoint& val );
void sock_read_point( QLocalSocket* sock, QPoint& val );

void sock_write_rect( QLocalSocket* sock, const QRect& val );
void sock_read_rect( QLocalSocket* sock, QRect& val );

void sock_write_region( QLocalSocket* sock, const QRegion& val );
void sock_read_region( QLocalSocket* sock, QRegion& val );

// qwsWindowCreate, client -> server
void createWindowClient( QLocalSocket* sock, int id, const QRect& geom );
int createWindowServer(  QLocalSocket* sock, QRect& geom );

// qwsWindowFlushResponse, server -> client
void flushWindowResponseServer( QLocalSocket* sock, int id, const QRegion& r );
int flushWindowResponseClient( QLocalSocket* sock, QRegion& r );

// qwsWindowFlushRequest, client -> server
void flushWindowRequestClient( QLocalSocket* sock, int id, const QRegion& r );
int flushWindowRequestServer( QLocalSocket* sock, QRegion& r );

// qwsWindowExpose, server -> client
void exposeWindowServer( QLocalSocket* sock, int id, const QRect& r );
int exposeWindowClient( QLocalSocket* sock, QRect& r );

// qwsWindowVisible, client -> server
void setWindowVisibleClient( QLocalSocket* sock, int id, bool visible, bool modal);
int setWindowVisibleServer( QLocalSocket* sock, bool& visible, bool& modal);

// qwsWindowGeometry, client -> server
void setWindowGeometryClient( QLocalSocket* sock, int id, const QRect& geom);
int setWindowGeometryServer( QLocalSocket* sock, QRect& geom);

// qwsMouseEvent, server -> client
void sendMouseEventServer( QLocalSocket* sock, int id, const QPoint& lpt, const QPoint& gpt, int type );
int readMouseEventClient( QLocalSocket* sock, QPoint& lpt, QPoint& gpt, int& type );

// qwsWindowFlags, client -> server
void setWindowFlagsClient(QLocalSocket* sock, int id, int flags);
int setWindowFlagsServer(QLocalSocket* sock, int& flags);

// qwsWindowState, client -> server
void setWindowStateClient(QLocalSocket* sock, int id, int state);
int setWindowStateServer(QLocalSocket* sock, int& state);

// qwsWindowMask, client -> server
void setWindowMaskClient(QLocalSocket* sock, int id, const QRegion& mask);
int setWindowMaskServer(QLocalSocket* sock, QRegion& mask);

// qwsWindowRaise, client -> server
void raiseWindowClient(QLocalSocket* sock, int id);
int raiseWindowServer(QLocalSocket* sock);

// qwsWindowLower, client -> server
void lowerWindowClient(QLocalSocket* sock, int id);
int lowerWindowServer(QLocalSocket* sock);


#endif // QWS_PROTO_H
