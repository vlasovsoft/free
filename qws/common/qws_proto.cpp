#include <QRect>
#include <QRegion>
#include <QLocalSocket>

#include "qws_proto.h"

void sock_read( QLocalSocket* sock, void* data, int size )
{
    if ( QLocalSocket::ConnectedState == sock->state() )
    {
        int cnt = 0;
        while ( cnt < size )
        {
            if ( sock->bytesAvailable() > 0 )
                cnt += sock->read((char*)data+cnt, size-cnt);
            else
                sock->waitForReadyRead();
        }
    }
}

void sock_write( QLocalSocket* sock, const void* data, int size )
{
    if ( QLocalSocket::ConnectedState == sock->state() )
    {
        int cnt = 0;
        while ( cnt < size )
        {
            int count = sock->write((const char*)data+cnt, size-cnt);
            if ( count > 0 ) cnt += count;
        }
    }
}

void sock_write_int(QLocalSocket *sock, int val)
{
    ::sock_write( sock, &val, sizeof(val) );
}

void sock_read_int(QLocalSocket *sock, int &val)
{
    ::sock_read( sock, &val, sizeof(val));
}

void sock_write_rect(QLocalSocket *sock, const QRect &val)
{
    ::sock_write( sock, &val, sizeof(val) );
}

void sock_read_rect(QLocalSocket *sock, QRect &val)
{
    ::sock_read( sock, &val, sizeof(val) );
}

void sock_write_region(QLocalSocket *sock, const QRegion& val)
{
    ::sock_write_int( sock, val.rectCount() );
    for ( const auto& r: val ) {
        ::sock_write_rect( sock, r );
    }
}

void sock_read_region(QLocalSocket *sock, QRegion &val)
{
    int size = 0;
    ::sock_read_int( sock, size );
    for ( int i=0; i<size; i++ ) {
        QRect r;
        ::sock_read_rect( sock, r );
        val += r;
    }
}

void flushWindowResponseServer(QLocalSocket* sock, int id, const QRegion& r)
{
    ::sock_write_int(sock, qwsWindowFlushResponse);
    ::sock_write_int(sock, id);
    ::sock_write_region(sock, r);
    sock->flush();
}

int flushWindowResponseClient(QLocalSocket *sock, QRegion& r)
{
    int id = 0;
    ::sock_read_int( sock, id );
    ::sock_read_region(sock, r);
    return id;
}

void setWindowVisibleClient(QLocalSocket *sock, int id, bool visible, bool modal)
{
    ::sock_write_int(sock, qwsWindowVisible);
    ::sock_write_int(sock, id);
    ::sock_write(sock, &visible, sizeof(visible));
    ::sock_write(sock, &modal, sizeof(modal));
    sock->flush();
}

int setWindowVisibleServer(QLocalSocket *sock, bool& visible, bool& modal)
{
    int id = 0;
    ::sock_read_int(sock, id);
    ::sock_read(sock, &visible, sizeof(visible));
    ::sock_read(sock, &modal, sizeof(modal));
    return id;
}

void sendMouseEventServer(QLocalSocket* sock, int id, const QPoint& lpt, const QPoint& gpt, int type)
{
    ::sock_write_int( sock, qwsMouseEvent );
    ::sock_write_int( sock, id );
    ::sock_write( sock, &lpt, sizeof(lpt) );
    ::sock_write( sock, &gpt, sizeof(gpt) );
    ::sock_write( sock, &type, sizeof(type) );
    sock->flush();
}

int readMouseEventClient(QLocalSocket* sock, QPoint& lpt, QPoint& gpt, int& type)
{
    int id = 0;
    ::sock_read_int( sock, id);
    ::sock_read_point( sock, lpt );
    ::sock_read_point( sock, gpt );
    ::sock_read_int( sock, type );
    return id;
}

void activateWindowServer(QLocalSocket *sock)
{
    ::sock_write_int( sock, qwsWindowActivate );
    sock->flush();
}

void createWindowClient(QLocalSocket *sock, int id, const QRect& geom)
{
    ::sock_write_int( sock, qwsWindowCreate );
    ::sock_write_int( sock, id );
    ::sock_write_rect( sock, geom );    
    sock->flush();
}

void flushWindowRequestClient(QLocalSocket* sock, int id, const QRegion& r)
{
    ::sock_write_int( sock, qwsWindowFlushRequest);
    ::sock_write_int( sock, id);
    ::sock_write_region( sock, r );
    sock->flush();
}

int flushWindowRequestServer(QLocalSocket *sock, QRegion& r)
{
    int id = 0;
    ::sock_read_int( sock, id );
    ::sock_read_region( sock, r );
    return id;
}

void setWindowFlagsClient(QLocalSocket *sock, int id, int flags)
{
    ::sock_write_int( sock, qwsWindowFlags );
    ::sock_write_int( sock, id );
    ::sock_write_int( sock, flags );
    sock->flush();
}

int setWindowFlagsServer(QLocalSocket *sock, int &flags)
{
    int id = 0;
    ::sock_read_int( sock, id );
    ::sock_read_int( sock, flags );
    return id;
}

void setWindowStateClient(QLocalSocket *sock, int id, int state)
{
    ::sock_write_int( sock, qwsWindowState );
    ::sock_write_int( sock, id );
    ::sock_write_int( sock, state );
    sock->flush();
}

int setWindowStateServer(QLocalSocket *sock, int &state)
{
    int id = 0;
    ::sock_read_int( sock, id );
    ::sock_read_int( sock, state );
    return id;
}

void setWindowMaskClient(QLocalSocket *sock, int id, const QRegion &mask)
{
    ::sock_write_int( sock, qwsWindowMask );
    ::sock_write_int( sock, id );
    ::sock_write_region( sock, mask );
    sock->flush();
}

int setWindowMaskServer(QLocalSocket *sock, QRegion &mask)
{
    int id = 0;
    ::sock_read_int(sock, id);
    ::sock_read_region(sock, mask);
    return id;
}

void exposeWindowServer(QLocalSocket *sock, int id, const QRect &r)
{
    ::sock_write_int( sock, qwsWindowExpose );
    ::sock_write_int( sock, id);
    ::sock_write_rect( sock, r );
    sock->flush();
}

int exposeWindowClient(QLocalSocket *sock, QRect &r)
{
    int id = 0;
    ::sock_read_int(sock, id);
    ::sock_read_rect(sock, r);
    return id;
}

void sock_write_point(QLocalSocket *sock, const QPoint &val)
{
    ::sock_write( sock, &val, sizeof(val) );
}

void sock_read_point(QLocalSocket *sock, QPoint &val)
{
    ::sock_read( sock, &val, sizeof(val));
}

void setWindowGeometryClient(QLocalSocket *sock, int id, const QRect& geom)
{
    ::sock_write_int( sock, qwsWindowGeometry );
    ::sock_write_int( sock, id);
    ::sock_write_rect( sock, geom );
    sock->flush();
}

int setWindowGeometryServer(QLocalSocket *sock, QRect &geom)
{
    int id = 0;
    ::sock_read_int(sock, id);
    ::sock_read_rect(sock, geom);
    return id;
}

int createWindowServer(QLocalSocket *sock, QRect &geom)
{
    int id = 0;
    ::sock_read_int(sock, id);
    ::sock_read_rect(sock, geom);
    return id;
}

void raiseWindowClient(QLocalSocket *sock, int id)
{
    ::sock_write_int( sock, qwsWindowRaise );
    ::sock_write_int( sock, id);
    sock->flush();
}

void lowerWindowClient(QLocalSocket *sock, int id)
{
    ::sock_write_int( sock, qwsWindowLower );
    ::sock_write_int( sock, id);
    sock->flush();
}

int raiseWindowServer(QLocalSocket *sock)
{
    int id = 0;
    ::sock_read_int(sock, id);
    return id;
}

int lowerWindowServer(QLocalSocket *sock)
{
    int id = 0;
    ::sock_read_int(sock, id);
    return id;
}
