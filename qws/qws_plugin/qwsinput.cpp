#include <qpa/qwindowsysteminterface.h>
#include <qpa/qplatformscreen.h>

#include <QDebug>
#include <QGuiApplication>
#include <QTimer>

#include "qws_proto.h"
#include "qwsintegration.h"

#include "qwsinput.h"

QWSInput::QWSInput()
    : sock(new QLocalSocket(this))
{
    sock->connectToServer(QWS_SERVER);
    if ( !sock->waitForConnected() )
    {
        exit(EXIT_FAILURE);
    }
    connect(sock, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(sock, SIGNAL(disconnected()), this, SLOT(disconnect()));
}

void QWSInput::readyRead()
{
    while ( (unsigned)sock->bytesAvailable() > 0 )
    {
        int cmd = qwsNone;
        ::sock_read(sock, &cmd, sizeof(int));
        switch( cmd )
        {
        case qwsMouseEvent:
            {
                QWindow* w = NULL;
                int buttons = Qt::NoButton;
                QPoint pt, pg;
                ::sock_read( sock, &w, sizeof(w) );
                ::sock_read( sock, &buttons, sizeof(buttons) );
                ::sock_read( sock, &pt, sizeof(pt));
                ::sock_read( sock, &pg, sizeof(pg));
                QWindowSystemInterface::handleMouseEvent(w, pt, pg, static_cast<Qt::MouseButtons>(buttons));

                qDebug() << "mouseEvent(" << w << "," << pt << ")";
            }
            break;
        case qwsKeyEvent:
            {
                QWindow* w = NULL;
                QEvent::Type t = QEvent::None;
                int key = 0;
                Qt::KeyboardModifier mod = Qt::NoModifier;
                ushort ch = 0;
                ::sock_read( sock, &w, sizeof(w) );
                ::sock_read( sock, &t, sizeof(t) );
                ::sock_read( sock, &key, sizeof(key));
                ::sock_read( sock, &mod, sizeof(mod));
                ::sock_read( sock, &ch, sizeof(ch));
                QWindowSystemInterface::handleKeyEvent(w, t, key, mod, QString(QChar(ch)));
            }
            break;
        case qwsRotationEvent:
            {
                QScreen* pScreen = QGuiApplication::primaryScreen();
                QWindowSystemInterface::handleScreenOrientationChange(pScreen, pScreen->handle()->orientation());
                QWindowSystemInterface::handleScreenGeometryChange(pScreen, pScreen->handle()->geometry(), pScreen->handle()->geometry());
                qDebug() << "Rotation event" << pScreen->handle()->geometry();
            }
            break;
        }
    }
}

void QWSInput::disconnect()
{
    qApp->quit();
}
