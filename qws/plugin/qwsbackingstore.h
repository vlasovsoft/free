#ifndef QBACKINGSTORE_QWS_H
#define QBACKINGSTORE_QWS_H

#include <qpa/qplatformbackingstore.h>
#include <qpa/qplatformwindow.h>

#include <QImage>
#include <QMutex>
#include <QLocalSocket>
#include <QSharedMemory>

class QLinuxFbScreen;
class QWSPlatformWindow;

class QWSBackingStore : public QPlatformBackingStore
{
    friend class QWSIntegration;
    friend class QLinuxFbScreen;

    QLinuxFbScreen* mScreen;
    QWSPlatformWindow* mPlatformWindow;

public:
    QWSBackingStore(QWindow *window);
    ~QWSBackingStore();

    QPaintDevice* paintDevice() Q_DECL_OVERRIDE;
    void flush(QWindow *window, const QRegion &region, const QPoint &offset) Q_DECL_OVERRIDE;
    void resize(const QSize &size, const QRegion &staticContents) Q_DECL_OVERRIDE;
    const QImage image();

    void beginPaint(const QRegion &) override;
    void endPaint() override;

    void lock();
    void unlock();

    QLinuxFbScreen* platformScreen() const { return mScreen; }
    QWSPlatformWindow* platformWindow() const { return mPlatformWindow; }

private:
    QImage img;
    QMutex imgMtx;
};

#endif
