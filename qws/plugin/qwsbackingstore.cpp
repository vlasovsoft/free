#include <QScreen>
#include <QThread>
#include <QDateTime>
#include <QPainter>
#include <QCoreApplication>

#include <QDebug>

#include <qpa/qplatformscreen.h>

#include "qlinuxfbscreen.h"
#include "qwsbackingstore.h"
#include "qwsintegration.h"
#include "qwsplatformwindow.h"
#include "qws_proto.h"

QWSBackingStore::QWSBackingStore(QWindow *window)
    : QPlatformBackingStore(window)
    , mScreen(NULL)
    , mPlatformWindow(NULL)
{
    qDebug() << "QWSBackingStore::QWSBackingStore()";
}

QWSBackingStore::~QWSBackingStore()
{
}

QPaintDevice *QWSBackingStore::paintDevice()
{
    return &img;
}

void QWSBackingStore::flush(QWindow* window, const QRegion& region, const QPoint& offset)
{
    QCoreApplication::postEvent(mScreen, new QEvent(QEvent::UpdateRequest));
    if ( mPlatformWindow != NULL && !region.isEmpty() ) {
        ::flushWindowRequestClient(mPlatformWindow->sock, mPlatformWindow->id, region);
        //::flushWindowRequestClient(mPlatformWindow->sock, mPlatformWindow->id, region.boundingRect());
    }
}

void QWSBackingStore::resize(const QSize& size, const QRegion&)
{    
    if ( img.size() != size ) {
        img = QImage(size, QImage::Format_ARGB32_Premultiplied);
        //img.fill(0);
        qDebug() << "==== QWSBackingStore::resize " << size;
    }
}

const QImage QWSBackingStore::image()
{
    return img;
}

void QWSBackingStore::beginPaint(const QRegion& region)
{
    lock();
    if (img.hasAlphaChannel()) {
        QPainter p(&img);
        p.setCompositionMode(QPainter::CompositionMode_Source);
        for (const QRect &r : region)
            p.fillRect(r, Qt::transparent);
    }
    //qDebug() << "==== QWSBackingStore::beginPaint ";
}

void QWSBackingStore::endPaint()
{
    unlock();
    //qDebug() << "==== QWSBackingStore::endPaint ";
}

void QWSBackingStore::lock()
{
    imgMtx.lock();
}

void QWSBackingStore::unlock()
{
    imgMtx.unlock();
}
