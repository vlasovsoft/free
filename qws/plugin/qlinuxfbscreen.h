#ifndef QLINUXFBSCREEN_H
#define QLINUXFBSCREEN_H

#include <qpa/qplatformscreen.h>

class QWSPlatformWindow;
class QWSBackingStore;

class QPainter;
class QFbCursor;

class QLinuxFbScreen : public QObject, public QPlatformScreen
{
    Q_OBJECT

public:
    QLinuxFbScreen(const QStringList &args);
    ~QLinuxFbScreen();

    bool initialize();

    QRect geometry() const override { return mGeometry; }
    int depth() const override { return mDepth; }
    QImage::Format format() const override { return mFormat; }
    QSizeF physicalSize() const override { return mPhysicalSize; }

    void flushBackingStore(QWSBackingStore* bs, const QRegion& r);
    void addWindow(QWSPlatformWindow* w);
    void removeWindow(QWSPlatformWindow* w);
    QWSPlatformWindow* findWindow(int id);

private:
    QStringList mArgs;

    int mFbFd;
    int mDepth;
    QRect mGeometry;
    QImage::Format mFormat;
    QSizeF mPhysicalSize;

    QImage mFbScreenImage;
    int mBytesPerLine;
    int mOldTtyMode;

    struct {
        uchar *data;
        int offset, size;
    } mMmap;

    QPainter *mBlitter;
    std::map<int, QWSPlatformWindow*> windows;
};

QT_END_NAMESPACE

#endif // QLINUXFBSCREEN_H

