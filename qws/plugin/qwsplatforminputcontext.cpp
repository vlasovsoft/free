#include <QLocalSocket>
#include <QDebug>

#include "qwsplatforminputcontext.h"

#include "qws_proto.h"

QWSPlatformInputContext::QWSPlatformInputContext()
{
}

void QWSPlatformInputContext::showInputPanel()
{
}

void QWSPlatformInputContext::hideInputPanel()
{
}

void QWSPlatformInputContext::setFocusObject(QObject *object)
{
    QPlatformInputContext::setFocusObject(object);
}

