#ifndef QWSPLATFORMINPUTCONTEXT_H
#define QWSPLATFORMINPUTCONTEXT_H

#include <qpa/qplatforminputcontext.h>

class QWSPlatformInputContext : public QPlatformInputContext
{
public:
    QWSPlatformInputContext();

    void showInputPanel();
    void hideInputPanel();
    bool isInputPanelVisible() const { return true; }
    void setFocusObject(QObject *object);
    bool hasCapability(Capability) const { return false; }
};

#endif // QWSPLATFORMINPUTCONTEXT_H
