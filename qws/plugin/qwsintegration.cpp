#include "qwsintegration.h"
#include "qwsbackingstore.h"
#include "qwsplatformwindow.h"
#include "qwsplatforminputcontext.h"
#include "qlinuxfbscreen.h"
#include "qws_proto.h"

#include <QtGui/private/qpixmap_raster_p.h>
#include <QtGui/private/qguiapplication_p.h>

#include <QtFontDatabaseSupport/private/qfreetypefontdatabase_p.h>
#include <QtEventDispatcherSupport/private/qgenericunixeventdispatcher_p.h>

#include <qpa/qplatformwindow.h>
#include <qpa/qplatforminputcontextfactory_p.h>

QT_BEGIN_NAMESPACE

class QCoreTextFontEngine;

static inline unsigned parseOptions(const QStringList&)
{
    return 0;
}

QWSIntegration::QWSIntegration(const QStringList &parameters)
    : m_options(parseOptions(parameters))
    , fdb(new QFreeTypeFontDatabase())
    , screen(new QLinuxFbScreen(parameters))
{
    QWindowSystemInterface::handleScreenAdded(screen);
}

QWSIntegration::~QWSIntegration()
{
    QWindowSystemInterface::handleScreenRemoved(screen);
    delete fdb;
    delete mInputContext;
}

bool QWSIntegration::hasCapability(QPlatformIntegration::Capability cap) const
{
    switch (cap) {
    case ThreadedPixmaps: return true;
    case MultipleWindows: return true;
    case NonFullScreenWindows: return true;
    case WindowManagement: return false;
    default: return QPlatformIntegration::hasCapability(cap);
    }
}

void QWSIntegration::initialize()
{
    sock = new QLocalSocket(this);
    QObject::connect(sock, SIGNAL(readyRead()), this, SLOT(readyRead()));
    QObject::connect(sock, SIGNAL(errorOccurred(QLocalSocket::LocalSocketError)), this, SLOT(errorOccurred(QLocalSocket::LocalSocketError)));
    sock->connectToServer(QWS_SERVER);
    if ( !sock->waitForConnected() ) {
        errorOccurred(QLocalSocket::SocketAccessError);
    }
    mInputContext = QPlatformInputContextFactory::create();
}

QPlatformFontDatabase* QWSIntegration::fontDatabase() const
{
    return fdb;
}

QPlatformWindow* QWSIntegration::createPlatformWindow(QWindow *window) const
{
    QWSPlatformWindow *w = new QWSPlatformWindow(screen, window, sock);
    // try to assign backing store to this platform window
    auto i = std::find_if(
        pendingStores.begin(),
        pendingStores.end(),
        [window](const QWSBackingStore* bs) {
            return bs->window() == window;
        });
    if ( i != pendingStores.end() ) {
        QWSBackingStore* bs = *i;
        w->mBackingStore = bs;
        bs->mPlatformWindow = w;
        pendingStores.erase(i);
    } else {
        pendingWindows.push_back(w);
    }
    return w;
}

QPlatformBackingStore* QWSIntegration::createPlatformBackingStore(QWindow *window) const
{
    QWSBackingStore* bs = new QWSBackingStore(window);
    bs->mScreen = screen;
    // try to assign platform window to this backing store
    auto i = std::find_if(
        pendingWindows.begin(),
        pendingWindows.end(),
        [window](const QWSPlatformWindow* w) {
            return w->window() == window;
        });
    if ( i != pendingWindows.end() ) {
        QWSPlatformWindow* w = *i;
        bs->mPlatformWindow = w;
        w->mBackingStore = bs;
        pendingWindows.erase(i);
    } else {
        pendingStores.push_back(bs);
    }
    return bs;
}

QAbstractEventDispatcher *QWSIntegration::createEventDispatcher() const
{
    return createUnixEventDispatcher();
}

QWSIntegration *QWSIntegration::instance()
{
    return static_cast<QWSIntegration *>(QGuiApplicationPrivate::platformIntegration());
}

void QWSIntegration::connected()
{

}

void QWSIntegration::readyRead()
{
    QLocalSocket* sock = qobject_cast<QLocalSocket*>(sender());
    while ( (unsigned)sock->bytesAvailable() > 0 )
    {
        int cmd = qwsNone;
        ::sock_read(sock, &cmd, sizeof(int));
        switch ( cmd )
        {
        case qwsMouseEvent:
            {
                QPoint pt, lt;
                int type=0;
                int id = ::readMouseEventClient(sock, lt, pt, type);
                QWSPlatformWindow* w = screen->findWindow(id);
                if ( w != NULL ) {
                    Qt::MouseButtons buttons = QEvent::MouseButtonRelease == type? Qt::NoButton : Qt::LeftButton;
                    Qt::MouseButton button = QEvent::MouseMove == type? Qt::NoButton : Qt::LeftButton;
                    QWindowSystemInterface::handleMouseEvent(w->window(), lt, pt, buttons, button, static_cast<QEvent::Type>(type));
                    qDebug() << "Mouse event" << w->id;
                }
            }
            break;

        case qwsWindowFlushResponse:
            {
                QRegion r;
                int id = ::flushWindowResponseClient(sock, r);
                QWSPlatformWindow* w = screen->findWindow(id);
                if ( w != NULL ) {
                    screen->flushBackingStore(w->mBackingStore, r);
                }
            }
            break;

        case qwsWindowExpose:
            {
                QRect r;
                int id = ::exposeWindowClient(sock, r);
                QWSPlatformWindow* w = screen->findWindow(id);
                if ( w != NULL ) {
                    QWindowSystemInterface::handleExposeEvent(w->window(), QRect(QPoint(0, 0), r.size()));
                }
            }
            break;
        }
    }
}

void QWSIntegration::errorOccurred(QLocalSocket::LocalSocketError socketError)
{
    qDebug() << "Socket error:" << socketError << "terminating!" ;
    //QTimer::singleShot(0, qApp, SLOT(quit()));
    exit(1);
}

QT_END_NAMESPACE
