#ifndef INPUTPANEL_H_
#define INPUTPANEL_H_

#include <vector>

#include <QMap>
#include <QWidget>
#include <QEvent>
#include <QTimer>

#define VIRTUAL_KEYBOARD_WIDTH 11
#define VIRTUAL_KEYBOARD_HEIGHT 5

class Window;

class InputPanel : public QWidget
{
Q_OBJECT
    struct VirtualKey
    {
        QPoint pos;
        QSize size;
        int key;
        QString sn;
        QString ss;
        VirtualKey( int x, int y, int w, int h, int k, const QString& s1, const QString& s2 )
            : pos( x, y )
            , size( w, h )
            , key( k )
            , sn( s1 )
            , ss( s2 )
        {}
    };

    std::vector<VirtualKey> keys;

    VirtualKey* pLastKey;
    VirtualKey* pPressedKey;
    int shiftKeyIndex;

    bool isShift;
    bool isCapsLock;
    bool isEnableCapsLock;

    QTimer repeatDelayTimer;
    QTimer repeatRateTimer;

    QString layout;
    int layoutNo;
    QVector<QString> layouts;
    QMap<QString,QString> map;

    int rotation;

public:
    InputPanel(QWidget* parent = 0);
    ~InputPanel();

    void setVisible(bool visible);
    int heightForWidth ( int w ) const;

    void setLayout( int l );
    void changeLayout();

    void setRotation(int v) { rotation = v; }

protected:
    void paintEvent(QPaintEvent* event);
    void activateKey(VirtualKey* pKey, QEvent::Type t);
    void translateKey(QKeyEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    QRect getRectForKey( const VirtualKey& key ) const;
    void updateKey( const VirtualKey& key );
    VirtualKey* findKeyByMousePos( const QPoint& pt );
    VirtualKey* findKeyByCursorPos( int x, int y);
    void readLayouts();

private slots:
    void onCapsLock();
    void onAutoRepeat();
};

#endif /* INPUTPANEL_H_ */

