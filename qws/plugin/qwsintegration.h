#ifndef QPLATFORMINTEGRATION_QWS_H
#define QPLATFORMINTEGRATION_QWS_H

#include <qpa/qplatformintegration.h>

#include <QtCore/QObject>

#include <QLocalSocket>
#include <QSharedMemory>

#include "qws_proto.h"

class QWSPlatformWindow;
class QWSBackingStore;
class QLinuxFbScreen;

class QWSIntegration : public QObject, public QPlatformIntegration
{
    Q_OBJECT

public:
    QWSIntegration(const QStringList &parameters);
    ~QWSIntegration();

    bool hasCapability(QPlatformIntegration::Capability cap) const Q_DECL_OVERRIDE;

    void initialize() override;
    QPlatformFontDatabase *fontDatabase() const override;
    QPlatformWindow* createPlatformWindow(QWindow *window) const override;
    QPlatformBackingStore* createPlatformBackingStore(QWindow *window) const override;
    QAbstractEventDispatcher* createEventDispatcher() const override;
    QPlatformInputContext* inputContext() const override { return mInputContext; }

    unsigned options() const { return m_options; }

    static QWSIntegration* instance();

private slots:
    void connected();
    void readyRead();
    void errorOccurred(QLocalSocket::LocalSocketError socketError);

private:
    unsigned m_options;
    QPlatformFontDatabase* fdb;
    QLinuxFbScreen* screen;
    QPlatformInputContext* mInputContext;
    QLocalSocket* sock;

    mutable std::list<QWSPlatformWindow*> pendingWindows;
    mutable std::list<QWSBackingStore*> pendingStores;
};

#endif
