#include <qpa/qplatformintegrationplugin.h>
#include "qwsintegration.h"

QT_BEGIN_NAMESPACE

class QWSIntegrationPlugin : public QPlatformIntegrationPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QPlatformIntegrationFactoryInterface_iid FILE "qws.json")
public:
    QPlatformIntegration *create(const QString&, const QStringList&) Q_DECL_OVERRIDE;
};

QPlatformIntegration *QWSIntegrationPlugin::create(const QString& system, const QStringList& paramList)
{
    if (!system.compare(QLatin1String("qws"), Qt::CaseInsensitive))
        return new QWSIntegration(paramList);

    return 0;
}

QT_END_NAMESPACE

#include "main.moc"

