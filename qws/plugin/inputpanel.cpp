#include <QTimer>
#include <QPainter>
#include <QPaintEvent>
#include <QTimer>
#include <QDebug>

#include "inputpanel.h"

InputPanel::InputPanel(QWidget* parent)
    : QWidget(parent)
    , pLastKey(NULL)
    , shiftKeyIndex(0)
    , isShift(false)
    , isCapsLock(false)
    , isEnableCapsLock(false)
    , layoutNo(0)
    , rotation(0)
{
    setFont(QFont("DejaVu Sans Mono"));
    setFocusPolicy(Qt::NoFocus);

    QSizePolicy policy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    policy.setHeightForWidth(true);
    setSizePolicy(policy);

    keys.reserve(VIRTUAL_KEYBOARD_WIDTH*VIRTUAL_KEYBOARD_HEIGHT);

    keys.push_back( VirtualKey( 0, 0, 1, 1, Qt::Key_1, "1", "!" ) );
    keys.push_back( VirtualKey( 1, 0, 1, 1, Qt::Key_2, "2", "@" ) );
    keys.push_back( VirtualKey( 2, 0, 1, 1, Qt::Key_3, "3", "#" ) );
    keys.push_back( VirtualKey( 3, 0, 1, 1, Qt::Key_4, "4", "$" ) );
    keys.push_back( VirtualKey( 4, 0, 1, 1, Qt::Key_5, "5", "%" ) );
    keys.push_back( VirtualKey( 5, 0, 1, 1, Qt::Key_6, "6", "^" ) );
    keys.push_back( VirtualKey( 6, 0, 1, 1, Qt::Key_7, "7", "&" ) );
    keys.push_back( VirtualKey( 7, 0, 1, 1, Qt::Key_8, "8", "*" ) );
    keys.push_back( VirtualKey( 8, 0, 1, 1, Qt::Key_9, "9", "(" ) );
    keys.push_back( VirtualKey( 9, 0, 1, 1, Qt::Key_0, "0", ")" ) );
    keys.push_back( VirtualKey( 10, 0, 1, 1, Qt::Key_Close, "✕", "✕" ) );

    keys.push_back( VirtualKey( 0, 1, 1, 1, Qt::Key_Q, "q", "Q" ) );
    keys.push_back( VirtualKey( 1, 1, 1, 1, Qt::Key_W, "w", "W" ) );
    keys.push_back( VirtualKey( 2, 1, 1, 1, Qt::Key_E, "e", "E" ) );
    keys.push_back( VirtualKey( 3, 1, 1, 1, Qt::Key_R, "r", "R" ) );
    keys.push_back( VirtualKey( 4, 1, 1, 1, Qt::Key_T, "t", "T" ) );
    keys.push_back( VirtualKey( 5, 1, 1, 1, Qt::Key_Y, "y", "Y" ) );
    keys.push_back( VirtualKey( 6, 1, 1, 1, Qt::Key_U, "u", "U" ) );
    keys.push_back( VirtualKey( 7, 1, 1, 1, Qt::Key_I, "i", "I" ) );
    keys.push_back( VirtualKey( 8, 1, 1, 1, Qt::Key_O, "o", "O" ) );
    keys.push_back( VirtualKey( 9, 1, 1, 1, Qt::Key_P, "p", "P" ) );
    keys.push_back( VirtualKey( 10, 1, 1, 1, Qt::Key_Backspace, "⌫", "⌫" ) );

    keys.push_back( VirtualKey( 0, 2, 1, 1, Qt::Key_A, "a", "A" ) );
    keys.push_back( VirtualKey( 1, 2, 1, 1, Qt::Key_S, "s", "S" ) );
    keys.push_back( VirtualKey( 2, 2, 1, 1, Qt::Key_D, "d", "D" ) );
    keys.push_back( VirtualKey( 3, 2, 1, 1, Qt::Key_F, "f", "F" ) );
    keys.push_back( VirtualKey( 4, 2, 1, 1, Qt::Key_G, "g", "G" ) );
    keys.push_back( VirtualKey( 5, 2, 1, 1, Qt::Key_H, "h", "H" ) );
    keys.push_back( VirtualKey( 6, 2, 1, 1, Qt::Key_J, "j", "J" ) );
    keys.push_back( VirtualKey( 7, 2, 1, 1, Qt::Key_K, "k", "K" ) );
    keys.push_back( VirtualKey( 8, 2, 1, 1, Qt::Key_L, "l", "L" ) );
    keys.push_back( VirtualKey( 9, 2, 1, 1, Qt::Key_Semicolon, ":", ";" ) );
    keys.push_back( VirtualKey( 10, 2, 1, 1, Qt::Key_Return, "⮰", "⮰" ) );

    keys.push_back( VirtualKey( 0, 3, 1, 1, Qt::Key_Z, "z", "Z" ) );
    keys.push_back( VirtualKey( 1, 3, 1, 1, Qt::Key_X, "x", "X" ) );
    keys.push_back( VirtualKey( 2, 3, 1, 1, Qt::Key_C, "c", "C" ) );
    keys.push_back( VirtualKey( 3, 3, 1, 1, Qt::Key_V, "v", "V" ) );
    keys.push_back( VirtualKey( 4, 3, 1, 1, Qt::Key_B, "b", "B" ) );
    keys.push_back( VirtualKey( 5, 3, 1, 1, Qt::Key_N, "n", "N" ) );
    keys.push_back( VirtualKey( 6, 3, 1, 1, Qt::Key_M, "m", "M" ) );
    keys.push_back( VirtualKey( 7, 3, 1, 1, Qt::Key_Apostrophe, "'", "\"" ) );
    keys.push_back( VirtualKey( 8, 3, 1, 1, Qt::Key_Comma, ".", "," ) );
    keys.push_back( VirtualKey( 9, 3, 1, 1, Qt::Key_Up, "⇧", "⇧" ) );
    keys.push_back( VirtualKey( 10, 3, 1, 1, Qt::Key_Plus, "-", "+" ) );

    keys.push_back( VirtualKey( 0, 4, 1, 1, Qt::Key_Shift, "⇪", "⇪" ) );
    shiftKeyIndex = keys.size() - 1;

    keys.push_back( VirtualKey( 1, 4, 1, 1, Qt::Key_Mode_switch, "⛿", "⛿" ) );
    keys.push_back( VirtualKey( 2, 4, 1, 1, Qt::Key_BraceLeft, "[", "{" ) );
    keys.push_back( VirtualKey( 3, 4, 1, 1, Qt::Key_BraceRight, "]", "}" ) );
    keys.push_back( VirtualKey( 4, 4, 2, 1, Qt::Key_Space, " ", " " ) );
    keys.push_back( VirtualKey( 6, 4, 1, 1, Qt::Key_Question, "?", "/" ) );
    keys.push_back( VirtualKey( 7, 4, 1, 1, Qt::Key_Underscore, "_", "=" ) );
    keys.push_back( VirtualKey( 8, 4, 1, 1, Qt::Key_Left, "⇦", "⇦" ) );
    keys.push_back( VirtualKey( 9, 4, 1, 1, Qt::Key_Down, "⇩", "⇩" ) );
    keys.push_back( VirtualKey( 10, 4, 1, 1, Qt::Key_Right, "⇨", "⇨" ) );

    readLayouts();
    setLayout(0);
    // TODO:
    //if ( g_pConfig )
    //    setLayout(g_pConfig->readInt("kbd_layout", 0));

    repeatDelayTimer.setInterval(500);
    repeatDelayTimer.setSingleShot(true);
    repeatRateTimer.setInterval(150);

    QObject::connect( &repeatDelayTimer, SIGNAL(timeout()), this, SLOT(onAutoRepeat()) );
    QObject::connect( &repeatRateTimer, SIGNAL(timeout()), this, SLOT(onAutoRepeat()) );
}

InputPanel::~InputPanel()
{
    // TODO:
    //if ( g_pConfig )
    //    g_pConfig->writeInt("kbd_layout", layoutNo);
}

void InputPanel::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    repeatDelayTimer.stop();
    repeatRateTimer.stop();
    pLastKey = NULL;
}

int InputPanel::heightForWidth ( int w ) const
{
    return w * VIRTUAL_KEYBOARD_HEIGHT / VIRTUAL_KEYBOARD_WIDTH;
}

void InputPanel::setLayout(int l)
{
    if ( l >= 0 && l < layouts.size() )
    {
        map.clear();

        layoutNo = l;

        QStringList list( layouts[layoutNo].split(" ") );

        if ( list.empty() )
            return;

        layout = list[0];

        for ( int n=1; n<list.size(); ++n )
        {
            if ( 2 == list[n].length() )
                map[QString(list[n][0])] = QString(list[n][1]);
        }

        update();
    }
}

void InputPanel::changeLayout()
{
    setLayout ( (layoutNo + 1) % layouts.size() );
}

void InputPanel::paintEvent(QPaintEvent* event)
{
    QPainter p(this);

    QColor bg(palette().color( QPalette::Window ));

    QBrush bb(Qt::black);
    QBrush nb(Qt::NoBrush);
    QPen bp(Qt::black);
    QPen lp(bg);

    QRect er = event->rect();

    p.setBrush( QBrush( bg ) );
    p.drawRect( rect().adjusted(0,0,-1,-1) );

    int fontSize = 0;
    for ( std::vector<VirtualKey>::const_iterator
          i  = keys.begin();
          i != keys.end();
          ++i )
    {
        QRect r( getRectForKey( *i ) );
        if ( r.intersects( er ) )
        {
            p.setBrush( &*i == pLastKey? bb : nb );
            p.setPen(Qt::NoPen);
            p.drawRect(r);

            QString s(isShift? i->ss : i->sn);
            if ( s == " " ) s = "⎵";
            QMap<QString,QString>::ConstIterator it(map.find(s));
            if ( it != map.end() ) s = it.value();

            if ( !fontSize )
            {
                fontSize = r.height() * 7 / 10;
                QFont f(p.font());
                f.setPixelSize(fontSize);
                p.setFont(f);
            }

            p.setPen( &*i == pLastKey? lp : bp );
            p.drawText( r, Qt::AlignVCenter | Qt::AlignHCenter, s );
        }
    }
}

void InputPanel::activateKey(VirtualKey* pKey, QEvent::Type t)
{
    if ( pKey )
    {
        bool needFullUpdate = false;

        if ( QEvent::KeyPress == t )
            pLastKey = pKey;
        else
            pLastKey = NULL;
     
        if ( Qt::Key_Shift == pKey->key )
        {
            if ( QEvent::KeyRelease == t )
            {
                if ( isShift )
                {
                    if ( isCapsLock )
                    {
                        // shift and caps lock:
                        // just turn shift and caps lock off
                        isShift = false;
                        isCapsLock = false;
                        pKey->sn = pKey->ss = "⇪";
                        needFullUpdate = true;
                    }
                    else
                    {
                        // shift and not caps lock:
                        // check isEnableCapsLock
                        if ( isEnableCapsLock )
                        {
                            // caps lock is enabled:
                            // turn on caps lock
                            isCapsLock = true;
                            pKey->sn = pKey->ss = "⇫";
                        }
                        else
                        {
                            // caps lock is not enabled:
                            // turn off shift
                            isShift = false;
                            needFullUpdate = true;
                        }

                    }
                }
                else
                {
                    // turn on shift
                    isShift = true;
                    isCapsLock = false;
                    isEnableCapsLock = true;
                    // pKey->image = "shift1";
                    needFullUpdate = true;
                    QTimer::singleShot(1000, this, SLOT(onCapsLock()));
                }
            }

            if ( needFullUpdate )
            {
                update();
            }
            else
            {
                updateKey( *pKey );
            }
        }
        else
        if ( Qt::Key_Mode_switch == pKey->key )
        {
            if ( QEvent::KeyPress == t)
            {
                updateKey( *pKey );
            }
            else
            {
                changeLayout();
            }
        }
        else 
        if ( Qt::Key_Close == pKey->key )
        {
            if ( QEvent::KeyPress == t )
            {
                updateKey( *pKey );
            }
            else
            {
                hide();
            }
        }
        else
        {
            Qt::KeyboardModifier mod = isShift? Qt::ShiftModifier : Qt::NoModifier;
            QString s;
            switch ( pKey->key )
            {
            case Qt::Key_Up:
            case Qt::Key_Down:
            case Qt::Key_Left:
            case Qt::Key_Right:
            case Qt::Key_Return:
            case Qt::Key_Backspace:
                break;
            default:
                s = isShift? pKey->ss : pKey->sn;
                break;
            }
            QMap<QString,QString>::ConstIterator i(map.find(s));
            if ( i != map.end() ) s = i.value();

            // TODO:
            //if ( active != NULL )
            //{
            //    active->sendKeyEvent( t, pKey->key, mod, s);
            //}

            if (    isShift
                 && !isCapsLock
                 && pKey->key != Qt::Key_Up
                 && pKey->key != Qt::Key_Down
                 && pKey->key != Qt::Key_Left
                 && pKey->key != Qt::Key_Right
                 && pKey->key != Qt::Key_Return
                 && pKey->key != Qt::Key_Backspace )
            {
                needFullUpdate = true;
                isShift = false;
                keys[shiftKeyIndex].sn = keys[shiftKeyIndex].ss = "⇪";
            }

            if ( needFullUpdate )
            {
                update();
            }
            else
            {
                updateKey( *pKey );
            }
        }
    }
}

void InputPanel::translateKey(QKeyEvent* event)
{
    for ( std::vector<VirtualKey>::iterator
          i = keys.begin();
          i!= keys.end();
          ++i )
    {
        if ( event->key() == i->key )
        {
            activateKey( &*i, event->type() );
        }
    }
}

void InputPanel::mousePressEvent(QMouseEvent* event)
{
    QPoint pos = event->pos(); // TODO: ::screenToClient( size(), rotation, event->pos() );
    VirtualKey* pKey = findKeyByMousePos( pos );
    if ( pKey )
    {
        activateKey(pKey, QEvent::KeyPress);
        repeatDelayTimer.start();
    }
}

void InputPanel::mouseReleaseEvent(QMouseEvent* event)
{
    Q_UNUSED(event)
    if ( pLastKey )
    {
        activateKey(pLastKey, QEvent::KeyRelease);
        repeatDelayTimer.stop();
        repeatRateTimer.stop();
    }
}

QRect InputPanel::getRectForKey( const InputPanel::VirtualKey& key ) const
{
    QSize s(size());

    int cw = 0, ch = 0;

    switch( rotation )
    {
    case 90:
    case 270:
        cw = s.height() / VIRTUAL_KEYBOARD_WIDTH;
        ch = s.width()  / VIRTUAL_KEYBOARD_HEIGHT;
        break;
    case 0:
    case 180:
    default:
        cw = s.width() / VIRTUAL_KEYBOARD_WIDTH;
        ch = s.height() / VIRTUAL_KEYBOARD_HEIGHT;
        break;
    }

    int x  = key.pos.x() * cw;
    int y  = key.pos.y() * ch;
    int w  = key.size.width() * cw;
    int h  = key.size.height() * ch;
    return QRect( x, y, w, h );
}

void InputPanel::updateKey(const InputPanel::VirtualKey &key)
{
    update( ::clientToScreen( size(), rotation, getRectForKey( key ) ) );
}

InputPanel::VirtualKey* InputPanel::findKeyByMousePos( const QPoint& pt )
{
    for ( std::vector<VirtualKey>::iterator
          i  = keys.begin();
          i != keys.end();
          ++i )
        {
            QRect r( getRectForKey( *i ) );
            if ( r.contains(pt) )
                return &*i;
        }
    return NULL;
}

void InputPanel::readLayouts()
{
    Factory<LanguageDescriptor>& ldf = *Factory<LanguageDescriptor>::instance();
    if ( !ldf.isEmpty() )
    {
        int lang = getLangIndex();
        QFile file(ldf.at(lang).getKbdFileName());
        if (file.open(QIODevice::ReadOnly))
        {
            QTextStream in(&file);
            in.setCodec("UTF-8");
            while (!in.atEnd())
            {
                QString str(in.readLine().trimmed());
                if ( !str.isEmpty() )
                    layouts.push_back(str);
            }
        }
    }
    if ( layouts.isEmpty() )
    {
        layouts.push_back("EN");
    }
}
    
void InputPanel::onCapsLock()
{
    isEnableCapsLock = false;
}

void InputPanel::onAutoRepeat()
{
    if ( pLastKey )
    {
        activateKey(pLastKey, QEvent::KeyPress);
        if ( sender() == &repeatDelayTimer )
            repeatRateTimer.start();
    }
    else
    {
        repeatDelayTimer.stop();
        repeatRateTimer.stop();
    }
}

