#ifndef QWSPLATFORMWINDOW_H
#define QWSPLATFORMWINDOW_H

#include <qpa/qplatformwindow.h>

#include <QLocalSocket>

class QWindow;
class QLinuxFbScreen;
class QWSBackingStore;

class QWSPlatformWindow : public QObject, public QPlatformWindow
{
Q_OBJECT
    friend class QWSIntegration;
    friend class QWSBackingStore;
    friend class QLinuxFbScreen;

    int id;
    QLocalSocket* sock;
    Qt::WindowFlags mWindowFlags;
    Qt::WindowStates mWindowState;
    QLinuxFbScreen* mScreen;
    QWSBackingStore* mBackingStore;

    static int next_id;

public:
    QWSPlatformWindow(QLinuxFbScreen* s, QWindow* w, QLocalSocket* sc);
    ~QWSPlatformWindow();

    void setVisible(bool visible) override;
    void setGeometry(const QRect& rect) override;
    void setWindowState(Qt::WindowStates state) override;
    void setWindowFlags(Qt::WindowFlags flags) override;
    void raise() override;
    void lower() override;
    void setOpacity(qreal level) override;
    void setMask(const QRegion &region) override;
    void requestActivateWindow() override;
    virtual WId winId() const override { return id; }

    QLinuxFbScreen* platformScreen() { return mScreen; }
    QWSBackingStore* backingStore() { return mBackingStore; }
};

#endif // QWSPLATFORMWINDOW_H
