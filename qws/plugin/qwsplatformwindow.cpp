#include <qpa/qplatformscreen.h>
#include <qpa/qwindowsysteminterface.h>

#include <QLocalSocket>
#include <QWindow>
#include <QDebug>

#include "qws_proto.h"
#include "qlinuxfbscreen.h"

#include "qwsplatformwindow.h"

int QWSPlatformWindow::next_id = 1;

QWSPlatformWindow::QWSPlatformWindow(QLinuxFbScreen* s, QWindow *w, QLocalSocket* sc)
    : QPlatformWindow(w)
    , id(0)
    , sock(sc)
    , mWindowFlags(Qt::Window)
    , mWindowState(Qt::WindowNoState)
    , mScreen(s)
    , mBackingStore(NULL)
{
    id = next_id++;
    ::createWindowClient(sock, id, geometry());
    mScreen->addWindow(this);
    qDebug() << "QWSPlatformWindow::QWSPlatformWindow()" << id << geometry();
}

QWSPlatformWindow::~QWSPlatformWindow()
{
    mScreen->removeWindow(this);
    qDebug() << "QWSPlatformWindow::~QWSPlatformWindow()" << id;
}

void QWSPlatformWindow::setVisible(bool visible)
{
    QRect newGeom;
    if ( visible ) {
        if (mWindowState & Qt::WindowFullScreen)
            newGeom = platformScreen()->geometry();
        else if (mWindowState & Qt::WindowMaximized)
            newGeom = platformScreen()->availableGeometry();
        if (!newGeom.isEmpty())
            setGeometry(newGeom);
    }
    QPlatformWindow::setVisible(visible);
    ::setWindowVisibleClient(sock, id, visible, window()->isModal());

    qDebug() << "setVisible()" << id << visible;
}

void QWSPlatformWindow::setGeometry(const QRect &rect)
{    
    QPlatformWindow::setGeometry(rect);
    ::setWindowGeometryClient(sock, id, rect);
    QWindowSystemInterface::handleGeometryChange(window(), rect);
    qDebug() << "setGeometry()" << id << rect;
}

void QWSPlatformWindow::setWindowState(Qt::WindowStates state)
{
    QPlatformWindow::setWindowState(state);
    ::setWindowStateClient(sock, id, state);
    qDebug() << "setWindowState()" << id << state;
    mWindowState = state;
}

void QWSPlatformWindow::setWindowFlags(Qt::WindowFlags flags)
{
    QPlatformWindow::setWindowFlags(flags);
    ::setWindowFlagsClient(sock, id, flags);
    qDebug() << "setWindowFlags()" << id << flags;
    mWindowFlags = flags;
}

void QWSPlatformWindow::raise()
{
    ::raiseWindowClient(sock, id);
    qDebug() << "raise()" << id;
}

void QWSPlatformWindow::lower()
{
    ::lowerWindowClient(sock, id);
    qDebug() << "lower()" << id;
}

void QWSPlatformWindow::setOpacity(qreal level)
{
    qDebug() << "setOpacity()" << id << level;
}

void QWSPlatformWindow::setMask(const QRegion &region)
{
    ::setWindowMaskClient( sock, id, region );
    qDebug() << "setMask()" << id << region;
}

void QWSPlatformWindow::requestActivateWindow()
{
    QPlatformWindow::requestActivateWindow();
    qDebug() << "requestActivateWindow()" << id;
}
