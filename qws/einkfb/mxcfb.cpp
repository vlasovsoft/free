#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

#include <iostream>

#include <QString>

#if defined(KINDLE)
 #include "mxcfb_kindle.h"
 #include "KindleDevice.h"
#endif

#if defined(KOBO)
 #include "mxcfb_kobo.h"
 #include "KoboDevice.h"
#endif

#include "mxcfb.h"

#if defined(KINDLE)
// Kindle's MXCFB_WAIT_FOR_UPDATE_COMPLETE_PEARL == 0x4004462f
static int kindle_pearl_mxc_wait_for_update_complete(int fd, uint32_t marker)
{
    // Wait for a specific update to be completed
    return ioctl(fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE_PEARL, &marker);
}
#endif

#if defined(KOBO)
// Kobo's MXCFB_WAIT_FOR_UPDATE_COMPLETE_V1 == 0x4004462f
static int kobo_mxc_wait_for_update_complete(int fd, uint32_t marker)
{
    // Wait for a specific update to be completed
    return ioctl(fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE_V1, &marker);
}
#endif

#if defined(KOBO)
// Kobo's Mk7 MXCFB_WAIT_FOR_UPDATE_COMPLETE_V3
static int kobo_mk7_mxc_wait_for_update_complete(int fd, uint32_t marker)
{
    // Wait for a specific update to be completed
    mxcfb_update_marker_data mk7_update_marker;
    mk7_update_marker.update_marker = marker;
    // NOTE: 0 seems to be a fairly safe assumption for "we don't care about collisions".
    //       On a slightly related note, the EPDC_FLAG_TEST_COLLISION flag is for dry-run collision tests, never set it.
    mk7_update_marker.collision_test = 0;
    return ioctl(fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE_V3, &mk7_update_marker);
}
#endif


#if defined(KINDLE)
// Kindle's MXCFB_WAIT_FOR_UPDATE_COMPLETE == 0xc008462f
static int kindle_carta_mxc_wait_for_update_complete(int fd, uint32_t marker)
{
    // Wait for a specific update to be completed
    mxcfb_update_marker_data carta_update_marker;
    carta_update_marker.update_marker = marker;
    // NOTE: 0 seems to be a fairly safe assumption for "we don't care about collisions".
    //       On a slightly related note, the EPDC_FLAG_TEST_COLLISION flag is for dry-run collision tests, never set it.
    carta_update_marker.collision_test = 0;
    return ioctl(fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE, &carta_update_marker);
}
#endif

#if defined(KINDLE)
// Kindle's MXCFB_WAIT_FOR_UPDATE_SUBMISSION == 0x40044637
static int kindle_mxc_wait_for_update_submission(int fd, uint32_t marker)
{
    // Wait for a specific update to be submitted
    return ioctl(fd, MXCFB_WAIT_FOR_UPDATE_SUBMISSION, &marker);
}
#endif

// Returns true if waveform_mode arg matches the UI waveform mode for the current device
// NOTE: This is to avoid explicit comparison against device-specific waveform constants in mxc_update()
//       Here, it's because of the Kindle-specific WAVEFORM_MODE_GC16_FAST
bool Mxcfb::isUIWaveFormMode(int waveform_mode)
{
    return waveform_mode == waveform_ui;
}

// Returns true if waveform_mode arg matches the FlashUI waveform mode for the current device
// NOTE: This is to avoid explicit comparison against device-specific waveform constants in mxc_update()
//       Here, it's because of the Kindle-specific WAVEFORM_MODE_GC16_FAST
bool Mxcfb::isFlashUIWaveFormMode(int waveform_mode)
{
    return waveform_mode == waveform_flashui;
}

// Returns true if waveform_mode arg matches the REAGL waveform mode for the current device
// NOTE: This is to avoid explicit comparison against device-specific waveform constants in mxc_update()
//       Here, it's Kindle's various WAVEFORM_MODE_REAGL vs. Kobo's NTX_WFM_MODE_GLD16
bool Mxcfb::isREAGLWaveFormMode(int waveform_mode)
{
    return waveform_mode == waveform_reagl;
}

// Returns true if the night waveform mode for the current device requires a REAGL promotion to FULL
bool Mxcfb::isNightREAGL()
{
    return night_is_reagl;
}

// Returns true if waveform_mode arg matches the fast waveform mode for the current device
// NOTE: This is to avoid explicit comparison against device-specific waveform constants in mxc_update()
//       Here, it's because some devices use A2, while other prefer DU
bool Mxcfb::isFastWaveFormMode(int waveform_mode)
{
    return waveform_mode == waveform_fast;
}

// Returns true if waveform_mode arg matches the partial waveform mode for the current device
// NOTE: This is to avoid explicit comparison against device-specific waveform constants in mxc_update()
//       Here, because of REAGL or device-specific quirks.
bool Mxcfb::isPartialWaveFormMode(int waveform_mode)
{
    return waveform_mode == waveform_partial;
}

bool Mxcfb::isFullScreen(int w, int h)
{
    return w >= width && h >= height;
}

// Returns the device-specific nightmode waveform mode
int Mxcfb::getNightWaveFormMode()
{
    return waveform_night;
}

// Returns the device-specific flashing nightmode waveform mode
int Mxcfb::getFlashNightWaveFormMode()
{
    return waveform_flashnight;
}

uint32_t Mxcfb::get_next_marker()
{
    uint32_t marker = this->marker + 1;
    if ( marker > 128 ) {
        marker = 1;
    }

    this->marker = marker;
    return marker;
}

Mxcfb *Mxcfb::instance()
{
    static Mxcfb* fb = NULL;
    if ( NULL == fb ) {
        fb = new Mxcfb(23);
    }
    return fb;
}

#if defined(KINDLE) || defined(KOBO)
// Kindle's MXCFB_SEND_UPDATE == 0x4048462e
// Kobo's MXCFB_SEND_UPDATE == 0x4044462e
template <typename T>
static void mxc_update(Mxcfb& fb, int update_ioctl, T& refarea, int refresh_type, int waveform_mode, int x, int y, int w, int h)
{
    // NOTE: Discard empty or bogus regions, as they might murder some kernels with extreme prejudice...
    // (c.f., https://github.com/NiLuJe/FBInk/blob/5449a03d3be28823991b425cd20aa048d2d71845/fbink.c#L1755).
    // We have practical experience of that with 1x1 pixel blocks on Kindle PW2 and KV,
    // c.f., koreader/koreader#1299 and koreader/koreader#1486
    if ( w <= 1 || h <= 1 ) {
        if ( fb.debug ) {
            std::cout << "discarding bogus refresh region, w: " << w << " h: " << h << std::endl;
        }
        return;
    }

    // Pocketbook Color Lux refreshes based on bytes (not based on pixel)
    //if fb.device:has3BytesWideFrameBuffer() then
    //   w = w*3
    //end

    // NOTE: If we're trying to send a:
    //         * true FULL update,
    //         * GC16_FAST update (i.e., popping-up a menu),
    //       then wait for submission of previous marker first.
    uint32_t marker = fb.marker;
    // NOTE: Technically, we might not always want to wait for *exactly* the previous marker
    //       (we might actually want the one before that), but in the vast majority of cases, that's good enough,
    //       and saves us a lot of annoying and hard-to-get-right heuristics anyway ;).
    // Make sure it's a valid marker, to avoid doing something stupid on our first update.
    // Also make sure we haven't already waited on this marker ;).
    if ( fb.mech_wait_update_submission
      && (    refresh_type == UPDATE_MODE_FULL
           || fb.isUIWaveFormMode(waveform_mode) )
      && ( marker != 0 && marker != fb.dont_wait_for_marker ) )
    {
        if ( fb.debug ) {
            std::cout << "refresh: wait for submission of (previous) marker: " << marker << std::endl;
        }
        fb.mech_wait_update_submission(fb.fbd, marker);
        // NOTE: We don't set dont_wait_for_marker here,
        //       as we *do* want to chain wait_for_submission & wait_for_complete in some rare instances...
    }

    // NOTE: If we're trying to send a:
    //         * REAGL update,
    //         * GC16 update,
    //         * Full-screen, flashing GC16_FAST update,
    //       then wait for completion of previous marker first.
    // Again, make sure the marker is valid, too.
    if (( fb.isREAGLWaveFormMode(waveform_mode)
      || waveform_mode == WAVEFORM_MODE_GC16
      || (refresh_type == UPDATE_MODE_FULL && fb.isFlashUIWaveFormMode(waveform_mode) && fb.isFullScreen(w, h)))
      && fb.mech_wait_update_complete
      && (marker != 0 && marker != fb.dont_wait_for_marker))
    {
        if ( fb.debug ) {
            std::cout << "refresh: wait for completion of (previous) marker: " << marker << std::endl;
        }
        fb.mech_wait_update_complete(fb.fbd, marker);
    }

    refarea.update_mode = 0 == refresh_type ? UPDATE_MODE_PARTIAL : refresh_type;
    refarea.waveform_mode = 0 == waveform_mode ? WAVEFORM_MODE_GC16 : waveform_mode;
    refarea.update_region.left = x;
    refarea.update_region.top = y;
    refarea.update_region.width = w;
    refarea.update_region.height = h;
    marker = fb.get_next_marker();
    refarea.update_marker = marker;
    // NOTE: We're not using EPDC_FLAG_USE_ALT_BUFFER
    refarea.alt_buffer_data.phys_addr = 0;
    refarea.alt_buffer_data.width = 0;
    refarea.alt_buffer_data.height = 0;
    refarea.alt_buffer_data.alt_update_region.top = 0;
    refarea.alt_buffer_data.alt_update_region.left = 0;
    refarea.alt_buffer_data.alt_update_region.width = 0;
    refarea.alt_buffer_data.alt_update_region.height = 0;

    // Handle night mode shenanigans
    if ( fb.night_mode ) {
        // We're in nightmode! If the device can do HW inversion safely, do that!
        if ( fb.can_hw_invert ) {
            refarea.flags |= EPDC_FLAG_ENABLE_INVERSION;
        }

        // Enforce a nightmode-specific mode (usually, GC16), to limit ghosting, where appropriate (i.e., partial & flashes).
        // There's nothing much we can do about crappy flashing behavior on some devices, though (c.f., base/#884),
        // that's in the hands of the EPDC. Kindle PW2+ behave sanely, for instance, even when flashing on AUTO or GC16 ;).
        if (fb.isPartialWaveFormMode(waveform_mode)) {
            waveform_mode = fb.getNightWaveFormMode();
            refarea.waveform_mode = waveform_mode;
            // And handle devices like the KOA2/PW4, where night is a REAGL waveform that needs to be FULL...
            if ( fb.isNightREAGL() ) {
                refarea.update_mode = UPDATE_MODE_FULL;
            }
        } else if ( waveform_mode == WAVEFORM_MODE_GC16 || refresh_type == UPDATE_MODE_FULL ) {
            waveform_mode = fb.getFlashNightWaveFormMode();
            refarea.waveform_mode = waveform_mode;
        }
    }

    // Handle REAGL promotion...
    // NOTE: We need to do this here, because we rely on the pre-promotion actual refresh_type in previous heuristics.
    if ( fb.isREAGLWaveFormMode(waveform_mode) ) {
        // NOTE: REAGL updates always need to be full.
        refarea.update_mode = UPDATE_MODE_FULL;
    }

    // Recap the actual details of the ioctl, vs. what UIManager asked for...
    if ( fb.debug ) {
        std::cout << "mxc_update: " << x << "," << y << " " << w << "x" << h << " with marker " << marker << " IOCTL: " << update_ioctl <<  " WFM: " << refarea.waveform_mode << " UPD: " << refarea.update_mode << std::endl;
    }

    int rv = ioctl(fb.fbd, update_ioctl, &refarea);
    if ( rv < 0 ) {
        std::cout << "MXCFB_SEND_UPDATE ioctl failed: " << strerror(errno) << std::endl;
    }

    // NOTE: We want to fence off FULL updates.
    //       Mainly to mimic stock readers, but also because there's a good reason to do it:
    //       forgoing that can yield slightly "jittery" looking screens when multiple flashes are shown on screen and not in sync.
    //       To achieve that, we could simply store this marker, and wait for it on the *next* refresh,
    //       ensuring the wait would potentially be shorter, or even null.
    //       In practice, we won't actually be busy for a bit after most (if not all) flashing refresh calls,
    //       so we can instead afford to wait for it right now, which *will* block for a while,
    //       but will save us an ioctl before the next refresh, something which, even if it didn't block at all,
    //       would possibly end up being more detrimental to latency/reactivity.
    if (    refarea.update_mode == UPDATE_MODE_FULL
         && fb.mech_wait_update_complete )
    {
        if ( fb.debug ) {
            std::cout << "refresh: wait for completion of marker: " << marker << std::endl;
        }
        fb.mech_wait_update_complete(fb.fbd, marker);
        // And make sure we won't wait for it again, in case the next refresh trips one of our wait_for_*  heuristics ;).
        fb.dont_wait_for_marker = marker;
    }
}
#endif

#if defined(KINDLE)
static void refresh_k51(Mxcfb& fb, int refreshtype, int waveform_mode, int x, int y, int w, int h, bool)
{
    mxcfb_update_data refarea;
    // only for Amazon's driver, try to mostly follow what the stock reader does...
    if ( waveform_mode == WAVEFORM_MODE_REAGL ) {
        // If we're requesting WAVEFORM_MODE_REAGL, it's REAGL all around!
        refarea.hist_bw_waveform_mode = waveform_mode;
        refarea.hist_gray_waveform_mode = waveform_mode;
    } else {
        refarea.hist_bw_waveform_mode = WAVEFORM_MODE_DU;
        refarea.hist_gray_waveform_mode = WAVEFORM_MODE_GC16_FAST;
    }
    // And we're only left with true full updates to special-case.
    if ( waveform_mode == WAVEFORM_MODE_GC16 ) {
        refarea.hist_gray_waveform_mode = waveform_mode;
    }
    // TEMP_USE_PAPYRUS on Touch/PW1, TEMP_USE_AUTO on PW2 (same value in both cases, 0x1001)
    refarea.temp = TEMP_USE_AUTO;
    // Enable the appropriate flag when requesting what amounts to a 2bit update
    if ( waveform_mode == WAVEFORM_MODE_DU ) {
        refarea.flags = EPDC_FLAG_FORCE_MONOCHROME;
    } else {
        refarea.flags = 0;
    }

    mxc_update(fb, MXCFB_SEND_UPDATE, refarea, refreshtype, waveform_mode, x, y, w, h);
}
#endif

#if defined(KINDLE)
static void refresh_zelda(Mxcfb& fb, int refreshtype, int waveform_mode, int x, int y, int w, int h, bool dither)
{
    mxcfb_update_data_zelda refarea;
    // only for Amazon's driver, try to mostly follow what the stock reader does...
    if ( waveform_mode == WAVEFORM_MODE_ZELDA_GLR16 ) {
        // If we're requesting WAVEFORM_MODE_ZELDA_GLR16, it's REAGL all around!
        refarea.hist_bw_waveform_mode = waveform_mode;
        refarea.hist_gray_waveform_mode = waveform_mode;
    } else {
        refarea.hist_bw_waveform_mode = WAVEFORM_MODE_DU;
        refarea.hist_gray_waveform_mode = WAVEFORM_MODE_GC16; // NOTE: GC16_FAST points to GC16
    }
    // NOTE: Since there's no longer a distinction between GC16_FAST & GC16, we're done!
    refarea.temp = TEMP_USE_AMBIENT;
    // Did we request HW dithering on a device where it works?
    if ( dither && fb.can_hw_dither ) {
        refarea.dither_mode = EPDC_FLAG_USE_DITHERING_ORDERED;
        if ( waveform_mode == WAVEFORM_MODE_DU ) {
            refarea.quant_bit = 1;
        } else {
            refarea.quant_bit = 7;
        }
    } else {
        refarea.dither_mode = EPDC_FLAG_USE_DITHERING_PASSTHROUGH;
        refarea.quant_bit = 0;
    }
    // Enable the appropriate flag when requesting what amounts to a 2bit update, provided we're not dithering.
    if ( waveform_mode == WAVEFORM_MODE_DU && !dither ) {
        refarea.flags = EPDC_FLAG_FORCE_MONOCHROME;
    } else {
        refarea.flags = 0;
    }
    // TODO: There's also the HW-backed NightMode which should be somewhat accessible...

    mxc_update(fb, MXCFB_SEND_UPDATE_ZELDA, refarea, refreshtype, waveform_mode, x, y, w, h);
}
#endif

#if defined(KINDLE)
static void refresh_rex(Mxcfb& fb, int refreshtype, int waveform_mode, int x, int y, int w, int h, bool dither)
{
    mxcfb_update_data_rex refarea;
    // only for Amazon's driver, try to mostly follow what the stock reader does...
    if ( waveform_mode == WAVEFORM_MODE_ZELDA_GLR16 ) {
        // If we're requesting WAVEFORM_MODE_ZELDA_GLR16, it's REAGL all around!
        refarea.hist_bw_waveform_mode = waveform_mode;
        refarea.hist_gray_waveform_mode = waveform_mode;
    } else {
        refarea.hist_bw_waveform_mode = WAVEFORM_MODE_DU;
        refarea.hist_gray_waveform_mode = WAVEFORM_MODE_GC16; // NOTE: GC16_FAST points to GC16
    }
    // NOTE: Since there's no longer a distinction between GC16_FAST & GC16, we're done!
    refarea.temp = TEMP_USE_AMBIENT;
    // Did we request HW dithering on a device where it works?
    if ( dither && fb.can_hw_dither ) {
        refarea.dither_mode = EPDC_FLAG_USE_DITHERING_ORDERED;
        if ( waveform_mode == WAVEFORM_MODE_DU ) {
            refarea.quant_bit = 1;
        } else {
            refarea.quant_bit = 7;
        }
    } else {
        refarea.dither_mode = EPDC_FLAG_USE_DITHERING_PASSTHROUGH;
        refarea.quant_bit = 0;
    }
    // Enable the appropriate flag when requesting what amounts to a 2bit update, provided we're not dithering.
    if ( waveform_mode == WAVEFORM_MODE_DU && !dither ) {
        refarea.flags = EPDC_FLAG_FORCE_MONOCHROME;
    } else {
        refarea.flags = 0;
    }
    // TODO: There's also the HW-backed NightMode which should be somewhat accessible...

    mxc_update(fb, MXCFB_SEND_UPDATE_REX, refarea, refreshtype, waveform_mode, x, y, w, h);
}
#endif

#if defined(KOBO)
static void refresh_kobo(Mxcfb& fb, int refreshtype, int waveform_mode, int x, int y, int w, int h, bool)
{
    mxcfb_update_data_v1_ntx refarea;
    // only for Kobo's driver:
    refarea.alt_buffer_data.virt_addr = NULL;
    // TEMP_USE_AMBIENT, not that there was ever any other choice on Kobo...
    refarea.temp = TEMP_USE_AMBIENT;
    // Enable the appropriate flag when requesting a REAGLD waveform (NTX_WFM_MODE_GLD16 on the Aura)
    if ( waveform_mode == NTX_WFM_MODE_GLD16 ) {
        refarea.flags = EPDC_FLAG_USE_AAD;
    } else if ( waveform_mode == WAVEFORM_MODE_A2 ) {
        // As well as when requesting a 2bit waveform
        refarea.flags = EPDC_FLAG_FORCE_MONOCHROME;
    } else {
        refarea.flags = 0;
    }

    mxc_update(fb, MXCFB_SEND_UPDATE_V1_NTX, refarea, refreshtype, waveform_mode, x, y, w, h);
}
#endif

#if defined(KOBO)
static void refresh_kobo_mk7(Mxcfb& fb, int refreshtype, int waveform_mode, int x, int y, int w, int h, bool dither)
{
    mxcfb_update_data_v2 refarea;
    // TEMP_USE_AMBIENT, not that there was ever any other choice on Kobo...
    refarea.temp = TEMP_USE_AMBIENT;
    // Did we request HW dithering?
    if ( dither ) {
        refarea.dither_mode = EPDC_FLAG_USE_DITHERING_ORDERED;
        if ( waveform_mode == WAVEFORM_MODE_A2 ) {
            refarea.quant_bit = 1;
        } else {
            refarea.quant_bit = 7;
        }
    } else {
        refarea.dither_mode = EPDC_FLAG_USE_DITHERING_PASSTHROUGH;
        refarea.quant_bit = 0;
    }
    // Enable the appropriate flag when requesting a 2bit update, provided we're not dithering.
    // NOTE: As of right now (FW 4.9.x), WAVEFORM_MODE_GLD16 appears not to be used by Nickel,
    //       so we don't have to care about EPDC_FLAG_USE_REGAL
    if ( waveform_mode == WAVEFORM_MODE_A2 && !dither ) {
        refarea.flags = EPDC_FLAG_FORCE_MONOCHROME;
    } else {
        refarea.flags = 0;
    }

    return mxc_update(fb, MXCFB_SEND_UPDATE_V2, refarea, refreshtype, waveform_mode, x, y, w, h);
}
#endif

Mxcfb::Mxcfb(int device)
    : fbd(-1)
    , width(0)
    , height(0)
    , mech_wait_update_complete(NULL)
    , mech_wait_update_submission(NULL)
    , waveform_partial(0)
    , waveform_ui(0)
    , waveform_flashui(0)
    , waveform_full(0)
    , waveform_fast(0)
    , waveform_reagl(0)
    , waveform_night(0)
    , waveform_flashnight(0)
    , night_is_reagl(false) // TODO: ???
    , night_mode(false)
    , can_hw_invert(false)
    , can_hw_dither(false)
    , mech_refresh(NULL)
    , marker(0)
    , dont_wait_for_marker(0)
    , debug(false)
{
#if defined(KINDLE)
    mech_refresh = refresh_k51;
    mech_wait_update_complete = kindle_pearl_mxc_wait_for_update_complete;
    mech_wait_update_submission = kindle_mxc_wait_for_update_submission;

    waveform_fast = WAVEFORM_MODE_A2;
    waveform_ui = WAVEFORM_MODE_GC16_FAST;
    waveform_flashui = waveform_ui;
    waveform_full = WAVEFORM_MODE_GC16;
    waveform_night = WAVEFORM_MODE_GC16;
    waveform_flashnight = waveform_night;
    night_is_reagl = false;

    // New devices are REAGL-aware, default to REAGL
    bool isREAGL = true;

    // Zelda uses a new eink driver, one that massively breaks backward compatibility.
    bool isZelda = false;
    // And because that worked well enough the first time, lab126 did the same with Rex!
    bool isRex = false;
    // But of course, some devices don't actually support all the features the kernel exposes...
    bool isNightModeChallenged = false;

    switch(device)
    {
    case KindleTouch:
    case KindlePaperWhite:
        isREAGL = false;
        break;
    default:
        break;
    };

    if ( KindleOasis2 == device || KindleOasis3 == device ) {
        isZelda = true;
    }

    if ( KindlePaperWhite4 == device ) {
       isRex = true;
    } else if ( KindleTouch4 == device ) { // KindleBasic3
       isRex = true;
       // NOTE: Apparently, the KT4 doesn't actually support the fancy nightmode waveforms, c.f., ko/#5076
       //       It also doesn't handle HW dithering, c.f., base/#1039
       isNightModeChallenged = true;
    }

    if ( isREAGL ) {
        mech_wait_update_complete = kindle_carta_mxc_wait_for_update_complete;
        waveform_fast = WAVEFORM_MODE_DU; // NOTE: DU, because A2 looks terrible on REAGL devices. Older devices/FW may be using AUTO in this instance.
        waveform_reagl = WAVEFORM_MODE_REAGL;
        waveform_partial = waveform_reagl;
        // NOTE: GL16_INV is available since FW >= 5.6.x only, but it'll safely fall-back to AUTO on older FWs.
        //       Most people with those devices should be running at least FW 5.9.7 by now, though ;).
        waveform_night = WAVEFORM_MODE_GL16_INV;
        waveform_flashnight = WAVEFORM_MODE_GC16;
    } else {
        waveform_fast = WAVEFORM_MODE_DU; // NOTE: DU, because A2 looks terrible on the Touch, and ghosts horribly. Framework is actually using AUTO for UI feedback inverts.
        waveform_partial = WAVEFORM_MODE_GL16_FAST; // NOTE: Depending on FW, might instead be AUTO w/ hist_gray_waveform_mode set to GL16_FAST
    }

    // NOTE: Devices on the Rex platform essentially use the same driver as the Zelda platform, they're just passing a slightly smaller mxcfb_update_data struct
    if ( isZelda || isRex ) {
        // NOTE: Turns out, nope, it really doesn't work on *any* of 'em :/ (c.f., ko#5884).
        // [[
        // can_hw_dither = true;
        // ]]

        if ( isZelda ) {
            mech_refresh = refresh_zelda;
        } else {
            mech_refresh = refresh_rex;
        }

        waveform_fast = WAVEFORM_MODE_DU;
        waveform_ui = WAVEFORM_MODE_AUTO;
        // NOTE: Possibly to bypass the possibility that AUTO, even when FULL, might not flash (something which holds true for a number of devices, especially on small regions),
        //       Zelda explicitly requests GC16 when flashing an UI element that doesn't cover the full screen...
        //       And it resorts to AUTO when PARTIAL, because GC16_FAST is no more (it points to GC16).
        waveform_flashui = WAVEFORM_MODE_GC16;
        waveform_reagl = WAVEFORM_MODE_ZELDA_GLR16;
        waveform_partial = waveform_reagl;
        // NOTE: Because we can't have nice things, we have to account for devices that do not actuallly support the fancy inverted waveforms...
        if ( isNightModeChallenged ) {
            waveform_night = WAVEFORM_MODE_ZELDA_GL16_INV; // NOTE: Currently points to the bog-standard GL16, but one can hope...
            waveform_flashnight = WAVEFORM_MODE_GC16;
        } else {
            waveform_night = WAVEFORM_MODE_ZELDA_GLKW16;
            night_is_reagl = true;
            waveform_flashnight = WAVEFORM_MODE_ZELDA_GCK16;
        }
    }
#endif

#if defined(KOBO)
    mech_refresh = refresh_kobo;
    mech_wait_update_complete = kobo_mxc_wait_for_update_complete;

    waveform_fast = WAVEFORM_MODE_A2;
    waveform_ui = WAVEFORM_MODE_AUTO;
    waveform_flashui = waveform_ui;
    waveform_full = NTX_WFM_MODE_GC16;
    waveform_partial = WAVEFORM_MODE_AUTO;
    waveform_night = NTX_WFM_MODE_GC16;
    waveform_flashnight = waveform_night;
    night_is_reagl = false;

    // New devices *may* be REAGL-aware, but generally don't expect explicit REAGL requests, default to not.
    bool isREAGL = false;

    // Mark 7 devices sport an updated driver.
    // For now, it appears backward compatibility has been somewhat preserved,
    // but let's use the shiny new stuff!
    bool isMk7 = false;

    // NOTE: AFAICT, the Aura was the only one explicitly requiring REAGL requests...
    if ( device == KoboAura ) { // Kobo_phoenix
        isREAGL = true;
    }

    switch (device)
    {
    case KoboAura2_v2:     // Kobo_star_r2
    case KoboAuraH2O2_v2:  // Kobo_snow_r2
    case KoboClaraHD:      // Kobo_nova
    case KoboForma:        // Kobo_frost
    case KoboLibra:        // Kobo_storm
        isMk7 = true;
        break;
    }

    if ( isREAGL ) {
        waveform_reagl = NTX_WFM_MODE_GLD16;
        waveform_partial = waveform_reagl;
        waveform_fast = WAVEFORM_MODE_DU; // Mainly menu HLs, compare to Kindle's use of AUTO or DU also in these instances ;).
    }

    // NOTE: There's a fun twist to Mark 7 devices:
    //       they do use GLR16 update modes (i.e., REAGL), but they do NOT need/do the PARTIAL -> FULL trick...
    //       We handle that by NOT setting waveform_reagl (so _isREAGLWaveFormMode never matches), and just customizing waveform_partial.
    //       Nickel doesn't wait for completion of previous markers on those PARTIAL GLR16, so that's enough to keep our heuristics intact,
    //       while still doing the right thing everywhere ;).
    if ( isMk7 ) {
        can_hw_dither = true;
        mech_refresh = refresh_kobo_mk7;
        mech_wait_update_complete = kobo_mk7_mxc_wait_for_update_complete;

        waveform_partial = WAVEFORM_MODE_GLR16;
        // NOTE: DU may rarely be used instead of A2 by Nickel, but never w/ the MONOCHROME flag, so, keep using A2 everywhere on our end.
    }
#endif
}

void Mxcfb::refreshPartial(int x, int y, int w, int h, bool dither)
{
#if defined(KINDLE) || defined(KOBO)
    mech_refresh(*this, UPDATE_MODE_PARTIAL, waveform_partial, x, y, w, h, dither);
#endif
}

void Mxcfb::refreshFlashPartial(int x, int y, int w, int h, bool dither)
{
#if defined(KINDLE) || defined(KOBO)
    mech_refresh(*this, UPDATE_MODE_FULL, waveform_partial, x, y, w, h, dither);
#endif
}

void Mxcfb::refreshUI(int x, int y, int w, int h, bool dither)
{
#if defined(KINDLE) || defined(KOBO)
    mech_refresh(*this, UPDATE_MODE_PARTIAL, waveform_ui, x, y, w, h, dither);
#endif
}

void Mxcfb::refreshFlashUI(int x, int y, int w, int h, bool dither)
{
#if defined(KINDLE) || defined(KOBO)
    mech_refresh(*this, UPDATE_MODE_FULL, waveform_flashui, x, y, w, h, dither);
#endif
}

void Mxcfb::refreshFull(int x, int y, int w, int h, bool dither)
{
#if defined(KINDLE) || defined(KOBO)
    mech_refresh(*this, UPDATE_MODE_FULL, waveform_full, x, y, w, h, dither);
#endif
}

void Mxcfb::refreshFast(int x, int y, int w, int h, bool dither)
{
#if defined(KINDLE) || defined(KOBO)
    mech_refresh(*this, UPDATE_MODE_PARTIAL, waveform_fast, x, y, w, h, dither);
#endif
}
