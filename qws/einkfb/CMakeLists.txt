project(einkfb LANGUAGES CXX)
cmake_minimum_required(VERSION 3.2.0)

set(COMMON ../../common)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

include(${COMMON}/CMakeLists_config.txt)

find_package(Qt5 COMPONENTS Gui FontDatabaseSupport ServiceSupport EventDispatcherSupport InputSupport REQUIRED)

add_library(einkfb SHARED 
  mxcfb_kindle.h  
  mxcfb_kobo.h 
  main.cpp  
  mxcfb.cpp mxcfb.h 
  qfbbackingstore.cpp qfbbackingstore_p.h 
  qfbscreen.cpp qfbscreen_p.h 
  qfbwindow.cpp qfbwindow_p.h 
  qlinuxfbintegration.cpp qlinuxfbintegration.h 
  qlinuxfbscreen.cpp qlinuxfbscreen.h
  kobots.cpp kobots.h
  einkfb.json
)
target_link_libraries(einkfb PRIVATE Qt5::Gui Qt5::FontDatabaseSupport Qt::EventDispatcherSupport)
target_compile_options(einkfb PRIVATE -std=c++11)

target_include_directories(einkfb PRIVATE
  ${COMMON}
  ${Qt5Gui_PRIVATE_INCLUDE_DIRS}
  ${Qt5FontDatabaseSupport_PRIVATE_INCLUDE_DIRS}
  ${Qt5ServiceSupport_PRIVATE_INCLUDE_DIRS}
  ${Qt5EventDispatcherSupport_PRIVATE_INCLUDE_DIRS}
  ${Qt5InputSupport_PRIVATE_INCLUDE_DIRS}
)
