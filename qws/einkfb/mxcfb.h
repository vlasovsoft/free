#ifndef MXCFB_H
#define MXCFB_H

#include <stdint.h>

class Mxcfb;

typedef int (*mech_wait_update_complete_t)(int,uint32_t);
typedef int (*mech_wait_update_submission_t)(int,uint32_t);
typedef void (*mech_rerfesh_t)(Mxcfb&,int,int,int,int,int,int,bool);

// koreader-base/ffi/framebuffer_mxcfb.lua
// rev. d4706738381c378114c41e8b931c4630b14a9f99
// ported to C++

class Mxcfb
{
public:
    int fbd;
    int width;
    int height;

    mech_wait_update_complete_t mech_wait_update_complete;
    mech_wait_update_submission_t mech_wait_update_submission;

    int waveform_partial;
    int waveform_ui;
    int waveform_flashui;
    int waveform_full;
    int waveform_fast;
    int waveform_reagl;
    int waveform_night;
    int waveform_flashnight;
    bool night_is_reagl;
    bool night_mode;
    bool can_hw_invert;
    bool can_hw_dither;
    mech_rerfesh_t mech_refresh;
    uint32_t marker;
    uint32_t dont_wait_for_marker;
    bool debug;

    bool isUIWaveFormMode(int waveform_mode);
    bool isFlashUIWaveFormMode(int waveform_mode);
    bool isREAGLWaveFormMode(int waveform_mode);
    bool isNightREAGL();
    bool isFastWaveFormMode(int waveform_mode);
    bool isPartialWaveFormMode(int waveform_mode);
    bool isFullScreen(int w, int h);
    int getNightWaveFormMode();
    int getFlashNightWaveFormMode();
    uint32_t get_next_marker();

private:
    Mxcfb(int device);

public:
    static Mxcfb* instance();
    void refreshPartial(int x, int y, int w, int h, bool dither);
    void refreshFlashPartial(int x, int y, int w, int h, bool dither);
    void refreshUI(int x, int y, int w, int h, bool dither);
    void refreshFlashUI(int x, int y, int w, int h, bool dither);
    void refreshFull(int x, int y, int w, int h, bool dither);
    void refreshFast(int x, int y, int w, int h, bool dither);
};

#endif
