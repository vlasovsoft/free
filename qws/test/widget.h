#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

    QTimer timer;
    int counter;

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btnMsgBox_clicked();
    void on_btnClose_clicked();
    void on_btnReset_clicked();
    void timeout();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
