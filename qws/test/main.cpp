#include "widget.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setCursorFlashTime(0);
    Widget w;
    w.showFullScreen();
    return a.exec();
}
