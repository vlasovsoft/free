#include <QDebug>
#include <QThread>
#include <QMessageBox>

#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , counter(0)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));
    ui->lineEdit->setText("11111 22222 33333 44444");
    //timer.start(1000);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnMsgBox_clicked()
{
    QMessageBox::information(this, "QWS", "Hello, QWS !!!", QMessageBox::Ok);
}

void Widget::on_btnClose_clicked()
{
    close();
}

void Widget::on_btnReset_clicked()
{
    counter = 0;
    ui->label->setText(QString("Counter: %1").arg(counter));
}

void Widget::timeout()
{
    counter++;
    ui->label->setText(QString("Counter: %1").arg(counter));
}

void Widget::mousePressEvent(QMouseEvent *event) {
    //qDebug() << "mouse press:" << event->buttons() << event->button();
}

void Widget::mouseReleaseEvent(QMouseEvent *event) {
    //qDebug() << "mouse release:" << event->buttons() << event->button();
}

void Widget::mouseMoveEvent(QMouseEvent *event) {
    //qDebug() << "mouse move:" << event->buttons() << event->button();
}
