#include <QApplication>
#include <QDebug>

#include "inputpanel.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    InputPanel w;
    w.show();
    return a.exec();
}
