#ifndef WINDOWSTACK_H
#define WINDOWSTACK_H

#include <list>
#include <QRegion>

#include "window.h"

class WindowStack
{
public:
    typedef std::list<Window*> Stack;

private:
    Stack stack;

public:
    WindowStack();

    bool isEmpty() const { return stack.empty(); }

    Window* top() { return isEmpty()? NULL:stack.front(); }
    Window* topVisible();
    Window* find(QLocalSocket* sock, int id);
    Window* findWindowAt(const QPoint& pt, bool mask);
    void add(Window* w);
    void remove(Window* w);
    void raise(Window* w);
    void lower(Window* w);

    Stack& getList() { return stack; }
};

#endif // WINDOWSTACK_H
