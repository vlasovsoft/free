#include "windowstack.h"

WindowStack::WindowStack()
{}

Window *WindowStack::topVisible()
{
    auto i =
        std::find_if(stack.begin(), stack.end(), [](const Window* w) {
            return w->isVisible();
        });
    return i == stack.end()? NULL : *i;
}

Window* WindowStack::find(QLocalSocket* sock, int id)
{
    auto i =
        std::find_if(stack.begin(), stack.end(), [id,sock](const Window* w) {
            return sock == w->socket() && id == w->getId();
        });
    return i == stack.end()? NULL : *i;
}

Window* WindowStack::findWindowAt(const QPoint& pt, bool mask)
{
    Window* result = NULL;
    for( auto item: stack ) {
        if ( item->isVisible() ) {
            QRegion r(item->getMask());
            if ( r.contains(pt) ) {
                result = item;
                break;
            }
        }
    }
    return result;
}

void WindowStack::add(Window *w)
{
    stack.push_front(w);
}

void WindowStack::remove(Window *w)
{
    stack.erase(std::remove_if(stack.begin(), stack.end(), [w](Window* win){
        return w == win;
    }));
}

void WindowStack::raise(Window *w)
{
    if ( w != NULL && !stack.empty() && w != stack.front() ) {
        remove(w);
        // find first non-modal window and insert before it
        auto i =
            std::find_if(stack.begin(), stack.end(), [](const Window* w) {
                return !w->isModal();
            });
        stack.insert(i, w);
    }
}

void WindowStack::lower(Window *w)
{
    if ( !stack.empty() && w != stack.back() ) {
        remove(w);
        stack.push_back(w);
    }
}
