#ifndef TOUCHSCREEN_H
#define TOUCHSCREEN_H

#include <QObject>
#include <QPoint>

class QSocketNotifier;
class WindowStack;

class TouchScreen : public QObject
{
    Q_OBJECT

public:
    TouchScreen(QObject *parent, WindowStack* ws);
    ~TouchScreen();

private slots:
    void activity(int);
    void activityMt(int);

private:
    void processMouseEvent();

private:
    int fd;
    int slot;
    QSocketNotifier* sn;
    Qt::MouseButton buttons;
    QPoint oldPos, newPos;
    WindowStack* windows;
};

#endif // TOUCHSCREEN_H
