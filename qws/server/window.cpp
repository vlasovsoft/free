#include <QLocalSocket>

#include "qws_proto.h"

#include "window.h"

Window::Window(QLocalSocket* s, int id_)
    : id(id_)
    , sock(s)
    , visible(false)
    , modal(false)
    , flags(Qt::Widget)
    , state(Qt::WindowNoState)
{
}

Window::~Window()
{
}

void Window::sendMouseEvent(const QPoint &pt, QEvent::Type type)
{
    QRect geom(getGeometry());
    QPoint lt(pt.x()-geom.x(), pt.y()-geom.y()); // local coords
    sendMouseEventServer(sock, id, lt, pt, type);
}
