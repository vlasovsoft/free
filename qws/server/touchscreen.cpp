#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>

#include <QDebug>
#include <QSocketNotifier>

#include "touchscreen.h"

static QString event_type(int val) {
    switch(val) {
    case EV_ABS:
        return "EV_ABS";
    case EV_KEY:
        return "EV_KEY";
    case EV_SYN:
        return "EV_SYN";
    }
    return QString::number(val);
}

static QString event_code(int type, int val) {
    switch(type)
    {
    case EV_ABS:
        switch(val)
        {
        case ABS_X:
            return "ABS_X";
        case ABS_Y:
            return "ABS_Y";
        case ABS_MT_POSITION_X:
            return "ABS_MT_POSITION_X";
        case ABS_MT_POSITION_Y:
            return "ABS_MT_POSITION_Y";
        case ABS_MT_TRACKING_ID:
            return "ABS_MT_TRACKING_ID";
        case BTN_TOUCH:
            return "BTN_TOUCH";
        }
        break;
     case EV_KEY:
        switch(val)
        {
        case BTN_TOUCH:
            return "BTN_TOUCH";
        }
        break;
     case EV_SYN:
        switch(val)
        {
        case SYN_REPORT:
            return "SYN_REPORT";
        case SYN_MT_REPORT:
            return "SYN_MT_REPOR";
        }
        break;
    }

    return QString::number(val);
}

TouchScreen::TouchScreen(QObject *parent, WindowStack* ws)
    : QObject(parent)
    , fd(-1)
    , slot(0)
    , sn(NULL)
    , buttons(Qt::NoButton)
    , windows(ws)
{
    fd = open("/dev/input/event22", O_RDONLY);
    if ( fd != -1 ) {
        sn = new QSocketNotifier(fd, QSocketNotifier::Read);
        QObject::connect(sn, SIGNAL(activated(int)), this, SLOT(activityMt(int)));
    }
}

TouchScreen::~TouchScreen()
{
    if ( fd != -1 ) {
        close(fd);
        delete sn;
    }
}

void TouchScreen::activity(int)
{
    input_event in;
    unsigned size = 0;

    while ( size < sizeof(input_event) )
    {
        ssize_t s = read(fd, ((char*)&in)+size, sizeof(input_event)-size);
        if ( -1 == s || 0 == s ) return;
            size += s;
    }

    qDebug() << event_type(in.type) << " " << event_code(in.type, in.code) << " " << in.value;

    switch ( in.type )
    {
    case EV_ABS:
        switch ( in.code )
        {
        case ABS_X:
            newPos.setX(in.value);
            break;
        case ABS_Y:
            newPos.setY(in.value);
            break;
        default:
            break;
        }
        break;
    case EV_KEY:
        if (BTN_TOUCH == in.code) {
            buttons = in.value != 0 ? Qt::LeftButton : Qt::NoButton;
        }
        break;
    case EV_SYN:
        if ( 0 == in.code )
        {
            qDebug("Mouse changed: x=%d, y=%d, val=%d", newPos.x(), newPos.y(), buttons);
        }
        oldPos = newPos;
        break;
    default:
        break ;
    }
}

void TouchScreen::activityMt(int)
{
    input_event in;
    unsigned size = 0;

    while ( size < sizeof(input_event) )
    {
        ssize_t s = read(fd, ((char*)&in)+size, sizeof(input_event)-size);
        if ( -1 == s || 0 == s ) return;
            size += s;
    }

    qDebug() << event_type(in.type) << " " << event_code(in.type, in.code) << " " << in.value;

    switch ( in.type )
    {
    case EV_ABS:
        switch ( in.code )
        {
        case ABS_MT_POSITION_X:
            if ( 0 == slot ) {
              newPos.setX(in.value);
            }
            break;
        case ABS_MT_POSITION_Y:
            if ( 0 == slot ) {
                newPos.setY(in.value);
            }
            break;
        case ABS_MT_TRACKING_ID:
            if ( 0 == slot ) {                
                buttons = in.value > 0 ? Qt::LeftButton : Qt::NoButton;
            }
            break;
        default:
            break;
        }
        break;

    case EV_SYN:
        if ( 0 == in.code )
        {
            qDebug("Mouse changed: x=%d, y=%d, val=%d", newPos.x(), newPos.y(), buttons);
        }
        oldPos = newPos;
        break;

    default:
        break ;
    }
}
