#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>

#include <algorithm>

#include <QSocketNotifier>

#include "qws_proto.h"
#include "touchscreen.h"
#include "application.h"

static QString event_type(int val) {
    switch(val) {
    case EV_ABS:
        return "EV_ABS";
    case EV_KEY:
        return "EV_KEY";
    case EV_SYN:
        return "EV_SYN";
    }
    return QString::number(val);
}

static QString event_code(int type, int val) {
    switch(type)
    {
    case EV_ABS:
        switch(val)
        {
        case ABS_X:
            return "ABS_X";
        case ABS_Y:
            return "ABS_Y";
        case ABS_MT_POSITION_X:
            return "ABS_MT_POSITION_X";
        case ABS_MT_POSITION_Y:
            return "ABS_MT_POSITION_Y";
        case ABS_MT_TRACKING_ID:
            return "ABS_MT_TRACKING_ID";
        case BTN_TOUCH:
            return "BTN_TOUCH";
        }
        break;
     case EV_KEY:
        switch(val)
        {
        case BTN_TOUCH:
            return "BTN_TOUCH";
        }
        break;
     case EV_SYN:
        switch(val)
        {
        case SYN_REPORT:
            return "SYN_REPORT";
        case SYN_MT_REPORT:
            return "SYN_MT_REPOR";
        }
        break;
    }

    return QString::number(val);
}

Application::Application(int &argc, char **argv)
    : QCoreApplication(argc, argv)
    , tsFd(-1)
    , tsSlot(0)
    , oldBtn(false)
    , newBtn(false)
{
    qDebug() << "QWS server started: " << applicationPid();

    tsFd = open("/dev/input/event22", O_RDONLY);
    if ( tsFd != -1 ) {
        sn = new QSocketNotifier(tsFd, QSocketNotifier::Read);
        QObject::connect(sn, SIGNAL(activated(int)), this, SLOT(activityTsMt(int)));
    }
    QObject::connect( &serv, SIGNAL(newConnection()), this, SLOT(newConnection()) );
    bool isListen = serv.listen(QWS_SERVER);
    if ( !isListen ) {
        isListen = QLocalServer::removeServer(QWS_SERVER) && serv.listen(QWS_SERVER);
    }
    if ( !isListen ) {
        qDebug() << "Listen error!";
        exit(EXIT_FAILURE);
    }
}

Application::~Application()
{
    if ( tsFd != -1 ) {
        close(tsFd);
        delete sn;
    }
    serv.close();
}

void Application::newConnection()
{
    QLocalSocket* sock = serv.nextPendingConnection();
    connect(sock, SIGNAL(disconnected()), this, SLOT(disconnect()));
    connect(sock, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(sock, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(error(QLocalSocket::LocalSocketError)));
}

void Application::readyRead()
{
    QLocalSocket* sock = qobject_cast<QLocalSocket*>(sender());
    while ( (unsigned)sock->bytesAvailable() >= sizeof(int) )
    {
        int cmd = qwsNone;
        ::sock_read_int(sock, cmd);
        switch ( cmd )
        {
        case qwsWindowCreate:
            {
                QRect geom;
                int id = ::createWindowServer(sock, geom);
                Window* w = new Window(sock, id);
                w->setGeometry(geom);
                windows.add(w);
                qDebug() << "Window" << w->getId() << "is created: " << geom;
            }
            break;

        case qwsWindowFlushRequest:
            {
                QRegion r; // local (window) coordinates
                int id = ::flushWindowRequestServer(sock, r);
                Window* w = windows.find( sock, id );
                if ( w != NULL ) {
                    r.translate(w->getGeometry().topLeft());
                    updateRegion(r);
                }
            }
            break;

        case qwsWindowVisible:
            {
                bool visible = false, modal = false;
                int id = ::setWindowVisibleServer(sock, visible, modal);
                Window* w = windows.find( sock, id );
                if ( w != NULL ) {
                    w->setModal(modal);
                    w->setVisible(visible);
                    qDebug() << "setVisible()" << w->getId() << "visible:" << visible;
                    updateRegion(w->getGeometry());
                }
            }
            break;

        case qwsWindowGeometry:
            {
                QRect r;                
                int id = ::setWindowGeometryServer(sock, r);
                Window* w = windows.find( sock, id );
                if ( w != NULL ) {
                    QRegion reg(w->getGeometry());
                    reg += r;
                    w->setGeometry(r);
                    if ( w->isVisible() ) {
                        updateRegion(reg);
                    }
                    qDebug() << "Window" << w->getId() << "setGeometry()" << r;
                }
            }
            break;

        case qwsWindowRaise:
            {
                int id = ::raiseWindowServer(sock);
                Window* w = windows.find( sock, id );
                if ( w != NULL ) {
                    raise(w);
                }
            }
            break;

        case qwsWindowLower:
            {
                int id = ::lowerWindowServer(sock);
                Window* w = windows.find( sock, id );
                if ( w != NULL ) {
                    lower(w);
                }
            }
            break;

        case qwsWindowFlags:
            {
                int flags = 0;
                int id = ::setWindowFlagsServer(sock, flags);
                Window* w = windows.find( sock, id );
                if ( w != NULL )
                {
                    w->setFlags(static_cast<Qt::WindowFlags>(flags));
                    qDebug() << "Window" << w->getId() << "setWindowFlags()" << Qt::hex << flags;
                }
            }
            break;

        case qwsWindowState:
            {
                int state = 0;
                int id = ::setWindowStateServer(sock, state);
                Window* w = windows.find( sock, id );
                if ( w != NULL )
                {
                    w->setState(static_cast<Qt::WindowStates>(state));
                    qDebug() << "Window" << w->getId() << "setWindowState()" << Qt::hex << state;
                }
            }
            break;

        case qwsWindowMask:
            {
                QRegion mask;
                int id = ::setWindowMaskServer(sock, mask);
                Window* w = windows.find( sock, id );
                if ( w != NULL )
                {                    
                    w->setMask(mask);
                    qDebug() << "Window" << w->getId() << "setWindowMask()" << mask;
                }
            }
            break;
        }
    }
}

void Application::disconnect()
{
    QLocalSocket* sock = qobject_cast<QLocalSocket*>(sender());
    auto iEnd = std::remove_if(
        windows.getList().begin(),
        windows.getList().end(),
        [sock](Window* w) {
        return w->socket() == sock;
    });
    for ( auto i = iEnd; i != windows.getList().end(); ++i ) {
        qDebug() << "Window" << (*i)->getId() << "is destroyed";
        delete *i;
    }
    windows.getList().erase(iEnd, windows.getList().end());
}

void Application::error(QLocalSocket::LocalSocketError socketError)
{
}

void Application::activityTs(int)
{
    input_event in;
    unsigned size = 0;

    while ( size < sizeof(input_event) )
    {
        ssize_t s = read(tsFd, ((char*)&in)+size, sizeof(input_event)-size);
        if ( -1 == s || 0 == s ) return;
            size += s;
    }

    // qDebug() << event_type(in.type) << " " << event_code(in.type, in.code) << " " << in.value;

    switch ( in.type )
    {
    case EV_ABS:
        switch ( in.code )
        {
        case ABS_X:
            newPos.setX(in.value);
            break;
        case ABS_Y:
            newPos.setY(in.value);
            break;
        default:
            break;
        }
        break;
    case EV_KEY:
        if (BTN_TOUCH == in.code) {
            newBtn = in.value > 0;
        }
        break;
    case EV_SYN:
        if ( 0 == in.code )
        {
            QEvent::Type type = QEvent::None;
            if ( newBtn ) {
                type = oldBtn ? QEvent::MouseMove : QEvent::MouseButtonPress;
            } else {
                type = oldBtn ? QEvent::MouseButtonRelease : QEvent::None;
            }
            if ( type != QEvent::None ) {
                // qDebug("Mouse changed: x=%d, y=%d, type=%d", newPos.x(), newPos.y(), type);
                processMouseEvent(newPos, type);
            }
            oldBtn = newBtn;
        }
        oldPos = newPos;
        break;
    default:
        break ;
    }
}

void Application::activityTsMt(int)
{
    input_event in;
    unsigned size = 0;

    while ( size < sizeof(input_event) )
    {
        ssize_t s = read(tsFd, ((char*)&in)+size, sizeof(input_event)-size);
        if ( -1 == s || 0 == s ) return;
            size += s;
    }

    // qDebug() << event_type(in.type) << " " << event_code(in.type, in.code) << " " << in.value;

    switch ( in.type )
    {
    case EV_ABS:
        switch ( in.code )
        {
        case ABS_MT_POSITION_X:
            if ( 0 == tsSlot ) {
              newPos.setX(in.value);
            }
            break;
        case ABS_MT_POSITION_Y:
            if ( 0 == tsSlot ) {
                newPos.setY(in.value);
            }
            break;
        case ABS_MT_TRACKING_ID:
            if ( 0 == tsSlot ) {
                newBtn = in.value > 0;
            }
            break;
        default:
            break;
        }
        break;

    case EV_SYN:
        if ( 0 == in.code )
        {
            QEvent::Type type = QEvent::None;
            if ( newBtn ) {
                type = oldBtn ? QEvent::MouseMove : QEvent::MouseButtonPress;
            } else {
                type = oldBtn ? QEvent::MouseButtonRelease : QEvent::None;
            }
            if ( type != QEvent::None ) {
                // qDebug("Mouse changed: x=%d, y=%d, type=%d", newPos.x(), newPos.y(), type);
                processMouseEvent(newPos, type);
            }
            oldBtn = newBtn;
        }
        oldPos = newPos;
        break;

    default:
        break ;
    }
}

void Application::updateRegion(const QRegion& r)
{
    if ( !r.isEmpty() ) {
        for ( auto
              i  = windows.getList().rbegin();
              i != windows.getList().rend();
              ++i ) {
            if ( (*i)->isVisible() ) {
                Window* w = (*i);
                QRegion upd = r.intersected(w->getGeometry());
                if ( !upd.isEmpty() ) {
                    ::flushWindowResponseServer(w->socket(), w->getId(), upd);
                    qDebug() << "flush: " << w->getId() << upd;
                }
            }
        }
    }
}

void Application::processMouseEvent(const QPoint &pt, QEvent::Type type)
{
    switch(type) {
    case QEvent::MouseButtonPress:
        {
            Window* w = NULL;
            w = windows.findWindowAt(pt, true);
            if (w != NULL) {
                w->sendMouseEvent(pt, type);
            }
        }
        break;
    case QEvent::MouseMove:
    case QEvent::MouseButtonRelease:
        {
            Window* w = windows.findWindowAt(pt, true);
            if (w != NULL) {
                w->sendMouseEvent(pt, type);
            }
        }
        break;
    default:
        break;
    }
}

void Application::raise(Window *w)
{
    if ( w != NULL ) {
        windows.raise(w);
        if ( w->isVisible() ) {
            updateRegion(w->getGeometry());
        }
        qDebug() << "Window" << w->getId() << "raise()";
    }
}

void Application::lower(Window *w)
{
    if ( w != NULL ) {
        windows.lower(w);
        if ( w->isVisible() ) {
            updateRegion(w->getGeometry());
        }
        qDebug() << "Window" << w->getId() << "lower()";
    }
}
