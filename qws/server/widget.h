#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLocalServer>
#include <QLocalSocket>
#include <QList>
#include <QImage>
#include <QSharedMemory>

#include "qws_proto.h"

class Window;
class InputPanel;

class Widget : public QWidget
{
Q_OBJECT

    QLocalServer serv;
    QWSScreenData data;

    InputPanel* iPanel;

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void keyPressEvent(QKeyEvent*);
    void keyReleaseEvent(QKeyEvent*);

private slots:
    void newConnection();
    void readyRead();
    void disconnect();
    void error(QLocalSocket::LocalSocketError socketError);
    void showInputPanel(bool show);
    void rotateInputPanel(int v);

private:
    Window* findWindow( QLocalSocket* sock, void* id );
    QSize screenSize() const;
    int screenRotation() const;
    void setScreenRotation(int v, bool apply=true);
};

#endif // WIDGET_H
