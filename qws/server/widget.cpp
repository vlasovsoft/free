#include <QDebug>
#include <QPainter>
#include <QTimer>
#include <QMenu>
#include <QKeyEvent>

#include "qws_proto.h"
#include "inputpanel.h"
#include "window.h"

#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , data(true)
    , iPanel(new InputPanel(this))
{
    connect( &serv, SIGNAL(newConnection()), this, SLOT(newConnection()) );
    bool isListen = false;
    isListen = serv.listen(QWS_SERVER);
    if ( !isListen )
    {
        isListen = QLocalServer::removeServer(QWS_SERVER) && serv.listen(QWS_SERVER);
    }
    if ( !isListen )
    {
        qDebug() << "Listen error!";
        exit(EXIT_FAILURE);
    }

    setFixedSize(QSize(QWS_WIDTH,QWS_HEIGHT));

    setScreenRotation(0, false);
    showInputPanel(false);
}

Widget::~Widget()
{
    serv.close();
}

void Widget::keyPressEvent(QKeyEvent* e)
{
    if ( iPanel->activeWindow() != NULL )
    {
        iPanel->activeWindow()->sendKeyEvent( e->type(), e->key(), e->modifiers(), e->text() );
    }
}

void Widget::keyReleaseEvent(QKeyEvent* e)
{
    if ( e->key() >= Qt::Key_F1 && e->key() <= Qt::Key_F4 )
    {
        switch ( e->key() )
        {
        case Qt::Key_F1:
            setScreenRotation(0, true);
            break;
        case Qt::Key_F2:
            setScreenRotation(90, true);
            break;
        case Qt::Key_F3:
            setScreenRotation(180, true);
            break;
        case Qt::Key_F4:
            setScreenRotation(270, true);
            break;
        }
    }
    else
    {
        if ( iPanel->activeWindow() != NULL )
        {
            iPanel->activeWindow()->sendKeyEvent( e->type(), e->key(), e->modifiers(), e->text() );
        }
    }
}

void Widget::newConnection()
{
    QLocalSocket* sock = serv.nextPendingConnection();
    connect(sock, SIGNAL(disconnected()), this, SLOT(disconnect()));
    connect(sock, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(sock, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(error(QLocalSocket::LocalSocketError)));
    qDebug() << "New connection!";
}

void Widget::readyRead()
{
    QLocalSocket* sock = qobject_cast<QLocalSocket*>(sender());   
    while ( (unsigned)sock->bytesAvailable() > 0 )
    {
        int cmd = qwsNone;
        void* id = NULL;
        ::sock_read(sock, &cmd, sizeof(int));
        switch ( cmd )
        {
        case qwsWindowCreate:
            {
                ::sock_read(sock, &id, sizeof(void*));
                new Window(this, sock, &data, id);
            }
            break;
        case qwsWindowDestroy:
            {
                ::sock_read(sock, &id, sizeof(id));
                Window* w = findWindow( sock, id );
                if ( w != NULL ) {
                    delete w;
                    qDebug() << "Window is destroyed";
                }
            }
            break;
        case qwsWindowVisible:
            {
                bool visible = false;
                ::sock_read(sock, &id, sizeof(id));
                ::sock_read(sock, &visible, sizeof(bool));
                qDebug() << "Window visible: " << id << " " << visible;
                Window* w = findWindow( sock, id );
                if ( w != NULL ) {
                    w->setVisible( visible );
                }
            }
            break;
        case qwsWindowState:
            {
                Qt::WindowState state = Qt::WindowNoState;
                ::sock_read(sock, &id, sizeof(id));
                ::sock_read(sock, &state, sizeof(Qt::WindowState));
                qDebug() << "Window state: " << id << " " << state;
                Window* w = findWindow( sock, id );
                if ( w != NULL ) {
                    w->setWindowState( state );
                }
            }
            break;
        case qwsWindowGeometry:
            {
                QRect r;
                ::sock_read(sock, &id, sizeof(id));
                ::sock_read(sock, &r, sizeof(QRect));
                Window* w = findWindow( sock, id );
                if ( w != NULL ) {
                    w->setClientGeometry( r );
                    qDebug() << "Window geometry: " << r;
                }
            }
            break;
        case qwsWindowFlush:
            {
                QRect r;
                ::sock_read(sock, &id, sizeof(id));
                ::sock_read(sock, &r, sizeof(QRect));
                qDebug() << "Update: " << id << " " << r;
                Window* w = findWindow( sock, id );
                if ( w != NULL )
                {
                    qDebug() << "====>" << r << " " << clientToScreen(w->size(), data.getRotation(), r);
                    w->update(clientToScreen(w->size(), data.getRotation(), r));
                }
            }
            break;
        case qwsBackingStore:
            {
                QSize s;
                qint64 pid = 0;
                int seq = 0;
                ::sock_read(sock, &id, sizeof(id));
                ::sock_read(sock, &pid, sizeof(pid));
                ::sock_read(sock, &seq, sizeof(seq));
                ::sock_read(sock, &s, sizeof(s));
                Window* w = findWindow( sock, id );
                qDebug() << " attach shared memory: " << s;
                if ( w != NULL )
                {
                    w->attachSharedMemory( s, pid, seq );
                }
            }
            break;
        case qwsInputPanel:
            {
                bool show = false;
                ::sock_read(sock, &show, sizeof(show));
                showInputPanel(show);
                qDebug() << " show input panel: " << show;
            }
            break;
        case qwsWindowRaise:
            {
                ::sock_read(sock, &id, sizeof(id));
                Window* w = findWindow( sock, id );
                if ( w != NULL )
                {
                    w->raise();
                }
            }
            break;
        case qwsWindowLower:
            {
                ::sock_read(sock, &id, sizeof(id));
                Window* w = findWindow( sock, id );
                if ( w != NULL )
                {
                    w->lower();
                }
            }
            break;
        case qwsWindowActivate:
            {
                ::sock_read(sock, &id, sizeof(id));
                Window* w = findWindow( sock, id );
                if ( w != NULL )
                {
                    iPanel->setActiveWindow(w);
                }
            }
            break;
        }
    }
}

void Widget::disconnect()
{
    qDebug() << "Client disconnected!";
    QList<Window*> list = this->findChildren<Window*>();
    foreach(Window* w, list) {
        if ( w->getSocket() == sender() ) {
            delete w;
        }
    }
    //if ( list.isEmpty() )
    //    close();
}

void Widget::error(QLocalSocket::LocalSocketError socketError)
{
    qDebug() << socketError;
}

void Widget::showInputPanel(bool show)
{
    if ( show )
    {
        iPanel->raise();
        iPanel->show();
    }
    else
    {
        iPanel->hide();
    }
}

void Widget::rotateInputPanel(int v)
{
    QRect geom;
    switch( v )
    {
    case 0:
        geom.setWidth(width());
        geom.setHeight(qMin(iPanel->heightForWidth(geom.width()), height()/2));
        geom.moveTo(0, height()-geom.height());
        break;
    case 90:
        geom.setHeight(height());
        geom.setWidth(qMin(iPanel->heightForWidth(geom.height()), width()/2));
        geom.moveTo(width()-geom.width(), 0);
        break;
    case 180:
        geom.setWidth(width());
        geom.setHeight(qMin(iPanel->heightForWidth(geom.width()), height()/2));
        geom.moveTo(0, 0);
        break;
    case 270:
        geom.setHeight(height());
        geom.setWidth(qMin(iPanel->heightForWidth(geom.height()), width()/2));
        geom.moveTo(0, 0);
    }
    iPanel->setRotation(v);
    iPanel->setGeometry(geom);
}

Window* Widget::findWindow(QLocalSocket *sock, void* id)
{
    QList<Window*> list = this->findChildren<Window*>();
    foreach(Window* w, list) {
        if ( w->getId() == id && w->getSocket() == sock ) {
            return w;
        }
    }
    return NULL;
}

void Widget::setScreenRotation(int v, bool apply)
{
    int v1 = data.getRotation();
    data.setRotation(v);
    switch( v )
    {
    case 0:
    case 180:
    default:
        data.setSize(QSize(width(),height()));
        break;
    case 90:
    case 270:
        data.setSize(QSize(height(),width()));
        break;
    }
    if ( apply && v1 != v )
    {
        bool resize = 90 == abs(v-v1) || 270 == abs(v-v1);
        QSet<QLocalSocket*> set;
        QList<Window*> list = findChildren<Window*>();
        foreach(Window* w, list) {
            if ( !resize || ( 0 == w->windowState() && Qt::WindowFullScreen ) )
            {
                w->updateGeometry();
            }
            if ( resize )
            {
                set.insert(w->getSocket());
            }
        }
        foreach( QLocalSocket* sock, set ) {
            int cmd = qwsRotationEvent;
            sock_write(sock, &cmd, sizeof(int));
            sock->flush();
        }
    }
    rotateInputPanel(v);
}
