#ifndef WINDOW_H
#define WINDOW_H

#include <QRect>
#include <QRegion>
#include <QEvent>

class QLocalSocket;

class Window
{
    int id;
    QLocalSocket* sock;
    bool visible;
    bool modal;
    QRect geometry;
    QRegion mask;
    Qt::WindowFlags flags;
    Qt::WindowStates state;

public:
    Window(QLocalSocket* s, int id_);
    ~Window();

    int getId() const { return id; }
    QLocalSocket* socket() const { return sock; }
    QRect getGeometry() const { return geometry; }
    void setGeometry(const QRect& r) { geometry = r; }    
    bool isVisible() const { return visible; }
    void setVisible(bool val) { visible = val; }
    bool isModal() const { return modal; }
    void setModal(bool val) { modal = val; }
    Qt::WindowFlags getFlags() const { return flags; }
    void setFlags(Qt::WindowFlags val) { flags = val; }
    Qt::WindowStates getState() const { return state; }
    void setState(Qt::WindowStates val) { state = val; }
    QRegion getMask() const { return mask.isEmpty()? geometry:mask; }
    void setMask( const QRegion& val ) { mask = val; }
    void sendMouseEvent(const QPoint& pt, QEvent::Type type);
};

#endif // WINDOW_H
