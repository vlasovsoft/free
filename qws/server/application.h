#ifndef APPLICATION_H
#define APPLICATION_H

#include <list>

#include <QCoreApplication>
#include <QLocalServer>
#include <QLocalSocket>

#include "windowstack.h"

#include "window.h"

class QSocketNotifier;
class QRegion;

class Application : public QCoreApplication
{
Q_OBJECT
    QLocalServer serv;

public:
    Application(int &argc, char **argv);
    ~Application();

private slots:
    void newConnection();
    void readyRead();
    void disconnect();
    void error(QLocalSocket::LocalSocketError socketError);
    void activityTs(int);
    void activityTsMt(int);
    void updateRegion(const QRegion& r);

private:
    void processMouseEvent(const QPoint& pt, QEvent::Type type);
    void raise(Window* w);
    void lower(Window* w);

private:   
    int tsFd;
    int tsSlot;
    QSocketNotifier* sn;
    bool oldBtn, newBtn;
    QPoint oldPos, newPos;
    WindowStack windows;
};

#endif // APPLICATION_H
