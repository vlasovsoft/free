#ifndef GADGETMENUITEM_H
#define GADGETMENUITEM_H

#include <QRectF>

class QAction;
class QFontMetrics;
class GadgetMenu;

struct GadgetMenuItem
{
    enum Type {
      Separator,
      Action,
      Menu
    } type;

    QRectF rect;
    QRectF rectText;
    QRectF rectPrefix;
    QRectF rectSuffix;

    void* ptr;

    bool highlight;

    QString text() const;
    bool isEnabled() const;
    bool isVisible() const;

    GadgetMenuItem();
    GadgetMenuItem(QAction* a);
    GadgetMenuItem(GadgetMenu* m);
};

#endif // GADGETMENUITEM_H
