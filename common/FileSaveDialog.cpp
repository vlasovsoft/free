#include <QKeyEvent>

#include "QtUtils.h"
#include "messagebox.h"

#include "FileSaveDialog.h"
#include "ui_FileSaveDialog.h"

FileSaveDialog::FileSaveDialog(QWidget* parent, const QString& title, const QString& path, const QString& ext, bool confirm)
    : Dialog(parent)
    , ui( new Ui::FileSaveDialog )
    , fileExt(ext)
    , isConfirm(confirm)
{
    ui->setupUi(this);
    init();

    QFileInfo fi(path);

    QString fileName;
    QString fileDir;

    if ( fi.isDir() ) {
        // path is dir
        fileDir = path;
    } else {
        // path is dir + file name
        fileDir  = fi.absolutePath();
        fileName = fi.fileName();
    }

    setWindowTitle(title);

    ui->titleBar->titlePush(title);
    ui->titleBar->addSlot(this, SLOT(reject()));

    QFileSystemModel& model = ui->listView->getModel();
    model.setRootPath(fileDir);
    model.setFilter( QDir::Dirs | QDir::AllDirs | QDir::Files | QDir::NoDot );
    model.setNameFilterDisables(false);
    if ( !ext.isEmpty() ) {
        QStringList filters;
        filters.append(QString("*") + ext);
        model.setNameFilters(filters);
        model.setNameFilterDisables(false);
    }

    ui->listView->setRootIndex(model.index(fileDir));

    ui->lineEdit->setText(fileName);
    ui->lblPath->setText(fileDir);
    ui->lblPath->setElideMode( Qt::ElideMiddle );

    QObject::connect(ui->listView, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)));
    QObject::connect(ui->listView, SIGNAL(clicked(QModelIndex)), this, SLOT(select(QModelIndex)));
    QObject::connect(&ui->listView->getModel(), SIGNAL(rootPathChanged(QString)), ui->lblPath, SLOT(setText(QString)) );
}

FileSaveDialog::~FileSaveDialog()
{
    delete ui;
}

void FileSaveDialog::setFileName(const QString &fn)
{
    ui->lineEdit->setText( fn );
}

void FileSaveDialog::setModelFilters(QDir::Filters val)
{
    ui->listView->getModel().setFilter(val);
}

QDir::Filters FileSaveDialog::modelFilters() const
{
    return ui->listView->getModel().filter();
}

void FileSaveDialog::select( const QModelIndex& index )
{
    if ( index.isValid() )
    {
        QFileInfo fInfo = ui->listView->getModel().fileInfo(index);
        if ( fInfo.isFile() ) {
            ui->lineEdit->setText(fInfo.fileName());
        }
    }
}

void FileSaveDialog::accept()
{
    if ( ui->lineEdit->text().isEmpty() ) return;

    QModelIndex index(ui->listView->selectionModel()->currentIndex());

    if ( !index.isValid() )
        index = ui->listView->rootIndex();

    QFileInfo fInfo = ui->listView->getModel().fileInfo(index);
    if ( fInfo.isFile() )
    {
        fileName = fInfo.absolutePath() + QDir::separator() + ui->lineEdit->text();
    }
    else
    if ( fInfo.isDir() )
    {
        fileName = fInfo.absoluteFilePath() + QDir::separator() + ui->lineEdit->text();
    }

    if ( !defaultSuffix.isEmpty() && QFileInfo(fileName).suffix() != defaultSuffix )
        fileName += "." + defaultSuffix;

    if ( !isConfirm || !::fileExistsAndNoOverwrite( this, fileName ) ) {
        Dialog::accept();
    }
}
