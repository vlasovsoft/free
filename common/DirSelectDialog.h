#ifndef FILEOPENDIALOG_H_
#define FILEOPENDIALOG_H_

#include "Dialog.h"

namespace Ui {
class DirSelectDialog;
}

class DirSelectDialog: public Dialog
{
    Q_OBJECT

    Ui::DirSelectDialog* ui;

    QString path;

public:
    DirSelectDialog(QWidget* parent, const QString& title, const QString& path, bool hidden=false);
    ~DirSelectDialog();

    const QString& getPath() const
    { return path; }

private slots:
    void accept();
};

#endif /* FILEOPENDIALOG_H_ */
