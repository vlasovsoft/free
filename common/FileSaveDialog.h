#ifndef FILESAVEDIALOG_H_
#define FILESAVEDIALOG_H_

#include <QDir>

#include "Dialog.h"

class QModelIndex;

namespace Ui {
class FileSaveDialog;
}

class FileSaveDialog: public Dialog
{
    Q_OBJECT

    Ui::FileSaveDialog* ui;

    QString fileName;
    QString fileExt;
    QString defaultSuffix;
    bool isConfirm;

public:
    FileSaveDialog(QWidget* parent, const QString& title, const QString& path, const QString& ext, bool confirm=true);
    ~FileSaveDialog();

    const QString& getFileName() const
    { return fileName; }

    void setFileName( const QString& fn );

    void setDefaultSuffix( const QString& defSuff )
    { defaultSuffix = defSuff; }

    QString getDefaultSuffix() const
    { return defaultSuffix; }

    void setModelFilters( QDir::Filters val );

    QDir::Filters modelFilters() const;

private slots:
    void accept();
    void select( const QModelIndex & index );
};

#endif /* FILESAVEDIALOG_H_ */

