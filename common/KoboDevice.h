#if !defined(KOBO_DEVICE_H)
#define KOBO_DEVICE_H

enum KoboDevice {
    KoboTouch = 1,
    KoboGlo,
    KoboMini,
    KoboAura,
    KoboAuraHD,
    KoboAuraH2O,
    KoboGloHD,
    KoboTouch2,
    KoboAura2_v1,
    KoboAura2_v2,
    KoboAuraH2O2_v1,
    KoboAuraH2O2_v2,
    KoboAuraOne,
    KoboClaraHD,
    KoboForma,
    KoboLibra,
    KoboNia,
    KoboLibra2,
    KoboOther
};

inline KoboDevice get_kobo_device()
{
    QString d(qgetenv("DEVICE"));
    if ( d == "TOUCH" ) return KoboTouch;
    else if ( d == "GLO" ) return KoboGlo; 
    else if ( d == "MINI" ) return KoboMini;
    else if ( d == "AURA" ) return KoboAura;
    else if ( d == "AURAHD" ) return KoboAuraHD;
    else if ( d == "AURAH2O" ) return KoboAuraH2O;
    else if ( d == "GLOHD" ) return KoboGloHD;
    else if ( d == "TOUCH2" ) return KoboTouch;
    else if ( d == "AURAONE" ) return KoboAuraOne;
    else if ( d == "AURA2_v1" ) return KoboAura2_v1;
    else if ( d == "AURA2_v2" ) return KoboAura2_v2;
    else if ( d == "AURAH2O2_v1" ) return KoboAuraH2O2_v1;
    else if ( d == "AURAH2O2_v2" ) return KoboAuraH2O2_v2;
    else if ( d == "CLARAHD" ) return KoboClaraHD;
    else if ( d == "FORMA" ) return KoboForma;
    else if ( d == "LIBRA" ) return KoboLibra;
    else if ( d == "NIA" ) return KoboNia;
    else if ( d == "LIBRA2" ) return KoboLibra2;
    return KoboOther;
}

inline bool automagic_sysfs(KoboDevice dev)
{
    return KoboNia == dev || KoboLibra2 == dev;
}


#endif

