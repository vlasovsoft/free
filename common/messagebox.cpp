#include "QtUtils.h"
#include "Platform.h"

#include "messagebox.h"

void messageBox( QWidget* parent, QMessageBox::Icon icon, const QString& title, const QString& text )
{
    MessageBox mBox(icon,title,text,MessageBox::Ok,parent);
    Platform::get()->lockOrientation();
    mBox.exec();
    Platform::get()->unlockOrientation();
    Platform::get()->scheduleEinkFullUpdate();
}

void messageBox( QWidget* parent,
                 QMessageBox::Icon icon,
                 const char* title,
                 const char* text )
{
    messageBox( parent, icon, QString::fromUtf8(title), QString::fromUtf8(text) );
}

QMessageBox::StandardButton
questionBox(QWidget *parent, const QString &title, const QString &text, QMessageBox::StandardButtons buttons, QMessageBox::StandardButton defaultButton)
{
    MessageBox mBox(QMessageBox::Question,title,text,buttons,parent);
    mBox.setDefaultButton(defaultButton);
    Platform::get()->lockOrientation();
    QMessageBox::StandardButton r = static_cast<QMessageBox::StandardButton>(mBox.exec());
    Platform::get()->unlockOrientation();
    Platform::get()->scheduleEinkFullUpdate();
    return r;
}
