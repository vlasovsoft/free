#include <QDir>
#include <QString>
#include <QStringList>

#include "Platform.h"
#include "Exception.h"
#include "Config.h"
#include "Factory.h"

#include "LanguageDescriptor.h"

LanguageDescriptor::LanguageDescriptor()
{}

static inline void fail(const QString& str) {
    throw Exception( true, QString("Invalid language description: %1").arg(str) );
}

LanguageDescriptor::LanguageDescriptor( const QString& str )
{
    QStringList list(str.split("|"));

    if ( list.size() < 4 ) {
        fail(str);
    }

    name   = list.at(0).trimmed();
    native = list.at(1).trimmed();
    trans  = list.at(2).trimmed();
    kbd    = list.at(3).trimmed();
}

QString LanguageDescriptor::getTransFileName() const
{
    return trans;
}

QString LanguageDescriptor::getKbdFileName() const
{
    return Platform::get()->getTranslationsPath() + QDir::separator() + kbd;
}
