#ifndef LONGTAP_H
#define LONGTAP_H

#include <QObject>
#include <QPoint>
#include <QTimer>

class LongTap : public QObject
{
    Q_OBJECT

    QPoint pt;
    QTimer timer;

public:
    LongTap(int interval, QObject *parent = 0);
    ~LongTap();

protected:
     bool eventFilter(QObject*, QEvent *event);

signals:
     void longTapSignal( QPoint pt );

private slots:
     void timerSlot();
};

#endif // LONGTAP_H
