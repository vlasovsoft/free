#ifndef GADGETMENU_H
#define GADGETMENU_H

#include <QObject>
#include <QWidget>

class QAction;
class GadgetMenuArea;

class GadgetMenu : public QWidget
{
    Q_OBJECT

    GadgetMenuArea* area;
    QString title;

    bool enabled;
    bool visible;

    int border;

public:
    GadgetMenu(QWidget* parent, const QString& t = QString());
    ~GadgetMenu();

    void addAction(QAction* a);
    void addMenu(GadgetMenu* m);
    void addSeparator();

    QString getTitle() const
    { return title; }
    void setTitle( const QString& val )
    { title = val; }

    bool isMenuEnabled() const;
    void setMenuEnabled( bool val );

    bool isMenuVisible() const;
    void setMenuVisible( bool val );

    bool eventFilter(QObject* obj, QEvent* event);

    QSize sizeHint() const;

protected:
    void resizeEvent(QResizeEvent *);
    void closeEvent(QCloseEvent *);

public slots:
    void setup();
    void exec();

private slots:
    void onActivateAction(QAction*);
    void onActivateSubmenu(GadgetMenu*);
};

#endif // GADGETMENU_H
