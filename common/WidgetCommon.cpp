#include <QMessageBox>
#include <QApplication>

#ifndef DESKTOP
 #include <QDesktopWidget>
#endif

#include "version.h"
#include "QtUtils.h"
#include "Log.h"
#include "Config.h"
#include "Platform.h"
#include "messagebox.h"

#include "WidgetCommon.h"

WidgetCommon::WidgetCommon()
#if defined(Q_WS_QWS)
    : VirtualKeyboardContainer(this)
    , WidgetEventFilter(this, this)
#endif
{}

WidgetCommon::~WidgetCommon()
{}

void WidgetCommon::show()
{
#if defined (DESKTOP) || defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
    QWidget::show();
#else
    QWidget::showFullScreen();
#endif
}
