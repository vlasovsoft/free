#ifndef COMPAT_H
#define COMPAT_H

#include <QSize>

#if QT_VERSION > 0x050000
 #include <QUrlQuery>
#else
 #include <QUrl>
#endif

class QString;
class QFontMetrics;
class QComboBox;

#if QT_VERSION > 0x050000
    #define PostDataQuery QUrlQuery
#else
    #define PostDataQuery QUrl
#endif

QByteArray encoded_query( const PostDataQuery& );
int font_metrics_width(const QFontMetrics&, const QString&);
void set_current_text(QComboBox&, const QString&);

#endif // COMPAT_H
