#if !defined H__SINGLETON
#define H__SINGLETON

template <typename T>
class Singleton
{
public:
    static T& instance()
    {
        static T inst;
        return inst;
    }
protected:
    Singleton() {}
    ~Singleton() {}
};

#endif
