
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QKeyEvent>

#include "QtUtils.h"

#include "FileOpenDialog.h"
#include "ui_FileOpenDialog.h"

FileOpenDialog::FileOpenDialog(QWidget* parent, const QString& title, const QString& path, const QString& exts, bool hidden)
    : Dialog(parent)
    , ui(new Ui::FileOpenDialog)
{
    ui->setupUi(this);
    init();

    setWindowTitle(title);

    ui->titleBar->titlePush(title);
    ui->titleBar->addSlot(this, SLOT(reject()));

    QFileSystemModel& model = ui->listView->getModel();
    if ( hidden ) {
        model.setFilter(model.filter() | QDir::Hidden);
    }
    model.setRootPath(path);
    model.setFilter( QDir::Dirs | QDir::AllDirs | QDir::Files | QDir::NoDot);
    model.setNameFilterDisables(false);
    if ( !exts.isEmpty() ) {
        QStringList filters(exts.split("|", QString::SkipEmptyParts));
        model.setNameFilters(filters);
        model.setNameFilterDisables(false);
    }

    ui->listView->setRootIndex(model.index(path));

    ui->lblPath->setText(path);
    ui->lblPath->setElideMode( Qt::ElideMiddle );

    QObject::connect(&ui->listView->getModel(), SIGNAL(rootPathChanged(QString)), ui->lblPath, SLOT(setText(QString)) );
}

FileOpenDialog::~FileOpenDialog()
{
    delete ui;
}

void FileOpenDialog::accept()
{
    QModelIndex index(ui->listView->selectionModel()->currentIndex());
    if ( index.isValid() ) {
        QFileInfo fInfo = ui->listView->getModel().fileInfo(index);
        if (fInfo.isFile()) {
            fileName = fInfo.absoluteFilePath();
            Dialog::accept();
        }
    }
}

void FileOpenDialog::onRootPathChanged(const QString &str)
{
    ui->lblPath->setText(str);
}
