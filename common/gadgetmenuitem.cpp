#include <QAction>
#include <QFontMetrics>

#include "gadgetmenu.h"
#include "gadgetmenuitem.h"

QString GadgetMenuItem::text() const
{
    switch( type )
    {
    case Action:
        return static_cast<QAction*>(ptr)->text();
    case Menu:
        return static_cast<GadgetMenu*>(ptr)->getTitle();
    default:
        break;
    }
    return QString();
}

bool GadgetMenuItem::isEnabled() const
{
    switch( type )
    {
    case Action:
        return static_cast<QAction*>(ptr)->isEnabled();
    case Menu:
        return static_cast<GadgetMenu*>(ptr)->isMenuEnabled();
    default:
        break;
    }
    return true;
}

bool GadgetMenuItem::isVisible() const
{
    switch( type )
    {
    case Action:
        return static_cast<QAction*>(ptr)->isVisible();
    case Menu:
        return static_cast<GadgetMenu*>(ptr)->isMenuVisible();
    default:
        break;
    }
    return true;
}

GadgetMenuItem::GadgetMenuItem()
    : type(Separator)
    , ptr(NULL)
{
}

GadgetMenuItem::GadgetMenuItem(QAction *a)
    : type(Action)
    , ptr(a)
{
}

GadgetMenuItem::GadgetMenuItem(GadgetMenu *m)
    : type(Menu)
    , ptr(m)
{
}
