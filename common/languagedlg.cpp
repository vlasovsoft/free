#include "Factory.h"
#include "Singleton.h"
#include "Config.h"
#include "LangUtils.h"
#include "LanguageDescriptor.h"
#include "languagedlg.h"
#include "ui_languagedlg.h"

LanguageDlg::LanguageDlg(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::LanguageDlg)
{
    ui->setupUi(this);
    init();

    const Factory<LanguageDescriptor>& ldf = Singleton<Factory<LanguageDescriptor> >::instance();
    for ( int i=0; i<ldf.size(); ++i )
    {
        ui->lwLanguage->addItem(0==i? QString("System"):ldf[i].native);
    }
    ui->lwLanguage->setCurrentRow(g_pConfig->readInt("lang", 0));

    QObject::connect(ui->lwLanguage, SIGNAL(itemActivated(QListWidgetItem*)), this, SLOT(itemActivated(QListWidgetItem*)));
}

LanguageDlg::~LanguageDlg()
{
    delete ui;
}

void LanguageDlg::accept()
{
    Dialog::accept();
    int lang = ui->lwLanguage->currentRow();
    g_pConfig->writeInt("lang", lang);
    loadTranslations();
}

void LanguageDlg::itemActivated(QListWidgetItem *item)
{
    if ( item != NULL )
        accept();
}
