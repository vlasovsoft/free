#ifndef GADGETTITLEBAR_H
#define GADGETTITLEBAR_H

#include <QStack>
#include <QWidget>

namespace Ui {
class GadgetTitleBar;
}

class GadgetTitleBar : public QWidget
{
    Q_OBJECT

public:
    explicit GadgetTitleBar(QWidget *parent = NULL);
    ~GadgetTitleBar();

    void titlePush(const QString& title);
    void titlePop();

    void addSlot(QObject* target, const char* slot);

protected:
    void paintEvent(QPaintEvent* event);

private:
    void titleSync();

private:
    Ui::GadgetTitleBar *ui;
    QStack<QString> stack;
};

#endif // GADGETTITLEBAR_H
