#ifndef H__EXCEPTION
#define H__EXCEPTION

#include <QString>

#include "utils.h"

class Exception
{
    bool isCritical_;
    QString message_;

public:
    Exception(bool isCritical, const QString& message);
    virtual ~Exception() throw() {}
    const QString& what() const {
        return message_;
    }
    bool isCritical() const {
        return isCritical_;
    }
};

#endif
