#ifndef FILE_OPEN_DIALOG_H
#define FILE_OPEN_DIALOG_H

#include <QFileSystemModel>
#include <QTreeView>
#include <QFileIconProvider>

#include "Dialog.h"

class QKeyEvent;

namespace Ui {
class FileOpenDialog;
}

class FileOpenDialog: public Dialog
{
    Q_OBJECT

    Ui::FileOpenDialog* ui;

    QString fileName;
    QString ext;

public:
    FileOpenDialog(QWidget* parent, const QString& title, const QString& path, const QString& exts, bool hidden=false);
    ~FileOpenDialog();

    const QString& getFileName() const
    { return fileName; }

private slots:
    void accept();
    void onRootPathChanged(const QString& str);
};

#endif // FILE_OPEN_DIALOG_H
