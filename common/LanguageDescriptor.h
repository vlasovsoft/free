#ifndef H__LANGUAGE_DESCRIPTOR
#define H__LANGUAGE_DESCRIPTOR

#include <QVector>
#include <QString>

class LanguageDescriptor
{
public:
    QString name;
    QString native;
    QString trans;
    QString kbd;

public:
    LanguageDescriptor();
    LanguageDescriptor( const QString& str );

    QString getTransFileName() const;
    QString getKbdFileName() const;
};

#endif

