#include <QPainter>
#include <QStyle>
#include <QStyleOption>

#include "gadgettitlebar.h"
#include "ui_gadgettitlebar.h"

GadgetTitleBar::GadgetTitleBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GadgetTitleBar)
{
    ui->setupUi(this);
}

GadgetTitleBar::~GadgetTitleBar()
{
    delete ui;
}

void GadgetTitleBar::titlePush(const QString &title)
{
    stack.push(title);
    titleSync();
}

void GadgetTitleBar::titlePop()
{
    if ( !stack.isEmpty() ) {
        stack.pop();
        titleSync();
    }
}

void GadgetTitleBar::addSlot(QObject *target, const char* slot)
{
    QObject::connect(ui->btnBack, SIGNAL(clicked()), target, slot);
}

void GadgetTitleBar::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}

void GadgetTitleBar::titleSync()
{
    ui->lblTitle->setText(stack.isEmpty()? "" : stack.top());
}
