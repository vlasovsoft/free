#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QMessageBox>

class MessageBox : public QMessageBox
{
public:
    MessageBox(QWidget* parent = 0)
        : QMessageBox(parent)
    {
#if defined(Q_OS_IOS) || defined(Q_OS_ANDROID)
        setWindowFlags(Qt::Tool);
        resize(sizeHint());
#endif    
    }

    MessageBox(Icon icon, const QString& title, const QString & text, StandardButtons buttons = NoButton, QWidget * parent = 0, Qt::WindowFlags f = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint)
        : QMessageBox( icon, title, text, buttons, parent, f )
    {
#if defined(Q_OS_IOS) || defined(Q_OS_ANDROID)
        setWindowFlags(Qt::Tool);
        resize(sizeHint());
#endif    
    }
};

void messageBox( QWidget* parent, QMessageBox::Icon icon, const QString& title, const QString& text );
QMessageBox::StandardButton questionBox(QWidget *parent, const QString &title, const QString &text, QMessageBox::StandardButtons buttons, QMessageBox::StandardButton defaultButton);

#endif // MESSAGEBOX_H
