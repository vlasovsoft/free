#include <fstream>
#include <string>
#include <QMutex>

#include "utils.h"

using std::ofstream;

class Log
{
    std::ofstream f;
    int level;
    std::string name;
    QMutex mutex;

public:
    enum MsgType
    {
        MT_I,  // Informational message
        MT_W,  // Warning message
        MT_E,  // Non-critical error message
        MT_F   // fatal error message
    };

public:
    Log( const char* nm, int lvl );

    void write( int lvl, MsgType type, const char* format, ... ) FORMAT(4,5);
    void write( int lvl, MsgType type, const QString& str );

    int getLevel() const 
    { return level; }

    void setLevel( int lvl )
    { level = lvl; }
};

#ifdef CPP__LOG
    Log* g_pLog;
#else
    extern Log* g_pLog;
#endif
