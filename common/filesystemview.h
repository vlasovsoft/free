#ifndef FILESYSTEMVIEW_H_
#define FILESYSTEMVIEW_H_

#include <QListView>
#include <QFileIconProvider>
#include <QFileSystemModel>

class FileSystemView: public QListView, public QFileIconProvider
{
    Q_OBJECT

    QFileSystemModel fsModel;

    QIcon iconFile;
    QIcon iconFolder;

public:
    FileSystemView(QWidget* parent);

    QFileSystemModel& getModel() { return fsModel; }

    QIcon icon ( QFileIconProvider::IconType type ) const;
    QIcon icon ( const QFileInfo & info ) const;

private slots:
    void select(const QModelIndex & index);
};

#endif /* FILESYSTEMVIEW_H_ */
