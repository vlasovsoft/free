#ifndef GESTURESCONTROLLER_H
#define GESTURESCONTROLLER_H

#include <QTimer>
#include <QPoint>
#include <QSize>
#include <QElapsedTimer>

class QAbstractScrollArea;

#include "statemachine.h"

class GesturesController : public QTimer, protected StateMachine, protected StateMachineCallback
{
    Q_OBJECT

    QPoint ptF;
    QPoint ptT;

    bool skipPress;
    bool skipRelease;

    int rotation;
    int minDist;
    int minDuration;

    QSize weights;

    QWidget* w;
    QWidget* wp;
    QAbstractScrollArea* pScrollArea;

    QElapsedTimer lastEvent;

public:
    enum GestureType
    {
        gtSwipeLR,
        gtSwipeRL,
        gtSwipeTB,
        gtSwipeBT,
        gtTapShort,
        gtTapLong
    };

    enum State
    {
        sWait  , // Initial state
        sBP    , // Button pressed
        sBPOk  , // Button pressed ok
        sBLP   , // Button long press
        sSwipe   // Swipe
    };

    enum Event
    {
        eBP    , // Button press
        eTimer , // Timer
        eMove  , // Mouse move
        eBR      // Button release
    };

    GesturesController(QWidget* widget = NULL);

    void setWidget(QWidget* widget);
    void setScrollArea(QAbstractScrollArea* pArea)
    { pScrollArea = pArea; }
    
    virtual bool eventFilter ( QObject* pObj, QEvent* e );

    virtual void onStateChanged( const StateMachine::Transition& t );

    int getRotation() const
    { return rotation; }

    void setRotation( int val )
    { rotation = val; }

    int getXWeight() const
    { return 1 == rotation || 3 == rotation? weights.height() : weights.width(); }

    int getYWeight() const
    { return 1 == rotation || 3 == rotation? weights.width() : weights.height(); }

    void setWeights( const QSize& val )
    { weights = val; }

    QPoint rotatePoint( const QPoint& src ) const;

private slots:
    void timer();

signals:
    void sigGesture(QPoint,GesturesController::GestureType);

private:
    bool scroll_left();
    bool scroll_right();
    bool scroll_up();
    bool scroll_down();
};

#endif // GESTURESCONTROLLER_H
