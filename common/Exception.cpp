#include "Exception.h"

Exception::Exception(bool isCritical, const QString& message)
    : isCritical_(isCritical)
    , message_(message)
{
}
