#ifndef H__UTILS
#define H__UTILS

#define PSEP '/'
#define PSEPS "/"
#define SIZE(arr) (sizeof(arr)/sizeof(arr[0]))
#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
#define FORMAT(x,y) __attribute__((format(printf, x, y)));
#else
#define FORMAT(x,y)
#endif

template <typename T> T limit(T val, T min, T max) {
    if ( val < min )
        val = min;
    if ( val > max )
        val = max;
    return val;
}

void random_init();
int  random_get(int min, int max);

#endif

