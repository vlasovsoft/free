#ifndef FACTORY_H
#define FACTORY_H

#include <QVector>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDateTime>

#include "Platform.h"
#include "Exception.h"

template <typename T>
class Factory: public QVector<T>
{
public:
    void init( const QString& fn, bool raise = true )
    {
        QFileInfo fi(fn);
        QFile file( fi.isAbsolute() ? fn : Platform::get()->getRootPath() + QDir::separator() + fn );
        if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            QTextStream in(&file);
            in.setCodec("UTF-8");
            while(!in.atEnd())
            {
                QString line = in.readLine();
                this->push_back( T(line) );
            }
        }

        if ( this->empty() && raise )
            throw Exception(true, QString("Error reading file: %1").arg(fi.fileName()));
    }
};

#endif // FACTORY_H
