#include <ctime>
#include <cstdlib>

#include "utils.h"

void random_init() {
    srand(time(NULL));
}

int random_get(int min, int max) {
    if ( max < min ) {
        int tmp = min;
        min = max;
        max = tmp;
    }
    return min + rand() % (max - min + 1);
}
