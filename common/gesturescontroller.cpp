#include <QWidget>
#include <QMouseEvent>
#include <QCoreApplication>
#include <QAbstractScrollArea>
#include <QScrollBar>
#include <QDebug>

#include "QtUtils.h"

#include "gesturescontroller.h"

#define SWIPE_MIN_DISTANCE 10   // 10 mm
#define SHORT_TAP_TIME     150
#define LONG_TAP_TIME      500

GesturesController::GesturesController(QWidget* widget)
    : StateMachine(0, this)
    , skipPress(false)
    , skipRelease(false)
    , rotation(0)
    , minDist(100)
    , weights(2,1)
    , w(NULL)
    , wp(NULL)
    , pScrollArea(NULL)
{
    int dmm = qgetenv("SWIPE_MIN_DISTANCE").toInt();
    if ( dmm < 5 || dmm > 100 )
        dmm = SWIPE_MIN_DISTANCE;
    minDist = mm_to_px(dmm);
    minDuration = qgetenv("GESTURE_MIN_DURATION").toInt();
    transitions.push_back(Transition( sWait  , sBP    , eBP ));
    transitions.push_back(Transition( sBP    , sWait  , eBR ));
    transitions.push_back(Transition( sBP    , sBPOk  , eTimer ));
    transitions.push_back(Transition( sBP    , sSwipe , eMove ));
    transitions.push_back(Transition( sBPOk  , sWait  , eBR ));
    transitions.push_back(Transition( sBPOk  , sBLP   , eTimer ));
    transitions.push_back(Transition( sBLP   , sWait  , eBR ));
    transitions.push_back(Transition( sBLP   , sWait  , eBR ));
    transitions.push_back(Transition( sSwipe , sWait  , eBR ));

    setSingleShot(true);
    QObject::connect( this, SIGNAL(timeout()), this, SLOT(timer()) );

    lastEvent.start();

    setWidget(widget);
}

void GesturesController::setWidget(QWidget* widget)
{
    if ( NULL == w && widget != NULL )
    {
        QAbstractScrollArea* scrollArea = qobject_cast<QAbstractScrollArea*>(widget);
        if (scrollArea)
        {
            w = widget;
            wp = scrollArea->viewport();
            wp->installEventFilter(this);
            pScrollArea = scrollArea;
        }
        else
        {
            w = wp = widget;
            wp->installEventFilter(this);
        }
    }
}

bool GesturesController::eventFilter(QObject *pObj, QEvent *e)
{
    Q_UNUSED(pObj)

    int t = e->type();
    if (    QEvent::MouseButtonPress    == t
         || QEvent::MouseButtonRelease  == t
         || QEvent::MouseMove           == t
         || QEvent::MouseButtonDblClick == t )
    {
        QMouseEvent* me = static_cast<QMouseEvent*>(e);
        switch ( t )
        {
        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonDblClick:
            if ( skipPress )
            {
                skipPress = false;
                return false;
            }
            else
            {
                ptF = me->pos();
                StateMachine::event( eBP );
            }
            break;
        case QEvent::MouseButtonRelease:
            if ( skipRelease )
            {
                skipRelease = false;
                return false;
            }
            else
            {
                ptT = me->pos();
                StateMachine::event( eBR );
            }
            break;
        case QEvent::MouseMove:
            if ( sBP == state_ )
            {
                int dx = abs(ptF.x() - me->pos().x());
                int dy = abs(ptF.y() - me->pos().y());
                if ( dx > minDist || dy > minDist )
                    StateMachine::event( eMove );
                else
                    QTimer::start(SHORT_TAP_TIME);
            }
        }
        return true;
    }
    return false;
}

void GesturesController::onStateChanged(const StateMachine::Transition& t)
{
#if 0
    qDebug("onStateChanged: %d -> %d -> %d", t.oldState, t.event, t.newState);
#endif
    switch ( t.newState )
    {
    case sWait:
        QTimer::stop();
        if ( sBP == t.oldState )
        {
            skipPress = true;
            QCoreApplication::postEvent( wp, new QMouseEvent( QEvent::MouseButtonPress, ptF, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier  ));
        }
        if ( sSwipe == t.oldState )
        {
           int dx = ptT.x() - ptF.x();
           int dy = ptT.y() - ptF.y();
           if ( abs(dx) > minDist || abs(dy) > minDist )
           {
               if (    getXWeight()*abs(dx) > getYWeight()*abs(dy)
                    && lastEvent.hasExpired(minDuration) )
               {
                   if ( pScrollArea )
                   {
                       if ( dx > 0 ) // LR
                       {
                           if ( !scroll_left() )
                               scroll_up();
                       }
                       else
                       {
                           if ( !scroll_right() )
                               scroll_down();
                       }
                   }
                   else
                   {
                       emit sigGesture( ptF, dx>0 ? gtSwipeLR:gtSwipeRL );
                       lastEvent.restart();                   
                   }
               }
               else
               if (    getYWeight()*abs(dy) > getXWeight()*abs(dx)
                    && lastEvent.hasExpired(minDuration) )
               {
                   if ( pScrollArea )
                   {
                       if ( dy > 0 ) // TB
                       {
                           scroll_up();
                       }
                       else
                       {
                           scroll_down();
                       }
                   }
                   else
                   {
                       emit sigGesture( ptF, dy>0 ? gtSwipeTB:gtSwipeBT );
                       lastEvent.restart();                   
                   }
               }
           }
        } 
        else
        if ( t.event == eBR )
        {
            skipRelease = true;
            QCoreApplication::postEvent( wp, new QMouseEvent( QEvent::MouseButtonRelease, ptT, Qt::LeftButton, Qt::NoButton, Qt::NoModifier  ) );
            if ( lastEvent.hasExpired(minDuration) && ( sBP == t.oldState || sBPOk == t.oldState ) )
            {
                emit sigGesture( ptF, gtTapShort );
                lastEvent.restart();                   
            }
        }
        break;
    case sBP:
        QTimer::start(SHORT_TAP_TIME);
        break;
    case sBPOk:
        skipPress = true;
        QCoreApplication::postEvent( wp, new QMouseEvent( QEvent::MouseButtonPress, ptF, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier  ) );
        QTimer::start(LONG_TAP_TIME);
        break;
    case sBLP:
        StateMachine::event(eBR);
        if ( lastEvent.hasExpired(minDuration) )
        {
            emit sigGesture( ptF, gtTapLong );
            lastEvent.restart();                   
        }
        QTimer::stop();
        break;
    case sSwipe:
        QTimer::stop();
        break;
    default:
        break;
    }
}

void GesturesController::timer()
{
    StateMachine::event(eTimer);
}

bool GesturesController::scroll_left()
{
    if ( Qt::ScrollBarAlwaysOff == pScrollArea->horizontalScrollBarPolicy() )
        return false;

    QScrollBar* pSb = pScrollArea->horizontalScrollBar();
    int val = pSb->value();
    val -= pSb->pageStep();
    if ( val < pSb->minimum() ) val = pSb->minimum();
    pSb->setValue( val);
    return true;
}

bool GesturesController::scroll_right()
{
    if ( Qt::ScrollBarAlwaysOff == pScrollArea->horizontalScrollBarPolicy() )
        return false;

    QScrollBar* pSb = pScrollArea->horizontalScrollBar();
    int val = pSb->value();
    val += pSb->pageStep();
    if ( val > pSb->maximum() ) val = pSb->maximum();
    pSb->setValue( val);
    return true;
}

bool GesturesController::scroll_up()
{
    if ( Qt::ScrollBarAlwaysOff == pScrollArea->verticalScrollBarPolicy() )
        return false;

    QScrollBar* pSb = pScrollArea->verticalScrollBar();
    int val = pSb->value();
    val -= pSb->pageStep();
    if ( val < pSb->minimum() ) val = pSb->minimum();
    pSb->setValue( val);
    return true;
}

bool GesturesController::scroll_down()
{
    if ( Qt::ScrollBarAlwaysOff == pScrollArea->verticalScrollBarPolicy() )
        return false;

    QScrollBar* pSb = pScrollArea->verticalScrollBar();
    int val = pSb->value();
    val += pSb->pageStep();
    if ( val > pSb->maximum() ) val = pSb->maximum();
    pSb->setValue( val);
    return true;
}
