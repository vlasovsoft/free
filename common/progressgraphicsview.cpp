#include "QtUtils.h"

#include "progressgraphicsview.h"

ProgressGraphicsView::ProgressGraphicsView( QWidget* parent )
    : GraphicsView(parent)
{
    setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
    int sizePx = mm_to_px(30);
    setSizeHint(QSize(sizePx, sizePx));
}
