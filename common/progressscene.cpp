#include <QGraphicsRectItem>

#include "progressscene.h"

static char anim[16][8] = {
    {0,0,0,0,0,0,0,0},
    {1,0,0,0,0,0,0,0},
    {1,1,0,0,0,0,0,0},
    {1,1,1,0,0,0,0,0},
    {1,1,1,1,0,0,0,0},
    {1,1,1,1,1,0,0,0},
    {1,1,1,1,1,1,0,0},
    {1,1,1,1,1,1,1,0},
    {1,1,1,1,1,1,1,1},
    {0,1,1,1,1,1,1,1},
    {0,0,1,1,1,1,1,1},
    {0,0,0,1,1,1,1,1},
    {0,0,0,0,1,1,1,1},
    {0,0,0,0,0,1,1,1},
    {0,0,0,0,0,0,1,1},
    {0,0,0,0,0,0,0,1}
};

ProgressScene::ProgressScene(QObject* parent)
    : QGraphicsScene(parent)
    , stage(0)
{
    memset(pItem, 0, sizeof(pItem));
    setSceneRect(QRectF(-41,-41,82,82));
    addItem(pItem[0] = new QGraphicsRectItem(QRectF(-40,20,20,20), NULL));
    addItem(pItem[1] = new QGraphicsRectItem(QRectF(-10,20,20,20), NULL));
    addItem(pItem[2] = new QGraphicsRectItem(QRectF(20,20,20,20), NULL));
    addItem(pItem[3] = new QGraphicsRectItem(QRectF(20,-10,20,20), NULL));
    addItem(pItem[4] = new QGraphicsRectItem(QRectF(20,-40,20,20), NULL));
    addItem(pItem[5] = new QGraphicsRectItem(QRectF(-10,-40,20,20), NULL));
    addItem(pItem[6] = new QGraphicsRectItem(QRectF(-40,-40,20,20), NULL));
    addItem(pItem[7] = new QGraphicsRectItem(QRectF(-40,-10,20,20), NULL));
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    timer.setInterval(500);
    timer.start();
}

void ProgressScene::onTimer()
{
    stage = (stage + 1) % 16;
    for ( int i=0; i<8; i++ )
    {
        pItem[i]->setBrush( anim[stage][i] != 0 ? QBrush(Qt::black) : QBrush(Qt::white) );
    }
}
