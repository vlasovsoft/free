for f in *.svg
do
  bn="$(basename -- $f .svg)"
  echo "Processing file - $bn"
  # light / enabled
  sed 's/{color}/#000000/g' $f > /tmp/$f
  rsvg-convert -w 48 -h 48 /tmp/$f > ../light/enabled/$bn.png
  # light / disabled
  sed 's/{color}/#666666/g' $f > /tmp/$f
  rsvg-convert -w 48 -h 48 /tmp/$f > ../light/disabled/$bn.png
  # dark / enabled
  sed 's/{color}/#AAAAAA/g' $f > /tmp/$f
  rsvg-convert -w 48 -h 48 /tmp/$f > ../dark/enabled/$bn.png
  # dark / disabled
  sed 's/{color}/#444444/g' $f > /tmp/$f
  rsvg-convert -w 48 -h 48 /tmp/$f > ../dark/disabled/$bn.png
done

