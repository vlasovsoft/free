#ifndef WIDGET_COMMON_H_
#define WIDGET_COMMON_H_

#include <QWidget>
#include <QSettings>

#if defined(Q_WS_QWS)
#include "WidgetEventFilter.h"
#include "VirtualKeyboardContainer.h"
#endif

class WidgetCommon : public QWidget
#if defined(Q_WS_QWS)
, protected VirtualKeyboardContainer
, private WidgetEventFilter
#endif
{
    Q_OBJECT

public:
    WidgetCommon();
    ~WidgetCommon();

protected:
#if defined(Q_WS_QWS)
    virtual bool eventFilter(QObject *obj, QEvent *event)
    { return WidgetEventFilter::filter(obj, event); }
#endif

public slots:
    void show();
};

#endif /* WIDGET_COMMON_H_ */
