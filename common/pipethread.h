#ifndef PIPETHREAD_H
#define PIPETHREAD_H

#include <QThread>

class PipeThread : public QThread
{
    Q_OBJECT

    QString pipe;

public:
    PipeThread( const QString& p );
    ~PipeThread();

signals:
    void sigPipeMsg( const QString& msg );

protected:
    virtual void run();
};

#endif // PIPETHREAD_H

