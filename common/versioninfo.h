#ifndef VERSIONINFO_H
#define VERSIONINFO_H

#include <QString>

#include "version.h"

class VersionInfo
{
    int major_;
    int minor_;
    int build_;

public:
    VersionInfo();
    VersionInfo(const QString& val);

    QString toStr() const;

    bool operator < ( const VersionInfo& v ) const;
    bool operator > ( const VersionInfo& v ) const;
    bool operator == ( const VersionInfo& v ) const;

private:
    void parse(const QString& val);
};

#endif // VERSIONINFO_H
