#include <QStringList>

#include "utils.h"
#include "versioninfo.h"

VersionInfo::VersionInfo()
    : major_(0)
    , minor_(0)
    , build_(0)
{
    parse(STR(VLASOVSOFT_VERSION));
}

VersionInfo::VersionInfo(const QString& val)
    : major_(0)
    , minor_(0)
    , build_(0)
{
    parse(val);
}

QString VersionInfo::toStr() const
{
    return QString("%1.%2.%3").arg(major_).arg(minor_).arg(build_);
}

bool VersionInfo::operator < (const VersionInfo &v) const
{
    return    ( major_ < v.major_ )
           || ( major_ == v.major_ && minor_ < v.minor_ )
            || ( major_ == v.major_ && minor_ == v.minor_ && build_ < v.build_ );
}

bool VersionInfo::operator > (const VersionInfo &v) const
{
    return    ( major_ > v.major_ )
           || ( major_ == v.major_ && minor_ > v.minor_ )
           || ( major_ == v.major_ && minor_ == v.minor_ && build_ > v.build_ );
}

bool VersionInfo::operator == (const VersionInfo &v) const
{
    return major_ == v.major_ && minor_ == v.minor_ && build_ == v.build_;
}

void VersionInfo::parse(const QString &val)
{
    QStringList list = val.split('.');
    if ( list.size() > 0 ) {
        major_ = list.at(0).toInt();
        if ( list.size() > 1 ) {
            minor_ = list.at(1).toInt();
            if ( list.size() > 2 ) {
                build_ = list.at(2).toInt();
            }
        }
    }
}
