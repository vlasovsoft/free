TEMPLATE  = lib
CONFIG   += lib

kobo|kindle|pb|obreey|qvfb {
    CONFIG += dll plugin
}
else {
    CONFIG += staticlib
}

include($$PWD/common.pri)
include($$PWD/i18n.pri)

SOURCES += \
    versioninfo.cpp \
    md5.cpp \
    json.cpp \
    utils.cpp \
    Log.cpp \
    Exception.cpp \
    QtUtils.cpp \
    compat.cpp \
    Dialog.cpp \
    Application.cpp \
    EnterTextLineDialog.cpp \
    Config.cpp \
    Platform.cpp \
    statemachine.cpp \
    LangUtils.cpp \
    LanguageDescriptor.cpp \
    languagedlg.cpp \
    graphicsview.cpp \
    messagebox.cpp \
    common_i18n.cpp \
    WidgetCommon.cpp \
    elidedlabel.cpp
desktop {
    SOURCES += singleappguard.cpp
}
else {
    SOURCES += FileOpenDialog.cpp \
        FileSaveDialog.cpp \
        DirSelectDialog.cpp \
        StyleEbook.cpp \
        menufilter.cpp \
        gadgetmenu.cpp \
        gadgetmenuarea.cpp \
        gadgetmenuitem.cpp
    eink|qvfb|simple {
        SOURCES += pipethread.cpp
    }
    eink|qvfb {
        SOURCES += svgwidget.cpp \
            wifidialog.cpp \
            progressscene.cpp \
            VirtualKeyboard.cpp \
            WidgetEventFilter.cpp \
            DictionaryWidget.cpp \
            VirtualKeyboardContainer.cpp \
            gesturescontroller.cpp \
            rotationdlg.cpp \
            progressgraphicsview.cpp \
            gadgettitlebar.cpp \
            filesystemview.cpp
    }
    kindle|kobo {
        SOURCES += mxcfb.cpp
    }
}

HEADERS += \
    versioninfo.h \
    md5.h \
    json.h \
    utils.h \
    Log.h \
    Exception.h \
    QtUtils.h \
    compat.h \
    Dialog.h \
    version.h \
    Application.h \
    EnterTextLineDialog.h \
    Config.h \
    Platform.h \
    statemachine.h \
    LangUtils.h \
    LanguageDescriptor.h \
    languagedlg.h \
    graphicsview.h \
    messagebox.h \
    WidgetCommon.h \
    elidedlabel.h \
    Factory.h \
    Singleton.h
desktop {
    HEADERS += singleappguard.h
}
else {
    HEADERS += FileOpenDialog.h \
        FileSaveDialog.h \
        DirSelectDialog.h \
        StyleEbook.h \
        menufilter.h \
        gadgetmenu.h \
        gadgetmenuarea.h \
        gadgetmenuitem.h
    eink|qvfb|simple {
        HEADERS += pipethread.h
    }
    eink|qvfb {
        HEADERS += svgwidget.h \
            wifidialog.h \
            progressscene.h \
            VirtualKeyboard.h \
            WidgetEventFilter.h \
            DictionaryWidget.h \
            VirtualKeyboardContainer.h \
            gesturescontroller.h \
            rotationdlg.h \
            progressgraphicsview.h \
            gadgettitlebar.h \
            filesystemview.h
    }
    kindle|kobo {
        HEADERS += mxcfb.h
        kindle {
            HEADERS += mxcfb_kindle.h
        }
        kobo {
            HEADERS += mxcfb_kobo.h
        }
    }
}

FORMS += EnterTextLineDialog.ui \
    languagedlg.ui
!desktop {
    FORMS += FileOpenDialog.ui \
        FileSaveDialog.ui \
        DirSelectDialog.ui
    eink|qvfb {
        FORMS += wifidialog.ui \
            rotationdlg.ui \
            gadgettitlebar.ui
    }
}

ios {
    QT += gui-private
    SOURCES += platform/IOSPlatform.cpp
    HEADERS += platform/IOSPlatform.h
}

RESOURCES += resources/icons/icons.qrc

