<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="it_IT">
<context>
    <name>DirSelectDialog</name>
    <message>
        <location filename="../DirSelectDialog.ui" line="14"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
</context>
<context>
    <name>FileOpenDialog</name>
    <message>
        <location filename="../FileOpenDialog.ui" line="14"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../FileSaveDialog.ui" line="14"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>File exists. Overwrite?</source>
        <translation type="vanished">Il file esiste già. Sovrascrivi?</translation>
    </message>
</context>
<context>
    <name>QAndroidPlatformTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="46"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="47"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../common_i18n.cpp" line="4"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="5"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="6"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="7"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="8"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="9"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="10"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="11"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="12"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="13"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="14"/>
        <source>Reset</source>
        <translation>Ripristina</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="15"/>
        <source>Discard</source>
        <translation>Scarta</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="16"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sì</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="17"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="18"/>
        <source>Abort</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="19"/>
        <source>Retry</source>
        <translation>Riprova</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="20"/>
        <source>Ignore</source>
        <translation>Ignora</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../common_i18n.cpp" line="22"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="26"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="27"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="28"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="29"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../common_i18n.cpp" line="24"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="31"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="32"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="33"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="34"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sì</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="35"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="36"/>
        <source>Abort</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="37"/>
        <source>Retry</source>
        <translation>Riprova</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="38"/>
        <source>Ignore</source>
        <translation>Ignora</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="39"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="40"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="41"/>
        <source>Discard</source>
        <translation>Scarta</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="42"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="43"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="44"/>
        <source>Reset</source>
        <translation>Ripristina</translation>
    </message>
</context>
<context>
    <name>QtUtils</name>
    <message>
        <location filename="../QtUtils.cpp" line="155"/>
        <source>File exists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../QtUtils.cpp" line="156"/>
        <source>File &quot;%1&quot; already exists. Do you want to overwrite it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../QtUtils.cpp" line="449"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../QtUtils.cpp" line="450"/>
        <source>No Internet connection!
Retry?</source>
        <translation>Nessuna connessione internet.
Riprova?</translation>
    </message>
</context>
<context>
    <name>RotationDlg</name>
    <message>
        <location filename="../rotationdlg.ui" line="14"/>
        <source>Screen rotation</source>
        <translation>Rotazione schermo</translation>
    </message>
</context>
<context>
    <name>WifiDialog</name>
    <message>
        <location filename="../wifidialog.ui" line="40"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../Application.cpp" line="108"/>
        <location filename="../Application.cpp" line="115"/>
        <source>Error:</source>
        <translation>Errore:</translation>
    </message>
</context>
</TS>