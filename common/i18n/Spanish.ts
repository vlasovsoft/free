<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="es">
<context>
    <name>DirSelectDialog</name>
    <message>
        <location filename="../DirSelectDialog.ui" line="+14"/>
        <source>Open</source>
        <translation type="unfinished">Abrir</translation>
    </message>
</context>
<context>
    <name>FileOpenDialog</name>
    <message>
        <location filename="../FileOpenDialog.ui" line="+14"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../FileSaveDialog.ui" line="+14"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <source>File exists. Overwrite?</source>
        <translation type="vanished">Archivo existe. ¿Sobrescribir?</translation>
    </message>
</context>
<context>
    <name>QAndroidPlatformTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="+46"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location line="-43"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset</source>
        <translation>Reinicializar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Discard</source>
        <translation>Descartar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Abort</source>
        <translation>Interrumpir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Retry</source>
        <translation>Reintentar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message>
        <location line="-15"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <source>OK</source>
        <translation type="vanished">Aceptar</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <location line="+4"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location line="-5"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <location line="+7"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Abort</source>
        <translation>Interrumpir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Retry</source>
        <translation>Reintentar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Discard</source>
        <translation>Descartar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset</source>
        <translation>Reinicializar</translation>
    </message>
</context>
<context>
    <name>QtUtils</name>
    <message>
        <location filename="../QtUtils.cpp" line="+155"/>
        <source>File exists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>File &quot;%1&quot; already exists. Do you want to overwrite it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+293"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No Internet connection!
Retry?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RotationDlg</name>
    <message>
        <location filename="../rotationdlg.ui" line="+14"/>
        <source>Screen rotation</source>
        <translation>Rotar pantalla</translation>
    </message>
</context>
<context>
    <name>WidgetCommonLicensed</name>
    <message>
        <source>The program is not registered.
Please register it at goo.gl/QArXWh</source>
        <translation type="vanished">El programa no está registrado.
Regístrese en goo.gl/QArXWh</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Licencia</translation>
    </message>
    <message>
        <source>The program is not registered.
Please register it at en.vlasovsoft.net</source>
        <translation type="vanished">El programa no está registrado.
Regístrese en en.vlasovsoft.net</translation>
    </message>
    <message>
        <source>Your time is up.
Please register this program or restart it to continue evaluation.</source>
        <translation type="vanished">Se acabó el tiempo.
Registre el programa o ejecútelo de nuevo para continuar evaluándolo.</translation>
    </message>
</context>
<context>
    <name>WifiDialog</name>
    <message>
        <location filename="../wifidialog.ui" line="+40"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../Application.cpp" line="+108"/>
        <location line="+7"/>
        <source>Error:</source>
        <translation>Error:</translation>
    </message>
</context>
</TS>