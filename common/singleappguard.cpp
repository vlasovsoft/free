#include <QCryptographicHash>

#include "singleappguard.h"

namespace
{
    QString generateKeyHash( const QString& key, const QString& salt )
    {
        QByteArray data;
        data.append( key.toUtf8() );
        data.append( salt.toUtf8() );
        data = QCryptographicHash::hash( data, QCryptographicHash::Sha1 ).toHex();
        return data;
    }
}

SingleAppGuard::SingleAppGuard( const QString& key )
    : key( key )
    , memLockKey( generateKeyHash( key, "_memLockKey" ) )
    , sharedMemKey( generateKeyHash( key, "_sharedMemKey" ) )
    , sharedMem( sharedMemKey )
    , memLock( memLockKey, 1 )
{
    QSharedMemory fix( sharedMemKey ); // Fix for *nix: http://habrahabr.ru/post/173281/
    fix.attach();
}

bool SingleAppGuard::tryToRun()
{
    memLock.acquire();
    const bool result = sharedMem.create( sizeof( quint64 ) );
    memLock.release();
    return result;
}

