#ifndef H__CONFIG
#define H__CONFIG

#include <string>
#include <limits>

#include <QString>

class QSettings;

class Config
{
public:
    QSettings* ini;

public:
    Config( const QString& name );
    ~Config();

public:
    void sync();
    int readInt( const char* param, int def = 0, int min = std::numeric_limits<int>::min(), int max = std::numeric_limits<int>::max() ) const;
    int readInt( const QString& param, int def = 0, int min = std::numeric_limits<int>::min(), int max = std::numeric_limits<int>::max() ) const;
    bool readBool( const QString& param, bool def = 0 ) const;
    std::string readString( const char* param, const char* def = "" ) const;
    QString readQString( const char* param, const QString& def = QString() ) const;
    QString readQString( const QString& param, const QString& def = QString() ) const;
    void writeInt( const char* param, int val );
    void writeInt( const QString& param, int val );
    void writeBool( const QString& param, bool val );
    void writeString( const char* param, const char* value );
    void writeQString( const char* param, const QString& value );
    void writeQString( const QString& param, const QString& value );
};

#ifdef CPP__CONFIG
    Config* g_pConfig;
#else
    extern Config* g_pConfig;
#endif

#endif // H__CONFIG

