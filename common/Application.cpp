#include <QFont>
#include <QDir>
#include <QPalette>
#include <QScreen>
#if defined(Q_WS_QWS) && (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
 #include <QWSServer>
 #include <QTextCodec>
#endif
#if !defined(DESKTOP)
 #include "StyleEbook.h"
#endif

#include "Platform.h"
#include "Exception.h"
#include "QtUtils.h"
#include "version.h"
#include "utils.h"
#include "Log.h"
#include "Config.h"
#include "Platform.h"
#include "LangUtils.h"
#include "messagebox.h"

#include "Application.h"

Application::Application( int& argc, char** argv )
    : QApplication(argc, argv)
{
#if defined(Q_OS_ANDROID) || defined(SIMPLE) || defined(Q_OS_IOS)
    QObject::connect( this, SIGNAL(applicationStateChanged(Qt::ApplicationState)), this, SLOT(stateChanged(Qt::ApplicationState)) );
#endif
}

Application::~Application()
{
    if ( g_pLog != NULL )
        g_pLog->write(1, Log::MT_I, "Program finished");

    delete g_pConfig;
    delete g_pLog;

    Platform::destroy();
}

bool Application::init()
try
{
#if defined(Q_OS_IOS)
    setAttribute(Qt::AA_UseHighDpiPixmaps, primaryScreen()->devicePixelRatio() >= 2.0);
#endif

#if !defined(DESKTOP)
#if defined(Q_WS_QWS)
    Q_INIT_RESOURCE(keyboard);
    Q_INIT_RESOURCE(rotation);
#endif
#endif

    Q_INIT_RESOURCE(icons);

#if defined(EINK)
    setCursorFlashTime(0);
#endif

    QString logFile( Platform::get()->getUserSettingsPath() + QDir::separator() + applicationName() + ".log.txt" );
    g_pLog = new Log( QFile::encodeName(logFile).constData(), 1 );

    g_pConfig = new Config(applicationName());
    int logLevel = g_pConfig->readInt("log_level", -1);
    if ( -1 == logLevel )
    {
        logLevel = qgetenv("VLASOVSOFT_LOG").toInt();
    } 
    if ( logLevel < 0 ) logLevel = 0;
    if ( logLevel > 2 ) logLevel = 2;
    g_pLog->setLevel( logLevel );

    g_pLog->write(1, Log::MT_I, "Program started, version: %s", STR(VLASOVSOFT_VERSION));

    // set default application font
    QFont fnt = font();
    fnt.setPointSize(Platform::get()->getDefaultFontSize());
    setFont(fnt);

#if !defined(DESKTOP)
    StyleEbook* pStyle = new StyleEbook(style());
    int bSize = g_pConfig->readInt( "button_size", 40 ); // 40 = 1 cm
    QSize size( getSizePx( QSize(bSize, bSize), 5 ) );
    pStyle->setButtonIconSize(qMax( size.width(), size.height() ));
    setStyle(pStyle);
#endif

#if defined(Q_WS_QWS) && (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QWSServer::setBackground(QBrush(Qt::black));
    QTextCodec::setCodecForCStrings( QTextCodec::codecForName("utf8") );
#endif

    loadTranslations();

    customInit();

    return true;
}
catch(const Exception& e)
{
    if ( g_pLog != NULL )
        g_pLog->write(1, Log::MT_F, "Fatal error: %s", e.what().toLocal8Bit().constData());
    ::messageBox(0, QMessageBox::Critical, qApp->translate("main", "Error:"), e.what());
    return false;
}
catch(...)
{
    if ( g_pLog != NULL )
        g_pLog->write(1, Log::MT_F, "Fatal error!");
    ::messageBox(0, QMessageBox::Critical, qApp->translate("main", "Error:"), "Fatal error!" );
    return false;
}

