#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QKeyEvent>

#include "QtUtils.h"

#include "DirSelectDialog.h"
#include "ui_DirSelectDialog.h"

DirSelectDialog::DirSelectDialog(QWidget* parent, const QString& title, const QString& path, bool hidden)
    : Dialog(parent)
    , ui(new Ui::DirSelectDialog)
{
    ui->setupUi(this);
    init();

    setWindowTitle(title);

    ui->titleBar->titlePush(title);
    ui->titleBar->addSlot(this, SLOT(reject()));

    QFileSystemModel& model = ui->listView->getModel();
    model.setRootPath(path);
    if ( hidden ) {
        model.setFilter(model.filter() | QDir::Hidden);
    }
    model.setFilter( QDir::Dirs | QDir::AllDirs | QDir::NoDot );
    model.setNameFilterDisables(false);

    ui->listView->setRootIndex(model.index(path));

    ui->lblPath->setText(path);
    ui->lblPath->setElideMode( Qt::ElideMiddle );

    QObject::connect(&ui->listView->getModel(), SIGNAL(rootPathChanged(QString)), ui->lblPath, SLOT(setText(QString)) );
}

DirSelectDialog::~DirSelectDialog()
{
    delete ui;
}

void DirSelectDialog::accept()
{
    QModelIndex index = ui->listView->rootIndex();
    if ( index.isValid() ) {
        QFileInfo fInfo = ui->listView->getModel().fileInfo(index);
        if ( fInfo.isDir() )
        {
            path = fInfo.absoluteFilePath();
            Dialog::accept();
        }
    }
}
