#include <QAction>
#include <QResizeEvent>
#include <QStyle>
#include <QPainter>
#include <QDebug>

#include "gadgetmenuarea.h"
#include "gadgetmenu.h"

GadgetMenu::GadgetMenu(QWidget* parent, const QString& t)
    : QWidget(parent)
    , title(t)
    , enabled(true)
    , visible(true)
    , border(2)
{
    setWindowFlags( Qt::Popup );
    area = new GadgetMenuArea(this);
    QObject::connect(area, SIGNAL(activateAction(QAction*)), this, SLOT(onActivateAction(QAction*)), Qt::QueuedConnection);
    QObject::connect(area, SIGNAL(activateSubmenu(GadgetMenu*)), this, SLOT(onActivateSubmenu(GadgetMenu*)), Qt::QueuedConnection);
}

GadgetMenu::~GadgetMenu()
{
}

void GadgetMenu::addAction(QAction *a)
{
    area->addAction(a);
}

void GadgetMenu::addMenu(GadgetMenu* m)
{
    area->addMenu(m);
}

void GadgetMenu::addSeparator()
{
    area->addSeparator();
}

bool GadgetMenu::isMenuEnabled() const
{ return enabled; }

void GadgetMenu::setMenuEnabled(bool val)
{ enabled = val; }

bool GadgetMenu::isMenuVisible() const
{ return visible; }

void GadgetMenu::setMenuVisible(bool val)
{ visible = val; }

bool GadgetMenu::eventFilter(QObject *obj, QEvent *event)
{
    if ( QEvent::Resize == event->type() )
    {
        setup();
    }
    return QObject::eventFilter(obj, event);
}

QSize GadgetMenu::sizeHint() const
{
    return area->sizeHint();
}

void GadgetMenu::resizeEvent(QResizeEvent* e)
{
    area->setGeometry(0,0,e->size().width(), e->size().height());
}

void GadgetMenu::closeEvent(QCloseEvent *)
{
    parentWidget()->removeEventFilter(this);
}

void GadgetMenu::setup()
{
    QSize hint(sizeHint());
    QRect geo(parentWidget()->geometry());
    int height = qMin(hint.height(), 9*geo.height()/10);
    int width  = qMin(hint.width(), 9*geo.width()/10);
    if ( height < hint.height() ) {
        // there will be a vertical scrollbar
        // request additional width for it
        width = qMin(geo.width(), width+area->style()->pixelMetric(QStyle::PM_ScrollBarExtent));
    }
    if ( width != this->width() || height != this->height() )
    {
        resize(width, height);
    }
    else
    {
        area->layout();
    }

    move(geo.left()+(geo.width()-width)/2, geo.top()+(geo.height()-height)/2);
}

void GadgetMenu::exec()
{
    parentWidget()->installEventFilter(this);
    setup();
    show();
}

void GadgetMenu::onActivateAction(QAction* a)
{
    close();
    a->trigger();
}

void GadgetMenu::onActivateSubmenu(GadgetMenu* m)
{
    close();
    m->exec();
}
