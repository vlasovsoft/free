#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
public:
    GraphicsView(QWidget* parent = 0);

protected:
    void resizeEvent(QResizeEvent*);

private:
    void fitScene();
};

#endif // GRAPHICSVIEW_H
