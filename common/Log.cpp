#include <ctime>
#include <cstdio>
#include <cstdarg>

#include <fstream>
#include <iomanip>

#include <QString>

#define CPP__LOG

#include "Log.h"

Log::Log( const char* nm, int lvl )
    : level(lvl)
    , name(nm)
{
}

void Log::write( int lvl, Log::MsgType type, const char* format, ... )
{
    if ( lvl <= level )
    {
        QMutexLocker locker(&mutex);
        char t[5] = "IWEF";
        char pBuff[1024];
        unsigned buffLen = sizeof(pBuff)/sizeof(pBuff[0]);
        if ( !f.is_open() )
        {
            f.open( name.c_str(), std::ios_base::out | std::ios_base::app );
            f.fill('0');
        }
        if ( f.good() )
        {
            va_list ap;
            va_start(ap, format);
            vsnprintf(pBuff, buffLen, format, ap);
            pBuff[buffLen - 1] = '\0';
            va_end(ap);
            time_t rawtime = time(NULL);
            const tm* ptm = localtime(&rawtime);
            f << '[' 
                << std::setw(4) << 1900+ptm->tm_year << "-"
                << std::setw(2) << 1+ptm->tm_mon << "-"
                << ptm->tm_mday << " "
                << ptm->tm_hour << ":"
                << ptm->tm_min  << ":"
                << ptm->tm_sec 
              << "]\t" 
              << t[type] << '\t' 
              << pBuff 
            << std::endl;
            f.flush();
        }
    }
}

void Log::write(int lvl, Log::MsgType type, const QString &str)
{
    write(lvl, type, "%s", str.toLocal8Bit().constData());
}

