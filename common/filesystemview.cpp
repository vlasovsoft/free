#include <QDebug>

#include "QtUtils.h"

#include "filesystemview.h"

FileSystemView::FileSystemView(QWidget* parent)
    : QListView(parent)
    , iconFile(createIcon("file.png"))
    , iconFolder(createIcon("folder.png"))
{
    fsModel.setIconProvider(this);

    setModel(&fsModel);

    QFontInfo fi(font());
    setIconSize(QSize(fi.pixelSize(),fi.pixelSize()));

    connect(this, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)));
    connect(this, SIGNAL(clicked(QModelIndex)), this, SLOT(select(QModelIndex)));
}

QIcon FileSystemView::icon(QFileIconProvider::IconType type) const
{
    switch ( type )
    {
    case QFileIconProvider::Folder:
        return iconFolder;
        break;
    case QFileIconProvider::File:
        return iconFile;
        break;
    default:
        return QFileIconProvider::icon( type );
    }
}

QIcon FileSystemView::icon(const QFileInfo &info) const
{
    if ( info.isFile() )
        return iconFile;
    else
    if ( info.isDir() )
        return iconFolder;
    else
        return QFileIconProvider::icon( info );
}

void FileSystemView::select(const QModelIndex &index)
{
    if ( index.isValid() )
    {
        QFileInfo fInfo = fsModel.fileInfo(index);
        if ( fInfo.isDir() ) {
            if ( fInfo.fileName() == ".." ) {
                QModelIndex oldRoot(rootIndex());
                QModelIndex newRoot(fsModel.parent(oldRoot));
                if ( newRoot.isValid() )
                {
                    fsModel.setRootPath( fsModel.filePath( newRoot ) );
                    setRootIndex( newRoot );
                    setCurrentIndex( oldRoot );
                }
            } else {
                fsModel.setRootPath( fsModel.filePath( index ) );
                setRootIndex( index );
            }
        }
    }
}
