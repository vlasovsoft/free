#ifndef H__KOBO_PLATFORM
#define H__KOBO_PLATFORM

#include "QWSPlatform.h"
#include "KoboDevice.h"

class KoboPlatform : public QWSPlatform
{
public:
    KoboPlatform();
    virtual QString name() const { return "kobo"; }
    virtual QString getSerialNumber() const;
    virtual int getOrientation() const;
    virtual void setOrientation( int ori );
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual int frontlightGetMinTemp() const;
    virtual int frontlightGetMaxTemp() const;
    virtual void frontlightSetLevel( int val, int temp );

private:
    void setNaturalBrightness(int brig, int temp);
    bool comfortLight() const;
    const char* channel(int) const;
    void toRgb(unsigned);

private:
    KoboDevice dev;
    QString batteryPath;
    QString flWhite;
    QString flRed;
    QString flGreen;
    QString flMixer;
};

#endif // H__KOBO_PLATFORM

