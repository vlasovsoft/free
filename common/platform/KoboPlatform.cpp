#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <cmath>

#include <string>

#include <QDir>
#include <QFileInfo>
#include <QTextStream>
#include <QCoreApplication>

#include "QtUtils.h"

#include "KoboPlatform.h"

static void write_light_value(const QString& file, int value)
{
    QFile f(file);
    if (f.open(QIODevice::WriteOnly))
    {
        f.write(QString::number(value).toLatin1());
        f.flush();
        f.close();
    }
}

static void set_light_value(const QString& dir, int value)
{
    write_light_value(dir + "/bl_power", value > 0 ? 31:0);
    write_light_value(dir + "/brightness", value);
}

static void set_brightness( int value )
{
    int light = -1;
    // Open the file for reading and writing
    if ((light = ::open("/dev/ntx_io", O_RDWR)) != -1)
    {
        ::ioctl(light, 241, value);
        ::close(light);
    }
}

void get_first_valid_path(const char* paths[], QString& path)
{
    unsigned i = 0;
    size_t size = sizeof(paths)/sizeof(paths[0]);
    for (; i<size; ++i) {
        if ( QFileInfo(paths[i]).exists() ) {
            path = paths[i];
            break;
        }
    }
}

KoboPlatform::KoboPlatform()
    : dev(get_kobo_device())
{
    batteryPath = "/sys/class/power_supply/mc13892_bat";
    flWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
    switch ( dev )
    {
    case KoboAuraOne:
        flWhite = "/sys/class/backlight/lm3630a_led1b";
        flRed   = "/sys/class/backlight/lm3630a_led1a";
        flGreen = "/sys/class/backlight/lm3630a_ledb";
        break;
    case KoboAuraH2O2_v1:
        flWhite = "/sys/class/backlight/lm3630a_ledb";
        flRed   = "/sys/class/backlight/lm3630a_led";
        flGreen = "/sys/class/backlight/lm3630a_leda";
        break;
     case KoboAuraH2O2_v2:
        flWhite = "/sys/class/backlight/lm3630a_ledb";
        flRed   = "/sys/class/backlight/lm3630a_leda";
        break;
     case KoboClaraHD:
        flWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
        flMixer = "/sys/class/backlight/lm3630a_led/color";
        break;
     case KoboForma:
        flWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
        flMixer = "/sys/class/backlight/tlc5947_bl/color";
        break;
     case KoboLibra:
        flWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
        flMixer = "/sys/class/backlight/lm3630a_led/color";
        break;
     case KoboLibra2:
        // battery
        {
            static const char* paths[] = {
                "/sys/class/power_supply/battery",
                "/sys/class/power_supply/mc13892_bat"
            };
            get_first_valid_path(paths, batteryPath);
        }
        // frontlight
        {
            static const char* paths[] = {
                "/sys/class/leds/aw99703-bl_FL1/color",
                "/sys/class/backlight/lm3630a_led/color",
                "/sys/class/backlight/tlc5947_bl/color"
            };
            get_first_valid_path(paths, flMixer);
        }
        break;
     default:
        break;
    }
}

QString KoboPlatform::getSerialNumber() const
{
    static QString serial;
    if ( serial.isEmpty() )
    {
        QString fileName("/mnt/onboard/.kobo/version");
        serial = ::str_from_file(fileName);
        QStringList tokens( serial.split(',')  );
        serial = tokens.size() >= 1 ? tokens.at(0) : "0";
    }
    return serial;
}

int KoboPlatform::getOrientation() const
{
    static int map_norm[4]  = {1,0,3,2};
    static int map_h2o2[4]  = {3,2,1,0};
    static int map_libra[4] = {0,3,2,1};
    const int* map = map_norm;
    switch(dev)
    {
    case KoboAuraH2O2_v1:
    case KoboAuraH2O2_v2:
    case KoboLibra2:
        map = map_h2o2;
        break;
    case KoboLibra:
        map = map_libra;
        break;
    default:
        break;
    }
    return map[ QWSPlatform::getOrientation() ];
}

void KoboPlatform::setOrientation( int ori )
{
    static int map_norm[4]  = {1,0,3,2};
    static int map_h2o2[4]  = {3,2,1,0};
    static int map_libra[4] = {0,3,2,1};
    const int* map = map_norm;
    switch(dev)
    {
    case KoboAuraH2O2_v1:
    case KoboAuraH2O2_v2:
    case KoboLibra2:
        map = map_h2o2;
        break;
    case KoboLibra:
        map = map_libra;
        break;
    default:
        break;
    }
    QWSPlatform::setOrientation( map[ori] );
}

int KoboPlatform::getBatteryLevel() const
{
    return int_from_file(batteryPath + "/capacity");
}

bool KoboPlatform::isBatteryCharging() const
{
    return str_from_file(batteryPath + "/status") == "Charging";
}

int KoboPlatform::frontlightGetMinLevel() const
{
    return 0;
}

int KoboPlatform::frontlightGetMaxLevel() const
{
    return 100;
}

int KoboPlatform::frontlightGetMinTemp() const
{
    return 0;
}

int KoboPlatform::frontlightGetMaxTemp() const
{
    if ( comfortLight() )
    {
        switch( dev ) {
            case KoboForma:
            case KoboClaraHD:
            case KoboLibra:
            case KoboLibra2:
	            return 10;
            default:
	            return 100;
        }
    }
    return 0;
}

void KoboPlatform::frontlightSetLevel( int val, int temp )
{
    Platform::frontlightSetLevel(val,temp);
    if ( comfortLight() )
    {
        setNaturalBrightness(frontlightGetLevel(), frontlightGetTemp());
    }
    else
    {
        set_brightness(frontlightGetLevel());
    }
}

void KoboPlatform::setNaturalBrightness(int brig, int temp)
{
    if ( !flMixer.isEmpty() )
    {
        write_light_value( flWhite, brig );
        write_light_value( flMixer, 10 - temp );
    }
    else
    {
        static const int white_gain = 25;
        static const int red_gain = 24;
        static const int green_gain = 24;
        static const int white_offset = -25;
        static const int red_offset = 0;
        static const int green_offset = -65;
        static const double exponent = 0.25;
        double red = 0.0, green = 0.0, white = 0.0;
        if ( brig > 0 )
        {
            white = std::min(white_gain * pow(brig, exponent) * pow(frontlightGetMaxTemp()-temp, exponent) + white_offset, 255.0);
        }
        if ( temp > 0 )
        {
            red = std::min(red_gain * pow(brig, exponent) * pow(temp, exponent) + red_offset, 255.0);
            green = std::min(green_gain * pow(brig, exponent) * pow(temp, exponent) + green_offset, 255.0);
        }
        white = std::max(white, 0.0);
        red = std::max(red, 0.0);
        green = std::max(green, 0.0);

        if ( !flWhite.isEmpty() )
            set_light_value( flWhite, floor(white));
        if ( !flRed.isEmpty() )
            set_light_value( flRed, floor(red));
        if ( !flGreen.isEmpty() )
            set_light_value( flGreen, floor(green));
    }
}

bool KoboPlatform::comfortLight() const
{
    return !flMixer.isEmpty() || (!flRed.isEmpty() && !flGreen.isEmpty());
}
