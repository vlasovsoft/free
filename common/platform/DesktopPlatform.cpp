#include <QDir>

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#include <QDesktopWidget>
#endif

#if defined(Q_OS_WINDOWS)
#include <winsock2.h>
#include <iphlpapi.h>
#include <QCryptographicHash>
#endif

#include <QApplication>
#include <QClipboard>
#include <QLocale>
#include <QScreen>

#include "QtUtils.h"

#include "DesktopPlatform.h"

QString DesktopPlatform::name() const
{
#if defined(Q_OS_LINUX)
    return "linux";
#elif defined(Q_OS_WINDOWS)
    return "windows";
#elif defined(Q_OS_MACOS)
    return "osx";
#else
    return "null";
#endif
}

#if defined(Q_OS_WINDOWS)
static QString get_mac_addresses()
{
    IP_ADAPTER_INFO AdapterInfo[16];
    DWORD dwBufLen = sizeof (AdapterInfo);

    DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);
    if (dwStatus != ERROR_SUCCESS) {
        return QString();
    }

    QStringList list;

    PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;
    while (pAdapterInfo) {
        QString addr;
        for (int i = 0; i < 6; ++i) {
            addr += QString::number(pAdapterInfo->Address[i], 16);
        }
        list.push_back(addr);
        pAdapterInfo = pAdapterInfo->Next;
    }

    list.sort();

    return list.join(",");
}
#endif

QString DesktopPlatform::generateSerialNumber() const
{
    QString sn;
#if defined(Q_OS_LINUX)
    sn += "L";
#elif defined(Q_OS_WINDOWS)
    sn += "W";
#elif defined(Q_OS_MACOS)
    sn += "M";
#endif

#if defined(Q_OS_WINDOWS)
    QString addrs = get_mac_addresses();
    if ( !addrs.isEmpty() ) {
        QCryptographicHash hash(QCryptographicHash::Md5);
        hash.addData(addrs.toLatin1());
        sn += hash.result().toHex().left(8).toUpper();
    } else {
        sn += Platform::generateSerialNumber();
    }
#else
    sn += Platform::generateSerialNumber();
#endif

#if defined(AMAZON_STORE)
    sn += "-AMAZ";
#endif

    return sn;
}

QString DesktopPlatform::getUserSettingsPath() const
{
    QString path(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}

QString DesktopPlatform::getRootPath() const
{
#if defined(Q_OS_MACOS)
    return QCoreApplication::applicationDirPath() + "/../Resources";
#else
    return QCoreApplication::applicationDirPath();
#endif
}

QString DesktopPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

qreal DesktopPlatform::getDPI() const
{
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
}

QSize DesktopPlatform::getScreenSize() const
{
    return QGuiApplication::primaryScreen()->size();
}

QString DesktopPlatform::getDocumentsPath() const
{
#if QT_VERSION > 0x050000
    QString path(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
#else
    QString path(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation));
#endif
    path += QDir::separator();
    path += qApp->applicationName();
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}
