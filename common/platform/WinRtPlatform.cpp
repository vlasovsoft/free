#include <QDir>

#include <QStandardPaths>
#include <QApplication>
#include <QClipboard>
#include <QLocale>
#include <QScreen>

#include "QtUtils.h"

#include "WinRtPlatform.h"

QString WinRtPlatform::generateSerialNumber() const
{
    return QString("W") + Platform::generateSerialNumber();
}

QString WinRtPlatform::getUserSettingsPath() const
{
    QString path(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}

QString WinRtPlatform::getDocumentsPath() const
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString WinRtPlatform::getRootPath() const
{
    return QCoreApplication::applicationDirPath();
}

QString WinRtPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

int WinRtPlatform::getDefaultFontSize() const
{
    return 20;
}

qreal WinRtPlatform::getDPI() const
{
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
}
