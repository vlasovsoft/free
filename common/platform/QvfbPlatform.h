#ifndef H__QVFB_PLATFORM
#define H__QVFB_PLATFORM

#include <vector>

#include "QWSPlatform.h"

class QvfbPlatform : public QWSPlatform
{
    std::vector<unsigned> a1FlTable;

public:
    QvfbPlatform();
    virtual QString name() const { return "qvfb"; }
    virtual int frontlightGetMinLevel() const { return 0; }
    virtual int frontlightGetMaxLevel() const { return 100; }
    virtual int frontlightGetMinTemp() const { return 0; }
    virtual int frontlightGetMaxTemp() const { return 100; }
    virtual bool useSuspendManager() const { return true; }
};

#endif // H__QVFB_PLATFORM

