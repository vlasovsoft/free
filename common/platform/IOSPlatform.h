#include "Platform.h"

class QWidget;

class IOSPlatform : public Platform
{
public:
    QString name() const override { return "ios"; }
    QString generateSerialNumber() const override;
    QString getUserSettingsPath() const override;
    QString getDocumentsPath() const override;
    QString getRootPath() const override;
    QString getSystemLang() const override;
    int getDefaultFontSize() const override;
    qreal getDPI() const override;
    QSize getScreenSize() const override;
    QMargins getSafeAreaMargins(QWidget* w) const override;
};
