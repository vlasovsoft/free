#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_link.h>

#include <algorithm>

#include <QDebug>
#include <QDir>
#include <QScreen>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QWSDisplay>
#include <QTransformedScreen>
#else
#include <QGuiApplication>
#endif
#include <QCoreApplication>
#include <QProcess>

#include "QtUtils.h"

#include "QWSPlatform.h"

QWSPlatform::QWSPlatform()
    : nwTimeout(qgetenv("WIFI_TIMEOUT").toInt())
{
    nwTimer.setSingleShot(true);
    nwTimer.setInterval(nwTimeout*60*1000);
    connect(&nwTimer, SIGNAL(timeout()), this, SLOT(networkTimer()));
}

QString QWSPlatform::getUserSettingsPath() const
{
    return getRootPath();
}

QString QWSPlatform::getRootPath() const
{
    return QCoreApplication::applicationDirPath();
}

QString QWSPlatform::getDocumentsPath() const
{
    return getRootPath();
}

QString QWSPlatform::getTranslationsPath() const
{
    QString path(qgetenv("VLASOVSOFT_I18N"));
    if ( path.isEmpty() )
    {
        path = Platform::getTranslationsPath();
    }
    return path;
}

int QWSPlatform::getOrientation() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    return QScreen::instance()->transformOrientation();
#else
    return 0;
#endif
}

void QWSPlatform::setOrientation(int angle)
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QWSDisplay::setTransformation(static_cast<QTransformedScreen::Transformation>(angle), 0);
#endif
}

bool QWSPlatform::canRotateScreen() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QScreen* pScreen = QScreen::instance();
    return pScreen->depth() > 4 && QScreen::TransformedClass == pScreen->classId();
#else
    return false;
#endif
}

int QWSPlatform::getBatteryLevel() const
{
    return int_from_file(QString(qgetenv("ROOT")) + QDir::separator() + "batt_level.txt");
}

bool QWSPlatform::isBatteryCharging() const
{
    return str_from_file(QString(qgetenv("ROOT")) + QDir::separator() + "batt_status.txt") == "Charging";
}

void QWSPlatform::scheduleEinkFullUpdate() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QScreen* pScreen = QScreen::instance();
    pScreen->setDirty(QRect(0,0,pScreen->width(),pScreen->height()));
#endif
}

int QWSPlatform::getDefaultFontSize() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QScreen* pScreen = QScreen::instance();
    int fs = static_cast<int>(0.5 + std::min(pScreen->physicalWidth(),pScreen->physicalHeight()) * 72.0 / 25.4 / 18.0);
    return std::min(fs,15); // 15 points ~ 5.25 mm
#else
    return Platform::getDefaultFontSize();
#endif
}
    
QString QWSPlatform::getSystemLang() const
{
    return qgetenv("VLASOVSOFT_LNG");
}

qreal QWSPlatform::getDPI() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QScreen* pScreen = QScreen::instance();
    qreal dpiX = 25.4 * pScreen->width()  / pScreen->physicalWidth();
    qreal dpiY = 25.4 * pScreen->height() / pScreen->physicalHeight();
    return (dpiX + dpiY)/2.0;
#else
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
#endif
}

QSize QWSPlatform::getScreenSize() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QScreen* pScreen = QScreen::instance();
    return QSize(pScreen->width(), pScreen->height());
#else
    return QGuiApplication::primaryScreen()->size();
#endif
}

bool QWSPlatform::isNetworkActive(char* ip) const
{
    ifaddrs *ifaddr, *ifa;

    if (getifaddrs(&ifaddr) == -1) {
        return false;
    }

    int n = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next, n++)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        if ( 0 == strcmp( ifa->ifa_name, "lo" ) )
            continue;

        if ( 0 == strcmp( ifa->ifa_name, "usb0" ) )
            continue;

        int family = ifa->ifa_addr->sa_family;
        if (family == AF_INET || family == AF_INET6)
        {
            char host[NI_MAXHOST];
            if ( !getnameinfo(ifa->ifa_addr,sizeof(sockaddr_in), host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) )
            {
                if ( ip != NULL )
                    strncpy(ip,host,NI_MAXHOST);
                return true;
            }
        }
    }

    return false;
}

void QWSPlatform::switchNetworkOn()
{
    runScript("wifi_on.sh");
    if (nwTimeout > 0) {
        nwTimer.start();
    }
}

void QWSPlatform::switchNetworkOff()
{
    nwTimer.stop();
    runScript("wifi_off.sh");
}

void QWSPlatform::networkActivity()
{
    if (nwTimeout > 0) {
        nwTimer.start();
    }
}

void QWSPlatform::networkTimer()
{
    switchNetworkOff();
}
