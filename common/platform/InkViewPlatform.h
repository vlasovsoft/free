#ifndef H__INKVIEW_PLATFORM
#define H__INKVIEW_PLATFORM

#include "QWSPlatform.h"

class InkViewPlatform : public QWSPlatform
{
public:
    InkViewPlatform();
    virtual QString name() const { return "pb"; }
    virtual QString getSerialNumber() const;
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    // Frontlight functions
#if defined(OBREEY)
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual int frontlightGetLevel() const;
    virtual void frontlightSetLevel( int val, int );
#endif
    virtual void setSleepMode( bool val );
#if defined(OBREEY)
    // Network functions (e.g. Wifi, 3G)
    virtual void switchNetworkOn();
    virtual void switchNetworkOff();
#endif
};

#endif // H__INKVIEW_PLATFORM

