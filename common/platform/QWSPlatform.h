#ifndef H__QWS_PLATFORM
#define H__QWS_PLATFORM

#include <QTimer>

#include "Platform.h"

class QWSPlatform : public QObject, public Platform
{
Q_OBJECT
    int nwTimeout;  // network inactivity timeout
    QTimer nwTimer; // network inactivity timer
public:
    QWSPlatform();
    virtual QString name() const { return "qvfb"; }
    virtual QString getUserSettingsPath() const;
    virtual QString getDocumentsPath() const;
    virtual QString getRootPath() const;
    virtual QString getTranslationsPath() const;
    virtual int getOrientation() const;
    virtual void setOrientation( int angle );
    virtual bool canRotateScreen() const;
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    virtual void scheduleEinkFullUpdate() const;
    virtual int getDefaultFontSize() const;
    virtual QString getSystemLang() const;
    virtual qreal getDPI() const;
    virtual QSize getScreenSize() const;
    // Network functions (e.g. Wifi, 3G)
    virtual bool isNetworkActive(char* ip=NULL) const;
    virtual void switchNetworkOn();
    virtual void switchNetworkOff();
    virtual void networkActivity();
private slots:
    void networkTimer();
};

#endif // H__QWS_PLATFORM
