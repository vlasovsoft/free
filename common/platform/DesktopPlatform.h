#include "Platform.h"

class DesktopPlatform : public Platform
{
public:
    QString name() const override;
    QString generateSerialNumber() const override;
    QString getUserSettingsPath() const override;
    QString getDocumentsPath() const override;
    QString getRootPath() const override;
    QString getSystemLang() const override;
    qreal getDPI() const override;
    QSize getScreenSize() const override;
};
