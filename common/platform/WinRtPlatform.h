#ifndef WINRTPLATFORM_H
#define WINRTPLATFORM_H

#include "Platform.h"

class WinRtPlatform : public Platform
{
public:
    virtual QString name() const { return "winrt"; }
    virtual QString generateSerialNumber() const;
    virtual QString getUserSettingsPath() const;
    virtual QString getDocumentsPath() const;
    virtual QString getRootPath() const;
    virtual QString getSystemLang() const;
    virtual int getDefaultFontSize() const;
    virtual qreal getDPI() const;
};

#endif // WINRTPLATFORM_H
