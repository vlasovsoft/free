#include <cstdlib>

#include <QDir>
#include <QScreen>
#include <QGuiApplication>
#include <QAndroidJniObject>

#include "md5.h"

#include "AndroidPlatform.h"

AndroidPlatform::AndroidPlatform()
    : lockCounter(0)
{
}

QString AndroidPlatform::getSerialNumber() const
{
    static QString sn;
    if ( sn.isEmpty() )
    {
        sn = qgetenv("ARCH") + ::md5( qgetenv("SERIAL") ).left( 8 );
        QString suff(qgetenv("STORE"));
        if ( !suff.isEmpty() )
            sn += "-" + suff;
    }
    return sn;
}

QString AndroidPlatform::getRootPath() const
{
    return qgetenv("ROOT_PATH");
}

QString AndroidPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

int AndroidPlatform::getDefaultFontSize() const
{
    return 20;
}
    
QString AndroidPlatform::getTranslationsPath() const
{
    return qgetenv("VLASOVSOFT_I18N");
}

qreal AndroidPlatform::getDPI() const
{
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
}

QSize AndroidPlatform::getScreenSize() const
{
    return QGuiApplication::primaryScreen()->size();
}

void AndroidPlatform::vibrate( int ms )
{
    jint arg = ms;
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "vibrate", "(I)V", arg);
}

void AndroidPlatform::lockOrientation()
{
    if ( 0 == lockCounter )
    {
        QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "lockOrientation", "()V");
    }
    lockCounter++;
}

void AndroidPlatform::unlockOrientation()
{
    if ( lockCounter > 0 ) lockCounter--;
    if ( 0 == lockCounter )
    {   
        QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "unlockOrientation", "()V");
    }
}

void AndroidPlatform::loadAd()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "loadAd", "()V");
}

void AndroidPlatform::showAd()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "showAd", "()V");
}

void AndroidPlatform::rateApp()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "rateApp", "()V");
}

void AndroidPlatform::keepScreenOn(bool val)
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivityBase", "keepScreenOn", "(Z)V", val);
}

QString AndroidPlatform::getParameter(const QString &name) const
{
    QAndroidJniObject jName = QAndroidJniObject::fromString(name);
    QAndroidJniObject result = QAndroidJniObject::callStaticObjectMethod(
        "com/vlasovsoft/qtandroid/MainActivityBase",
        "getParameter",
        "(Ljava/lang/String;)Ljava/lang/String;",
        jName.object<jstring>());
    return result.toString();
}

