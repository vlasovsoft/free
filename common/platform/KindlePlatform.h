#ifndef H__KINDLE_PLATFORM
#define H__KINDLE_PLATFORM

#include <QVector>

#include "QWSPlatform.h"
#include "KindleDevice.h"

class KindlePlatform : public QWSPlatform
{
public:
    KindlePlatform();
    virtual ~KindlePlatform();
    virtual QString name() const { return "kindle"; }
    virtual QString getSerialNumber() const;
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual int frontlightGetMinTemp() const;
    virtual int frontlightGetMaxTemp() const;
    virtual int frontlightGetLevel() const;
    virtual int frontlightGetTemp() const;
    virtual void frontlightSetLevel( int val, int temp );

private:
    void frontlightStore( int value );

private:
    KindleDevice dev;
    const char* devBatteryLevel;
    const char* devBatteryIsCharging;
    const char* devFrontLight;
    void* dlHandle;
    int lipcHandle;

    typedef int (*lipc_open_t)();
    typedef int (*lipc_get_int_prop_t)(int, char*, char*, int*);
    typedef int (*lipc_set_int_prop_t)(int, char*, char*, int );
    typedef int (*lipc_close_t)(int);

    lipc_open_t lipc_open;
    lipc_get_int_prop_t lipc_get_int_prop;
    lipc_set_int_prop_t lipc_set_int_prop;
    lipc_close_t lipc_close;

private:
    void lipcInit();
    void lipcCleanup();
    void lipcSetIntProperty(const char* publisher, const char* property, int value);
    int lipcGetIntProperty(const char* publisher, const char* property) const;
};

#endif // H__KINDLE_PLATFORM
