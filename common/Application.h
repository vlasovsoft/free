#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <QApplication>
#include <QDebug>

class Application : public QApplication
{
    Q_OBJECT

public:
    Application( int & argc, char ** argv );
    ~Application();

    bool init();

protected:
    virtual void customInit() {}

#if defined(Q_OS_ANDROID) || defined(SIMPLE) || defined(Q_OS_IOS)
protected slots:
    virtual void stateChanged( Qt::ApplicationState ) {}
#endif
};

#endif /* APPLICATION_H_ */
