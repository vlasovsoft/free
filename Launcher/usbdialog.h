#ifndef USBDIALOG_H
#define USBDIALOG_H

#include "Dialog.h"

namespace Ui {
class UsbDialog;
}

class UsbDialog : public Dialog
{
    Q_OBJECT

public:
    UsbDialog(QWidget *parent = 0);
    ~UsbDialog();

private slots:
    void on_btnUsbNet_clicked();

    void on_btnCharging_clicked();

private:
    Ui::UsbDialog *ui;
};

#endif // USBDIALOG_H
