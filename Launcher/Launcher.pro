TEMPLATE = app
TARGET = launcher
COMMON = ../common

include(../common/common.pri)
include(../common/i18n.pri)

SOURCES += main.cpp Launcher.cpp frontlight.cpp \
    frontlightdlg.cpp \
    settingsdialog.cpp \
    batterydlg.cpp \
    blankwidget.cpp \
    screenshotdialog.cpp \
    usbdialog.cpp \
    progressbarfilter.cpp
HEADERS += Launcher.h frontlight.h \
    frontlightdlg.h \
    settingsdialog.h \
    batterydlg.h \
    blankwidget.h \
    screenshotdialog.h \
    usbdialog.h \
    progressbarfilter.h
FORMS   += Launcher.ui \
    frontlightdlg.ui \
    settingsdialog.ui \
    batterydlg.ui \
    screenshotdialog.ui \
    usbdialog.ui

RESOURCES += images/launcher.qrc

kobo|kindle|qvfb {
    DEFINES += SUSPEND_MANAGER
    INCLUDEPATH += ../SuspendManager
    DEPENDPATH += ../SuspendManager
    HEADERS += screensaverwidget.h keyboardfilter.h
    SOURCES += screensaverwidget.cpp keyboardfilter.cpp
    LIBS    += -L../SuspendManager -lSuspendManager
}
