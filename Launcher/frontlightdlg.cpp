#include <QApplication>
#include <QDesktopWidget>
#include <QMouseEvent>

#include "Platform.h"
#include "Config.h"
#include "frontlight.h"
#include "progressbarfilter.h"

#include "frontlightdlg.h"
#include "ui_frontlightdlg.h"

FrontlightDlg::FrontlightDlg(QWidget *parent)
    : QDialog(parent)
    , tempStep(1)
    , initDone(false)
    , ui(new Ui::FrontlightDlg)

{
    ui->setupUi(this);

    // setup temperature step
    int diffTemp = Platform::get()->frontlightGetMaxTemp() - Platform::get()->frontlightGetMinTemp();
    int diffLevel = Platform::get()->frontlightGetMaxLevel() - Platform::get()->frontlightGetMinLevel();

    // set frontlight level, min/max
    ui->pbFrontlightLevel->setMinimum(Platform::get()->frontlightGetMinLevel());
    ui->pbFrontlightLevel->setMaximum(Platform::get()->frontlightGetMaxLevel());
    ui->pbFrontlightLevel->setValue(::Platform::get()->frontlightGetLevel());
    ui->pbFrontlightLevel->setFormat(99 == diffLevel ? "%v%":"%v");

    if ( Platform::get()->frontlightGetMaxTemp() > 0 )
    {
        // set frontlight temperature, min/max
        ui->pbFrontlightTemp->setMinimum(Platform::get()->frontlightGetMinTemp());
        ui->pbFrontlightTemp->setMaximum(Platform::get()->frontlightGetMaxTemp());
        ui->pbFrontlightTemp->setValue(::Platform::get()->frontlightGetTemp());
        ui->pbFrontlightTemp->setFormat(100 == diffTemp ? "%v%":"%v");
    }
    else
    {
        ui->lblTemp->setVisible(false);
        ui->btnTempDec->setVisible(false);
        ui->btnTempInc->setVisible(false);
        ui->pbFrontlightTemp->setVisible(false);
    }

    QRect r(qApp->desktop()->screenGeometry());
    int size = r.height()/20;
    ui->pbFrontlightLevel->setMaximumHeight(size);
    ui->pbFrontlightTemp->setMaximumHeight(size);
    ui->btnFrontlightDec->setIconSize(QSize(size,size));
    ui->btnFrontlightInc->setIconSize(QSize(size,size));
    ui->btnTempDec->setIconSize(QSize(size,size));
    ui->btnTempInc->setIconSize(QSize(size,size));

    setAttribute(Qt::WA_TranslucentBackground);

    ui->widget->installEventFilter(new ProgressBarFilter(ui->widget));
    ui->pbFrontlightLevel->installEventFilter(new ProgressBarFilter(ui->pbFrontlightLevel));
    ui->pbFrontlightTemp->installEventFilter(new ProgressBarFilter(ui->pbFrontlightTemp, tempStep));

    showFullScreen();

    initDone = true;

}

FrontlightDlg::~FrontlightDlg()
{
    delete ui;
}

void FrontlightDlg::frontLightPlus()
{
    ui->pbFrontlightLevel->setValue(ui->pbFrontlightLevel->value()+1);
}

void FrontlightDlg::frontLightMinus()
{
    ui->pbFrontlightLevel->setValue(ui->pbFrontlightLevel->value()-1);
}

void FrontlightDlg::on_pbFrontlightLevel_valueChanged(int value)
{
    if ( initDone )
    {
        ::front_light_set_level( value, Platform::get()->frontlightGetTemp() );
    }
}

void FrontlightDlg::on_btnFrontlightInc_clicked()
{
    ui->pbFrontlightLevel->setValue( ui->pbFrontlightLevel->value() + 1 );
}

void FrontlightDlg::on_btnFrontlightDec_clicked()
{
    ui->pbFrontlightLevel->setValue( ui->pbFrontlightLevel->value() - 1 );
}

void FrontlightDlg::mouseReleaseEvent(QMouseEvent*)
{
    accept();
}

void FrontlightDlg::keyReleaseEvent(QKeyEvent *)
{
    accept();
}

void FrontlightDlg::on_btnTempDec_clicked()
{
    ui->pbFrontlightTemp->setValue( ui->pbFrontlightTemp->value() - tempStep );
}

void FrontlightDlg::on_btnTempInc_clicked()
{
    ui->pbFrontlightTemp->setValue( ui->pbFrontlightTemp->value() + tempStep );
}

void FrontlightDlg::on_pbFrontlightTemp_valueChanged(int value)
{
    if ( initDone )
    {
        ::front_light_set_level( Platform::get()->frontlightGetLevel(), value );
    }
}
