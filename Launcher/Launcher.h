#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMap>
#include <QWidget>
#include <QTimer>
#include <QProcess>

#include "pipethread.h"
#if defined(SUSPEND_MANAGER)
#include "SuspendManager.h"
#endif

#include "ui_Launcher.h"

class QPushButton;
class QKeyEvent;
class UsbDialog;
#if defined(SUSPEND_MANAGER)
class ScreenSaverWidget;
#endif

class Widget : public QWidget, private Ui::Launcher
{
    Q_OBJECT

    QProcess proc;
    QString process;
    QTimer launchTimer;
    QTimer batteryTimer;
    QTimer screenShotTimer;
    QString screenShotFileName;
    QMap<QString, QString> apps;
    PipeThread pt;
#if defined(KOBO) || defined(QVFB)
    PipeThread ptKobo;
#endif
    QString fifo;
    bool isNotifyOn;
    bool initDone;
    int batteryWarnLevel;
    int batteryCurrLevel;   
    int batteryCheckTimer;
    bool showBatteryLow;
    bool showBatteryCharged;
    UsbDialog* usbDlg;
#if defined(SUSPEND_MANAGER)
    ScreenSaverWidget* screenSaver;
    QProcess suspProc;
    QTimer suspTimerSuspend;
    QTimer suspTimerResume;
    bool isSuspended;
    int frontlightLevel;
    int frontlightTemp;
    bool isSleepCoverClosed;
#endif
public:
    Widget(QWidget *parent = 0);
    ~Widget();

private:
    void frontLightPlus();
    void frontLightMinus();
    void frontLightToggle();
    void onSuspend();
    void onResume();
    int getBatteryCheckTime() const;

private slots:
    void pipeMsg( const QString& msg );  
    void button();
    void execute();
    void battery();
#if defined(SUSPEND_MANAGER)
    void suspendRequest();
    void onSuspendProcessFinished(int);
    void onSuspendStateChanged( const SuspendManager::Transition& t );
    void screenSaverShow();
    void screenSaverHide();
    void runSuspendScript();
#endif
    void onFrontLightDlg();
    void deferredInit();
    void finished(int,QProcess::ExitStatus status);
    void onReadyReadStandardError();
    void onReadyReadStandardOutput();
    void on_btnSettings_clicked();
    void on_btnExit_clicked();
    void on_btnNext_clicked();
    void on_btnClose_clicked();
#if defined(KINDLE)
    void usbDriveConnected();
    void usbDriveDisconnected();
    void screenSaverActivated();
    void screenSaverDeactivated();
#endif
    void refreshScreen();
    void screenShotDirect();
    void screenShotRequest(); 
    void on_btnScreenShot_clicked();
    void showUsbDialog();
    void hideUsbDialog();

private:
    void execute( const QString& app );
#if defined(SUSPEND_MANAGER)
    void doSuspend();
    void doResume();
    void setupSuspendManager();
#endif
};

#endif // LAUNCHER_H
