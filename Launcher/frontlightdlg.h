#ifndef FRONTLIGHTDLG_H
#define FRONTLIGHTDLG_H

#include <QDialog>

namespace Ui {
class FrontlightDlg;
}

class FrontlightDlg : public QDialog
{
    Q_OBJECT
    
public:
    explicit FrontlightDlg(QWidget *parent = 0);
    ~FrontlightDlg();
    
    void frontLightPlus();
    void frontLightMinus();
    void frontLightToggle();

private slots:
    void on_pbFrontlightLevel_valueChanged(int value);
    void on_btnFrontlightInc_clicked();
    void on_btnFrontlightDec_clicked();
    void on_btnTempDec_clicked();
    void on_btnTempInc_clicked();
    void on_pbFrontlightTemp_valueChanged(int value);

private:
    virtual void mouseReleaseEvent(QMouseEvent*);
    virtual void keyReleaseEvent(QKeyEvent*);

private:
    int tempStep;
    bool initDone;
    Ui::FrontlightDlg *ui;
};

#endif // FRONTLIGHTDLG_H
