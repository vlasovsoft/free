<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1">
<context>
    <name>BatteryDlg</name>
    <message>
        <location filename="../batterydlg.ui" line="113"/>
        <source>Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="15"/>
        <source>Battery is charged!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="16"/>
        <source>Battery is low!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FrontlightDlg</name>
    <message>
        <location filename="../frontlightdlg.ui" line="14"/>
        <location filename="../frontlightdlg.ui" line="49"/>
        <source>Frontlight</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../frontlightdlg.ui" line="157"/>
        <source>Temperature</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Launcher</name>
    <message>
        <location filename="../Launcher.ui" line="143"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../Launcher.ui" line="196"/>
        <source>Close</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ScreenShotDialog</name>
    <message>
        <location filename="../screenshotdialog.ui" line="14"/>
        <source>Screen shot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="20"/>
        <source>&lt;b&gt;Screen Shot&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="39"/>
        <source>Delay</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="121"/>
        <source>File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="131"/>
        <source>Path</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="32"/>
        <source>Orientation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="86"/>
        <source>Log level</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="69"/>
        <source>Autostart</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>Normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="104"/>
        <source>Debug</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Language</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="114"/>
        <source>Suspend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="120"/>
        <source>Enable sleepcover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="129"/>
        <source>Timeout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="136"/>
        <source>Wallpaper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="150"/>
        <source>Blank screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="155"/>
        <source>Random image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="160"/>
        <source>Book cover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="189"/>
        <source>min</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="203"/>
        <source>Frontlight</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="223"/>
        <source>Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="94"/>
        <location filename="../settingsdialog.cpp" line="24"/>
        <source>Off</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UsbDialog</name>
    <message>
        <location filename="../usbdialog.ui" line="20"/>
        <source>USB mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="32"/>
        <source>Charging</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="39"/>
        <source>UsbNet</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../Launcher.cpp" line="332"/>
        <source>Failed to start application!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="480"/>
        <source>Abnormal application termination!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Done</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Screen shot is ready!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Failed to create screenshot!</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>