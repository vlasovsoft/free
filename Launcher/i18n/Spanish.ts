<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="es_ES">
<context>
    <name>BatteryDlg</name>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>Battery is charged!</source>
        <translation>¡Batería cargada!</translation>
    </message>
    <message>
        <source>Battery is low!</source>
        <translation>¡Batería baja!</translation>
    </message>
</context>
<context>
    <name>FrontlightDlg</name>
    <message>
        <source>Frontlight</source>
        <translation>Iluminación</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Launcher</name>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
<context>
    <name>ScreenShotDialog</name>
    <message>
        <source>&lt;b&gt;Screen Shot&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delay</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screen shot</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>Off</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>Debug</source>
        <translation>Depuración</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <source>Frontlight</source>
        <translation>Iluminación</translation>
    </message>
    <message>
        <source>Orientation</source>
        <translation>Orientación</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <source>Log level</source>
        <translation>Nivel de registro</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Autoinicio</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation>Suspender</translation>
    </message>
    <message>
        <source>Enable sleepcover</source>
        <translation>Habilitar funda inteligente</translation>
    </message>
    <message>
        <source>Wallpaper</source>
        <translation>Salvapantallas</translation>
    </message>
    <message>
        <source>Blank screen</source>
        <translation>En blanco</translation>
    </message>
    <message>
        <source>Random image</source>
        <translation>Aleatorio</translation>
    </message>
    <message>
        <source>Book cover</source>
        <translation>Portada de libro</translation>
    </message>
    <message>
        <source>Timeout (min)</source>
        <translation type="vanished">Inactividad (min)</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>min</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UsbDialog</name>
    <message>
        <source>Charging</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>UsbNet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>USB mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">Atención</translation>
    </message>
    <message>
        <source>Battery is low!</source>
        <translation type="vanished">¡Batería baja!</translation>
    </message>
    <message>
        <source>Failed to start application!</source>
        <translation>¡Fallo al iniciar aplicación!</translation>
    </message>
    <message>
        <source>Abnormal application termination!</source>
        <translation>¡Cierre anormal de aplicación!</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screen shot is ready!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <source>Failed to create screenshot!</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>