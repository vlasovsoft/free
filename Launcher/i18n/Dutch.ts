<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="nl_NL">
<context>
    <name>BatteryDlg</name>
    <message>
        <location filename="../batterydlg.ui" line="113"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="15"/>
        <source>Battery is charged!</source>
        <translatorcomment>Not sure where this is used. Depending on context maining could be 'wordt opgeladen' (=is currently being charged) or 'ís opgeladen' (=has been charged fully)</translatorcomment>
        <translation>Accu wordt opgeladen!</translation>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="16"/>
        <source>Battery is low!</source>
        <translation>Accu bijna leeg!</translation>
    </message>
</context>
<context>
    <name>FrontlightDlg</name>
    <message>
        <location filename="../frontlightdlg.ui" line="14"/>
        <location filename="../frontlightdlg.ui" line="49"/>
        <source>Frontlight</source>
        <translation>Verlichting</translation>
    </message>
    <message>
        <location filename="../frontlightdlg.ui" line="157"/>
        <source>Temperature</source>
        <translation>Temperatuur</translation>
    </message>
</context>
<context>
    <name>Launcher</name>
    <message>
        <location filename="../Launcher.ui" line="143"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../Launcher.ui" line="196"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
</context>
<context>
    <name>ScreenShotDialog</name>
    <message>
        <location filename="../screenshotdialog.ui" line="14"/>
        <source>Screen shot</source>
        <translation>Schermadruk</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="20"/>
        <source>&lt;b&gt;Screen Shot&lt;/b&gt;</source>
        <translation>&lt;b&gt;Schermafdruk&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="39"/>
        <source>Delay</source>
        <translation>Wachttijd</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="121"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="131"/>
        <source>Path</source>
        <translation>Pad</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="32"/>
        <source>Orientation</source>
        <translation>Orientatie</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="86"/>
        <source>Log level</source>
        <translation>Log niveau</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="69"/>
        <source>Autostart</source>
        <translation>Automatisch starten</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="104"/>
        <source>Debug</source>
        <translation>Foutopsporing</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="114"/>
        <source>Suspend</source>
        <translation>Pauzeren</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="120"/>
        <source>Enable sleepcover</source>
        <translation>Activeer sleepcover</translation>
    </message>
    <message>
        <source>Timeout (min)</source>
        <translation type="vanished">Timeout (min)</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="129"/>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="136"/>
        <source>Wallpaper</source>
        <translation>Achtergrond</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="150"/>
        <source>Blank screen</source>
        <translation>Leeg scherm</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="155"/>
        <source>Random image</source>
        <translation>Willekeurige afbeelding</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="160"/>
        <source>Book cover</source>
        <translation>Boek kaft</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="189"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="203"/>
        <source>Frontlight</source>
        <translation>Verlichting</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="223"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="94"/>
        <location filename="../settingsdialog.cpp" line="24"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
</context>
<context>
    <name>UsbDialog</name>
    <message>
        <location filename="../usbdialog.ui" line="20"/>
        <source>USB mode</source>
        <translation>USB mode</translation>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="32"/>
        <source>Charging</source>
        <translation>Opladen</translation>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="39"/>
        <source>UsbNet</source>
        <translation>UsbNet</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../Launcher.cpp" line="332"/>
        <source>Failed to start application!</source>
        <translation>Applicatie kon niet gestart worden!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="480"/>
        <source>Abnormal application termination!</source>
        <translation>Applicatie onverwacht gesloten!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Done</source>
        <translation>Gedaan</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Screen shot is ready!</source>
        <translation>Schermafdruk klaar!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Failed to create screenshot!</source>
        <translation>Schermafdruk maken is mislukt!</translation>
    </message>
</context>
</TS>