<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="it_IT">
<context>
    <name>BatteryDlg</name>
    <message>
        <location filename="../batterydlg.ui" line="113"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="15"/>
        <source>Battery is charged!</source>
        <translation>Carica batteria completata!</translation>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="16"/>
        <source>Battery is low!</source>
        <translation>Batteria in esaurimento!</translation>
    </message>
</context>
<context>
    <name>FrontlightDlg</name>
    <message>
        <location filename="../frontlightdlg.ui" line="14"/>
        <location filename="../frontlightdlg.ui" line="49"/>
        <source>Frontlight</source>
        <translation>Illuminazione</translation>
    </message>
    <message>
        <location filename="../frontlightdlg.ui" line="157"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
</context>
<context>
    <name>Launcher</name>
    <message>
        <location filename="../Launcher.ui" line="143"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../Launcher.ui" line="196"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>ScreenShotDialog</name>
    <message>
        <location filename="../screenshotdialog.ui" line="14"/>
        <source>Screen shot</source>
        <translation>Istantanea schermo</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="20"/>
        <source>&lt;b&gt;Screen Shot&lt;/b&gt;</source>
        <translation>&lt;b&gt;Istantanea&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="39"/>
        <source>Delay</source>
        <translation>Ritardo</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="121"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="131"/>
        <source>Path</source>
        <translation>Destinazione</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="32"/>
        <source>Orientation</source>
        <translation>Orientamento</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="86"/>
        <source>Log level</source>
        <translation>Livello per i Log</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="69"/>
        <source>Autostart</source>
        <translation>Avvio automatico</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="104"/>
        <source>Debug</source>
        <translation>verifica errori</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="114"/>
        <source>Suspend</source>
        <translation>Sospensione</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="120"/>
        <source>Enable sleepcover</source>
        <translation>Attiva schermata sospensione</translation>
    </message>
    <message>
        <source>Timeout (min)</source>
        <translation type="vanished">Tempo di attesa (min)</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="129"/>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="136"/>
        <source>Wallpaper</source>
        <translation>Sfondo</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="150"/>
        <source>Blank screen</source>
        <translation>Schermo bianco</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="155"/>
        <source>Random image</source>
        <translation>Immagine casuale</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="160"/>
        <source>Book cover</source>
        <translation>Copertina del libro</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="189"/>
        <source>min</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="203"/>
        <source>Frontlight</source>
        <translation>Illuminazione</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="223"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="94"/>
        <location filename="../settingsdialog.cpp" line="24"/>
        <source>Off</source>
        <translation>Spegni</translation>
    </message>
</context>
<context>
    <name>UsbDialog</name>
    <message>
        <location filename="../usbdialog.ui" line="20"/>
        <source>USB mode</source>
        <translation>Modalità USB</translation>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="32"/>
        <source>Charging</source>
        <translation>Ricarica</translation>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="39"/>
        <source>UsbNet</source>
        <translation>USBNet</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../Launcher.cpp" line="332"/>
        <source>Failed to start application!</source>
        <translation>Errore durante l'avvio dell'applicazione!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="480"/>
        <source>Abnormal application termination!</source>
        <translation>Arresto dell'applicazione anomalo!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Screen shot is ready!</source>
        <translation>L'istantanea è pronta!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Failed to create screenshot!</source>
        <translation>Impossibile salvare istantanea!</translation>
    </message>
</context>
</TS>