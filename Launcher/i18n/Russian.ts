<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ru">
<context>
    <name>BatteryDlg</name>
    <message>
        <location filename="../batterydlg.ui" line="113"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="15"/>
        <source>Battery is charged!</source>
        <translation>Батарея заряжена!</translation>
    </message>
    <message>
        <location filename="../batterydlg.cpp" line="16"/>
        <source>Battery is low!</source>
        <translation>Низкий заряд аккумулятора!</translation>
    </message>
</context>
<context>
    <name>FrontlightDlg</name>
    <message>
        <location filename="../frontlightdlg.ui" line="14"/>
        <location filename="../frontlightdlg.ui" line="49"/>
        <source>Frontlight</source>
        <translation>Подсветка</translation>
    </message>
    <message>
        <location filename="../frontlightdlg.ui" line="157"/>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
</context>
<context>
    <name>Launcher</name>
    <message>
        <location filename="../Launcher.ui" line="143"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../Launcher.ui" line="196"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>ScreenShotDialog</name>
    <message>
        <location filename="../screenshotdialog.ui" line="14"/>
        <source>Screen shot</source>
        <translation>Снимок экрана</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="20"/>
        <source>&lt;b&gt;Screen Shot&lt;/b&gt;</source>
        <translation>&lt;b&gt;Снимок экрана&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="39"/>
        <source>Delay</source>
        <translation>Задержка</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="121"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../screenshotdialog.ui" line="131"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="32"/>
        <source>Orientation</source>
        <translation>Ориентация</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="86"/>
        <source>Log level</source>
        <translation>Логирование</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="69"/>
        <source>Autostart</source>
        <translation>Автозапуск</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>Normal</source>
        <translation>Нормально</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="104"/>
        <source>Debug</source>
        <translation>Отладка</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="114"/>
        <source>Suspend</source>
        <translation>Сон</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="120"/>
        <source>Enable sleepcover</source>
        <translation>Разрешить обложку</translation>
    </message>
    <message>
        <source>Timeout (min)</source>
        <translation type="vanished">Таймаут (мин)</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="129"/>
        <source>Timeout</source>
        <translation>Таймаут</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="136"/>
        <source>Wallpaper</source>
        <translation>Обои</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="150"/>
        <source>Blank screen</source>
        <translation>Пустой экран</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="155"/>
        <source>Random image</source>
        <translation>Случайная картинка</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="160"/>
        <source>Book cover</source>
        <translation>Обложка книги</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="189"/>
        <source>min</source>
        <translation>мин</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="203"/>
        <source>Frontlight</source>
        <translation>Подсветка</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="223"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="94"/>
        <location filename="../settingsdialog.cpp" line="24"/>
        <source>Off</source>
        <translation>Откл</translation>
    </message>
</context>
<context>
    <name>UsbDialog</name>
    <message>
        <location filename="../usbdialog.ui" line="20"/>
        <source>USB mode</source>
        <translation>Режим USB</translation>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="32"/>
        <source>Charging</source>
        <translation>Зарядка</translation>
    </message>
    <message>
        <location filename="../usbdialog.ui" line="39"/>
        <source>UsbNet</source>
        <translation>UsbNet</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">Внимание</translation>
    </message>
    <message>
        <source>Battery is low!</source>
        <translation type="vanished">Низкий заряд аккумулятора!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="332"/>
        <source>Failed to start application!</source>
        <translation>Ошибка запуска приложения!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="480"/>
        <source>Abnormal application termination!</source>
        <translation>Аварийное завершение приложения!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="675"/>
        <source>Screen shot is ready!</source>
        <translation>Снимок экрана готов!</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../Launcher.cpp" line="677"/>
        <source>Failed to create screenshot!</source>
        <translation>Ошибка создания снимка экрана!</translation>
    </message>
</context>
</TS>