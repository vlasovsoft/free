#include <signal.h>

#include <QFont>
#include <QFile>
#include <QLabel>
#include <QTextStream>
#include <QVBoxLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QTimer>
#include <QCoreApplication>
#include <QProcess>
#include <QDir>
#include <QSpacerItem>
#include <QKeyEvent>
#if defined(Q_WS_QWS)
#include <QWSServer>
#include <QWSDisplay>
#include <QTransformedScreen>
#endif
#include <QScrollBar>
#include <QScreen>
#include <QDesktopWidget>
#include <QDebug>
#if defined(KINDLE)
#include <QDBusConnection>
#endif

#include "Log.h"
#include "Config.h"
#include "Platform.h"
#include "QtUtils.h"
#if defined(SUSPEND_MANAGER)
#include "SuspendManager.h"
#include "keyboardfilter.h"
#include "screensaverwidget.h"
#endif
#include "frontlight.h"
#include "frontlightdlg.h"
#include "languagedlg.h"
#include "LangUtils.h"
#include "Factory.h"
#include "Singleton.h"
#include "LanguageDescriptor.h"
#include "settingsdialog.h"
#include "batterydlg.h"
#include "blankwidget.h"
#include "messagebox.h"
#include "screenshotdialog.h"
#include "usbdialog.h"

#include "Launcher.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , pt( qgetenv("VLASOVSOFT_FIFO1") )
#if defined(KOBO) || defined(QVFB)
    , ptKobo( "/tmp/nickel-hardware-status" )
#endif
    , fifo( qgetenv("VLASOVSOFT_FIFO2") )
    , isNotifyOn(false)
    , initDone(false)
    , batteryWarnLevel(g_pConfig->readInt("battery_warn_level", 30))
    , batteryCurrLevel(Platform::get()->getBatteryLevel())
    , batteryCheckTimer(getBatteryCheckTime())
    , showBatteryLow(true)
    , showBatteryCharged(true)
    , usbDlg(NULL)
#if defined(SUSPEND_MANAGER)
    , screenSaver(NULL)
    , isSuspended(false)
    , frontlightLevel(0)
    , frontlightTemp(0)
    , isSleepCoverClosed(false)
#endif
{
    QFont fnt(font());
    fnt.setPointSize(14);
    setFont(fnt);

    setupUi(this);

    int iconSize = mm_to_px(10);
    btnClose->setIconSize(QSize(iconSize,iconSize));
    btnSettings->setIconSize(QSize(iconSize,iconSize));
    btnScreenShot->setIconSize(QSize(iconSize,iconSize));
    btnNext->setIconSize(QSize(iconSize,iconSize));

    launchTimer.setInterval(0);
    launchTimer.setSingleShot(true);    

    QPushButton *pb = NULL, *fpb = NULL;
    QFile f(Platform::get()->getRootPath() + QDir::separator() + "applications.ini");
    f.open(QIODevice::ReadOnly);
    QTextStream stream(&f);
    while ( !stream.atEnd() )
    {
        QString str = stream.readLine();
        QStringList lst = str.split('=');
        if ( lst.size() > 1 )
        {
            QString name(lst[0].trimmed());
            QString path(lst[1].trimmed());
            pb = new QPushButton(name, this);          
            if ( !fpb ) fpb = pb;
            if ( path[0] != '/' ) // absolute
            {
                path = Platform::get()->getRootPath() + QDir::separator() + path;
            }
            apps.insert(name, path);
            scrollArea->widget()->layout()->addWidget(pb);
            connect(pb, SIGNAL(clicked()), this, SLOT(button()));
            g_pLog->write(2, Log::MT_I, "Added: %s -> %s", name.toUtf8().constData(), QFile::encodeName(path).constData());
        }
    }

    if ( fpb )
    {
        setTabOrder( pb, btnSettings );
        setTabOrder( btnSettings, btnExit );
        fpb->setFocus();
    }

    QSpacerItem* verticalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
    scrollArea->widget()->layout()->addItem( verticalSpacer );

    connect( &launchTimer,  SIGNAL(timeout()), this, SLOT(execute()) );
    connect( &batteryTimer, SIGNAL(timeout()), this, SLOT(battery()) );
    connect( &proc, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(finished(int,QProcess::ExitStatus)) );
    connect( &proc, SIGNAL(readyReadStandardError()), this, SLOT(onReadyReadStandardError()));
    connect( &proc, SIGNAL(readyReadStandardOutput()), this, SLOT(onReadyReadStandardOutput()));    
    connect( &pt, SIGNAL(sigPipeMsg(QString)), this, SLOT(pipeMsg(QString)));
#if defined(KOBO) || defined(QVFB)
    connect( &ptKobo, SIGNAL(sigPipeMsg(QString)), this, SLOT(pipeMsg(QString)));
#endif

    // show
    showFullScreen();

    ::front_light_startup();

    Platform::get()->setOrientation( g_pConfig->readInt( "orientation", 0 ) );

    stackedWidget->setCurrentIndex(0);

#if defined(KINDLE)
    QDBusConnection::systemBus().connect(QString(), QString(), "com.lab126.hal", "usbConfigured", this, SLOT(usbDriveConnected()));
    QDBusConnection::systemBus().connect(QString(), QString(), "com.lab126.hal", "usbUnconfigured", this, SLOT(usbDriveDisconnected()));
    if ( !SuspendManager::active() )
    {
        QDBusConnection::systemBus().connect(QString(), QString(), "com.lab126.powerd", "goingToScreenSaver", this, SLOT(screenSaverActivated()));
        QDBusConnection::systemBus().connect(QString(), QString(), "com.lab126.powerd", "outOfScreenSaver", this, SLOT(screenSaverDeactivated()));
    }
#endif

    initDone = true;

    QTimer::singleShot(0, this, SLOT(deferredInit()));

    batteryTimer.setInterval(1000);
    batteryTimer.start();

    screenShotTimer.setSingleShot(true);
    QObject::connect( &screenShotTimer, SIGNAL(timeout()), this, SLOT(screenShotDirect()) );

#if defined(SUSPEND_MANAGER)
    if ( SuspendManager::active() )
    {
        setupSuspendManager();
    }
#endif

#if defined(Q_WS_QWS)
    g_pLog->write(2, Log::MT_I, "Screen logical size: %d x %d", qt_screen->width(), qt_screen->height());
    g_pLog->write(2, Log::MT_I, "Screen device size: %d x %d", qt_screen->deviceWidth(), qt_screen->deviceHeight());
#endif
}

Widget::~Widget()
{
    ::front_light_cleanup();
}

void Widget::frontLightPlus()
{
    int step = g_pConfig->readInt("frontlight_step", 1);
    int curLevel = Platform::get()->frontlightGetLevel();
    int newLevel = (curLevel/step+1)*step;
    ::front_light_set_level(newLevel,Platform::get()->frontlightGetTemp());
}

void Widget::frontLightMinus()
{
    int step = g_pConfig->readInt("frontlight_step", 1);
    int curLevel = Platform::get()->frontlightGetLevel();
    int newLevel = (curLevel % step == 0) ?
            (curLevel/step-1)*step :
            (curLevel/step)*step;
    ::front_light_set_level(newLevel,Platform::get()->frontlightGetTemp());
}

void Widget::onSuspend()
{
    // notify running proc about suspend
    if ( QProcess::Running == proc.state() && isNotifyOn )
        writeFifoCommand(fifo, "suspend");

#if defined(SUSPEND_MANAGER)
    if ( SuspendManager::active() )
    {
        screenSaverShow();
        suspTimerResume.stop();
        suspTimerSuspend.start();
    }
#endif
}

void Widget::onResume()
{
    // notify running proc about resume
    if ( QProcess::Running == proc.state() && isNotifyOn )
        writeFifoCommand(fifo, "resume");

    showBatteryCharged = true;
    showBatteryLow = true;

#if defined(SUSPEND_MANAGER)
    if ( SuspendManager::active() )
    {
        suspTimerSuspend.stop();
        suspTimerResume.start();
    }
#endif
}

int Widget::getBatteryCheckTime() const
{
    return g_pConfig->readInt("battery_check_time", 60);
}

void Widget::pipeMsg( const QString& msg )
{
    if ( msg == "exit" )
    {
        if ( QProcess::Running == proc.state() )
            proc.waitForFinished(2000);
        close();
    }
#if defined(SUSPEND_MANAGER)
    else
    if ( msg == "suspend" )
        suspendRequest();
#endif
    else
    if ( msg == "fld" )
        onFrontLightDlg();
    else
    if ( msg == "fl+" )
        frontLightPlus();
    else
    if ( msg == "fl-" )
        frontLightMinus();
    else
    if ( msg == "n+" )
        isNotifyOn = true;
#if defined(OBREEY)
    else
    if ( msg == "s+" )
        Platform::get()->setSleepMode(true);
#endif
    else
    if ( msg == "shot" )
        screenShotRequest();
    else
    if ( msg == "settings" )
        on_btnSettings_clicked();
    else
    if ( msg == "usb plug add" )
    {
        showUsbDialog();
    }
    else
    if ( msg == "usb plug remove" )
    {
        hideUsbDialog();
    }
    else
    if ( msg == "blank" )
    {
        new BlankWidget(NULL);
    }
    else
    if ( msg == "activity" )
    {
#if defined(SUSPEND_MANAGER)
        SuspendManager::instanse()->activity();
#endif
    }
}

void Widget::button()
{
    if ( QProcess::NotRunning == proc.state() )
    {
        QPushButton* pSender = qobject_cast<QPushButton*>(sender());
        if ( pSender )
        {
            execute( pSender->text() );
        }
    }
}

void Widget::execute()
{
    g_pLog->write(2, Log::MT_I, "Executing: %s", QFile::encodeName(process).constData());

    // set process language
    const Factory<LanguageDescriptor>& ldf = Singleton<Factory<LanguageDescriptor> >::instance();
    int nLng = g_pConfig->readInt("lang", 0);
    if ( nLng < 0 || nLng >= ldf.size() ) nLng = 0;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("VLASOVSOFT_LNG", ldf.at(nLng).name);
    env.insert("VLASOVSOFT_LOG", QString::number(g_pLog->getLevel()));
    proc.setProcessEnvironment(env);

    // start process
    proc.start(process);
    isNotifyOn = false;

    if ( !proc.waitForStarted() ) 
    {
        lblMessage->setText(tr("Failed to start application!"));
        stackedWidget->setCurrentIndex(2);
    }
    else
    {
        stackedWidget->setCurrentIndex(1);
    }
 
    process.clear();
}

void Widget::battery()
{
    batteryCheckTimer--;
    int level = Platform::get()->getBatteryLevel();
    if ( batteryCheckTimer <= 0 )
    {
        batteryCheckTimer = getBatteryCheckTime();
        if ( batteryCurrLevel != level ) 
        {
            if ( QProcess::Running == proc.state() && isNotifyOn )
                writeFifoCommand(fifo, "battery");

            batteryCurrLevel = level;
        }

        if ( Platform::get()->isBatteryCharging() )
        {
            static bool isShown = false;
            if ( level >= 100 && showBatteryCharged && !isShown )
            {
                showBatteryCharged = false;
                g_pLog->write(2, Log::MT_I, "Battery is charged! %d >= 100", level);
                isShown = true;
                BatteryDlg(this,true).exec();
                isShown = false;
            }
        }
        else
        {
            static bool isShown = false;
            if ( level < batteryWarnLevel && showBatteryLow && !isShown )
            {
                showBatteryLow = false;
                g_pLog->write(2, Log::MT_I, "Battery is low! %d < %d", level, batteryWarnLevel);
                isShown = true;
                BatteryDlg(this,false).exec();
                isShown = false;
            }
        }
    }
}

#if defined(SUSPEND_MANAGER)
void Widget::suspendRequest()
{
    SuspendManager::instanse()->event( SuspendManager::evFifo );
}

void Widget::onSuspendProcessFinished(int)
{
    g_pLog->write(2, Log::MT_I, "suspend.sh: finished");
    if ( isSuspended )
    {
        suspTimerResume.stop();
        suspTimerSuspend.start();
    }
}

void Widget::onSuspendStateChanged(const SuspendManager::Transition &t)
{
    g_pLog->write(2, Log::MT_I, "Change state: %s -> %s -> %s",
                  SuspendManager::get_state_name(t.oldState),
                  SuspendManager::get_event_name(t.event),
                  SuspendManager::get_state_name(t.newState));

    if ( SuspendManager::evSleepCoverPressed == t.event )
        isSleepCoverClosed = true;
    else
    if ( SuspendManager::evSleepCoverReleased == t.event )
        isSleepCoverClosed = false;

    switch ( t.newState )
    {
    case SuspendManager::ssSuspended:
        g_pLog->write(2, Log::MT_I, "Suspending...reason: %s, battery: %d", SuspendManager::get_event_name(t.event), Platform::get()->getBatteryLevel());
        onSuspend();
        doSuspend();
        break;
    case SuspendManager::ssActive:
        g_pLog->write(2, Log::MT_I, "Resuming...reason: %s, battery: %d", SuspendManager::get_event_name(t.event), Platform::get()->getBatteryLevel());
        onResume();
        doResume();
        break;
    default:
        break;
    }
}

void Widget::screenSaverShow()
{
    if ( !screenSaver->isVisible() )
    {
        screenSaver->nextWallpaper();
        screenSaver->showFullScreen();
    }
}

void Widget::screenSaverHide()
{
    screenSaver->hide();
}

void Widget::runSuspendScript()
{
    suspProc.start(Platform::get()->getRootPath() + QDir::separator() + "suspend.sh");
    if ( suspProc.waitForStarted() )
        g_pLog->write(2, Log::MT_I, "suspend.sh: started");
    else
        g_pLog->write(2, Log::MT_I, "suspend.sh: is not started");
}
#endif

void Widget::onFrontLightDlg()
{
    FrontlightDlg(this).exec();
}

void Widget::deferredInit()
{
    const Factory<LanguageDescriptor>& ldf = Singleton<Factory<LanguageDescriptor> >::instance();
    int lang = g_pConfig->readInt( "lang", -1 );
    if ( lang < 0 || lang >= ldf.size() )
    {
        LanguageDlg(this).exec();
    }
    QString app(g_pConfig->readQString("autorun", "Off"));
    if ( app != "Off")
    {
        execute( app );
    }
}

void Widget::finished(int, QProcess::ExitStatus status)
{
    Platform::get()->setOrientation( g_pConfig->readInt("orientation", 0) );
    if ( QProcess::CrashExit == status )
    {
        lblMessage->setText(tr("Abnormal application termination!"));
        stackedWidget->setCurrentIndex(2);
    }
    else
    {
        stackedWidget->setCurrentIndex(0);
    }
}

void Widget::onReadyReadStandardError()
{
    QProcess* p = qobject_cast<QProcess*>(sender());
    if ( p != NULL )
    {
        QByteArray str(p->readAllStandardError());
        str.replace('\n', ' ');
        g_pLog->write(2, Log::MT_I, "process >> %s", str.constData() );
    }
}

void Widget::onReadyReadStandardOutput()
{
    QProcess* p = qobject_cast<QProcess*>(sender());
    if ( p != NULL )
    {
        QByteArray str(p->readAllStandardOutput());
        str.replace('\n', ' ');
        g_pLog->write(2, Log::MT_I, "process >> %s", str.constData() );
    }
}

void Widget::on_btnSettings_clicked()
{
    SettingsDialog(this, apps).exec();
}

void Widget::execute(const QString& app)
{
    QMap<QString, QString>::const_iterator i( apps.find( app ) );
    if ( i != apps.end() )
    {
        process = i.value();
        launchTimer.start();
        stackedWidget->setCurrentIndex(1);
    }
}

#if defined(SUSPEND_MANAGER)
void Widget::doSuspend()
{
    isSuspended = true;
    frontlightLevel = Platform::get()->frontlightGetLevel();
    frontlightTemp = Platform::get()->frontlightGetTemp();
    front_light_set_level( 0, frontlightTemp );
}

void Widget::doResume()
{
    isSuspended = false;
    if (    QProcess::Running == suspProc.state()
         && !proc.waitForFinished(200) )
    {
        g_pLog->write(2, Log::MT_I, "suspend.sh: terminating...");
        suspProc.terminate();
        suspProc.waitForFinished();
    }
    ::front_light_set_level( frontlightLevel, frontlightTemp );
    SuspendManager::instanse()->activity(); // restart suspend timer after resume
    g_pLog->write(2, Log::MT_I, "Resumed. battery: %d", Platform::get()->getBatteryLevel());
    if ( isSleepCoverClosed )
    {
        // resumed but the sleep cover is still closed
        // it can be when we occasionally touch the power button
        g_pLog->write(2, Log::MT_I, "Sleep cover button pressed");
        SuspendManager::instanse()->event( SuspendManager::evSleepCoverPressed );
    }
}

void Widget::setupSuspendManager()
{
    int suspendTimeout = g_pConfig->readInt( "sleep_timeout", 10 );
    SuspendManager::instanse()->setSuspendTimeout(suspendTimeout * 1000 * 60);
    g_pLog->write(1, Log::MT_I, "Sleep timeout: %d", suspendTimeout);
    QWSServer::addKeyboardFilter(new KeyboardFilter());
    screenSaver = new ScreenSaverWidget(NULL);
    suspTimerSuspend.setSingleShot(true);
    suspTimerSuspend.setInterval(1000);
    suspTimerResume.setSingleShot(true);
    suspTimerResume.setInterval(g_pConfig->readInt("ss_hide", 0, 0, 2000));
    QObject::connect(&suspTimerSuspend, SIGNAL(timeout()), this, SLOT(runSuspendScript()));
    QObject::connect(&suspTimerResume, SIGNAL(timeout()), this, SLOT(screenSaverHide()));
    QObject::connect(&suspProc, SIGNAL(finished(int)), this, SLOT(onSuspendProcessFinished(int)));
    QObject::connect(&suspProc, SIGNAL(readyReadStandardError()), this, SLOT(onReadyReadStandardError()));
    QObject::connect(&suspProc, SIGNAL(readyReadStandardOutput()), this, SLOT(onReadyReadStandardOutput()));
    QObject::connect(SuspendManager::instanse(), SIGNAL(stateChanged(SuspendManager::Transition)), this, SLOT(onSuspendStateChanged(SuspendManager::Transition)));
}
#endif

void Widget::on_btnExit_clicked()
{
    close();
}

void Widget::on_btnNext_clicked()
{
    QScrollBar* pBar = scrollArea->verticalScrollBar();
    int curr = pBar->value();
    int page = pBar->pageStep();
    int max  = pBar->maximum();
    if ( curr == max )
        curr = 0;
    else
        curr += page;
    pBar->setValue(curr);
}

void Widget::on_btnClose_clicked()
{
    stackedWidget->setCurrentIndex(0);
}

#if defined(KINDLE)
void Widget::usbDriveConnected()
{
    QWSServer::instance()->suspendMouse();
    g_pLog->write(2, Log::MT_I, "usbDriveConnected()");
}
void Widget::usbDriveDisconnected()
{
    QWSServer::instance()->resumeMouse();
    g_pLog->write(2, Log::MT_I, "usbDriveDisconnected()");
    QTimer::singleShot(1000, this, SLOT(refreshScreen()));
}
void Widget::screenSaverActivated()
{
    g_pLog->write(2, Log::MT_I, "screenSaverActivated()");
    onSuspend();
}
void Widget::screenSaverDeactivated()
{
    g_pLog->write(2, Log::MT_I, "screenSaverDeactivated()");
    QTimer::singleShot(1000, this, SLOT(refreshScreen()));
    onResume();
}
#endif

void Widget::refreshScreen()
{
#if defined(Q_WS_QWS)
    QWSServer::instance()->refresh();
#endif
}

void Widget::screenShotDirect()
{
#if defined(Q_WS_QWS)
    QString fn( ScreenShotDialog::get_path() + QDir::separator() + screenShotFileName );
    QScreen* screen = qt_screen;
    g_pLog->write(0,Log::MT_I,"ScreenShot: %d x %d, linestep=%d, depth=%d, format=%d", screen->deviceWidth(), screen->deviceHeight(), screen->linestep(), screen->depth(), screen->pixelFormat());
    QImage img;
    QWSDisplay::grab(false);
    if (    QImage::Format_Indexed8 == screen->pixelFormat()
         && 8 == screen->depth()
         && screen->linestep() < 2 * screen->width() )
    {
        // special case: kindle, pocketbook
        img = QImage( screen->deviceWidth(), screen->deviceHeight(), QImage::Format_RGB32 );
        for ( int y=0; y<screen->deviceHeight(); ++y )
        {
            uchar* dest = img.scanLine(y);
            for ( int x=0; x<screen->deviceWidth(); ++x ) {
                dest[4*x+3] = 0xFF;
                dest[4*x+0] = dest[4*x+1] = dest[4*x+2] =
                        screen->base()[y*screen->linestep()+x];
            }
        }
    }
    else {
        img = QImage(screen->base(), screen->deviceWidth(), screen->deviceHeight(), screen->linestep(), screen->pixelFormat());
    }
    if (screen->isTransformed())
    {
        QMatrix matrix;
        switch (screen->transformOrientation())
        {
            case 1: matrix.rotate(90); break;
            case 2: matrix.rotate(180); break;
            case 3: matrix.rotate(270); break;
            default: break;
        }
        img = img.transformed(matrix);
    }    
    bool result = img.save(fn, "PNG");
    QWSDisplay::ungrab();
    if (result) {
        ::messageBox(this, QMessageBox::Information, tr("Done"), tr("Screen shot is ready!"));
    } else {
        ::messageBox(this, QMessageBox::Information, tr("Error"), tr("Failed to create screenshot!"));
    }
#endif
}

void Widget::screenShotRequest()
{
    ScreenShotDialog dlg(this);
    if ( dlg.exec() )
    {
        QString file(dlg.file());
        QString path(dlg.path());
        QDir d(path);
        if ( d.exists() || d.mkpath(path) )
        {
            screenShotFileName = file;
            g_pConfig->writeQString("screen_shot_path", path);
            screenShotTimer.start( dlg.delay() * 1000 );
        }
    }
}

void Widget::on_btnScreenShot_clicked()
{
    screenShotRequest();
}

void Widget::showUsbDialog()
{
    if ( NULL == usbDlg ) usbDlg = new UsbDialog(this);
    usbDlg->exec();
}

void Widget::hideUsbDialog()
{
    if ( usbDlg != NULL ) usbDlg->reject();
    ::runScript("usbnet_off.sh");
}
