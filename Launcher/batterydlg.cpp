#include "QtUtils.h"
#include "Platform.h"

#include "batterydlg.h"
#include "ui_batterydlg.h"

BatteryDlg::BatteryDlg(QWidget *parent, bool flag)
    : Dialog(parent)
    , ui(new Ui::BatteryDlg)
{
    ui->setupUi(this);
    init();
    ui->lblTitle->setText(
        QString("<b>") +
        (flag? tr("Battery is charged!"):
               tr("Battery is low!")) +
        "</b>");
    ui->widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    int size = ::mm_to_px(40);
    ui->widget->setSizeHint(QSize(size,size));
    ui->widget->load(QString(":/low.svg"));
    ui->lblLevel->setText( QString("%1%").arg( Platform::get()->getBatteryLevel() ) );
}

BatteryDlg::~BatteryDlg()
{
    delete ui;
}
