#include <QProcess>

#include "Log.h"
#include "Config.h"
#include "Platform.h"
#include "QtUtils.h"
#include "frontlight.h"

void front_light_set_level( int value, int temp )
{
    Platform::get()->frontlightSetLevel( value, temp );
    g_pLog->write(2, Log::MT_I, "front_light_set_level( %d, %d )", value, temp );
}

void front_light_startup()
{
#if defined(KOBO) || defined(QVFB)
    ::front_light_set_level( g_pConfig->readInt("frontlight_level", 0), g_pConfig->readInt("frontlight_temp", 0) );
#endif
}

void front_light_cleanup()
{
#if defined(KOBO) || defined(QVFB)
    g_pConfig->writeInt( "frontlight_level", Platform::get()->frontlightGetLevel() );
    g_pConfig->writeInt( "frontlight_temp", Platform::get()->frontlightGetTemp() );
#endif
}
