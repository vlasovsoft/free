#ifndef PROGRESSBARFILTER_H
#define PROGRESSBARFILTER_H

#include <QObject>

class ProgressBarFilter : public QObject
{
    Q_OBJECT

    int pixelsOrigin;
    int progressOrigin;
    int step;

public:
    ProgressBarFilter(QObject *parent, int s=1);

public:
    virtual bool eventFilter( QObject* obj, QEvent* e );
};

#endif // PROGRESSBARFILTER_H
