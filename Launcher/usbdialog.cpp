#include "Log.h"
#include "QtUtils.h"

#include "usbdialog.h"
#include "ui_usbdialog.h"

UsbDialog::UsbDialog(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::UsbDialog)
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);
    setFullScreen(false);
    init();
}

UsbDialog::~UsbDialog()
{
    delete ui;
}

void UsbDialog::on_btnUsbNet_clicked()
{
    ::runScript("usbnet_on.sh");
    g_pLog->write(2, Log::MT_I, "UsbNet is ON!");
    accept();
}

void UsbDialog::on_btnCharging_clicked()
{
    reject();
}
