#include <QDir>
#include <QDateTime>

#include "Config.h"
#include "Platform.h"

#include "screenshotdialog.h"
#include "ui_screenshotdialog.h"

ScreenShotDialog::ScreenShotDialog(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::ScreenShotDialog)
{
    ui->setupUi(this);
    init();

    QDateTime dt(QDateTime::currentDateTime());
    QDate d(dt.date());
    QTime t(dt.time());
    ui->leFile->setText(QString("%1%2%3%4%5%6.png").arg(d.year(),4,10,QChar('0')).arg(d.month(),2,10,QChar('0')).arg(d.day(),2,10,QChar('0')).arg(t.hour(),2,10,QChar('0')).arg(t.minute(),2,10,QChar('0')).arg(t.second(),2,10,QChar('0')));
    ui->lePath->setText(get_path());
}

ScreenShotDialog::~ScreenShotDialog()
{
    delete ui;
}

int ScreenShotDialog::delay() const
{
    return ui->cbDelay->currentText().toInt();
}

QString ScreenShotDialog::file() const
{
    return ui->leFile->text();
}

QString ScreenShotDialog::path() const
{
    return ui->lePath->text();
}

void ScreenShotDialog::accept()
{
    if ( ui->leFile->text().isEmpty() )
        ui->leFile->setFocus();
    else
    if ( ui->lePath->text().isEmpty() )
        ui->lePath->setFocus();
    else
        Dialog::accept();
}

QString ScreenShotDialog::get_path()
{
    return g_pConfig->readQString( "screen_shot_path", Platform::get()->getRootPath() + QDir::separator() + "screenshot" );
}
