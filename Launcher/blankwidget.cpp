#include "blankwidget.h"

BlankWidget::BlankWidget(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags( windowFlags() | Qt::WindowStaysOnTopHint );
    QObject::connect( &timer, SIGNAL(timeout()), this, SLOT(close()) );
    timer.setSingleShot(true);
    timer.start(1000);
    showFullScreen();
}
