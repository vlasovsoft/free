#include <QObject>

#if defined(Q_WS_QWS)
#include <QWSServer>
#endif

#include "Factory.h"
#include "Log.h"
#include "QtUtils.h"
#include "Platform.h"
#include "Config.h"
#include "LangUtils.h"
#include "Launcher.h"
#include "Application.h"
#include "LanguageDescriptor.h"
#if defined(SUSPEND_MANAGER)
#include "SuspendManager.h"
#include "keyboardfilter.h"
#endif
#include "frontlight.h"

Widget* pMainWnd = NULL;

int main(int argc, char *argv[])
{
    Application app(argc,argv);
    app.setApplicationName("launcher");
    if ( !app.init() ) return 1;

#if defined(Q_WS_QWS)
    QWSServer::setBackground(Qt::white);
#endif

    pMainWnd = new Widget(NULL);

#if defined(KOBO)
    g_pLog->write(1, Log::MT_I, "Kobo version: %s", ::str_from_file("/mnt/onboard/.kobo/version").toAscii().constData());
#endif

    int result = app.exec();

    delete pMainWnd;

#if defined(SUSPEND_MANAGER)
    SuspendManager::cleanup();
#endif

    return result;
}

