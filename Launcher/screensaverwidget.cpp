#include <time.h>

#include <QDir>
#include <QPainter>
#include <QDebug>

#include "Log.h"
#include "QtUtils.h"
#include "Config.h"
#include "Platform.h"

#if defined(SUSPEND_MANAGER)
#include "SuspendManager.h"
#endif

#include "screensaverwidget.h"

ScreenSaverWidget::ScreenSaverWidget( QWidget *parent )
    : QWidget(parent)
    , currImageNo(0)
{
    setWindowFlags( windowFlags() | Qt::WindowStaysOnTopHint );

    // initialize screensaver images
    qsrand ( time( NULL ) );
    QStringList flt; flt << "*.png" << "*.jpg";
    QStringList paths;
    QFile file( Platform::get()->getRootPath() + QDir::separator() + "sleep" + QDir::separator() + "wpp.txt" );
    if ( file.open( QFile::ReadOnly | QFile::Text ) )
    {
        QTextStream stream(&file);
        while( !stream.atEnd() )
        {
            QString str(stream.readLine());
            QFileInfo fi(str);
            if ( fi.isDir() )
                paths.append( str );
        }
    }
    if ( paths.isEmpty() )
        paths.append( Platform::get()->getRootPath() + QDir::separator() + "sleep" );

    foreach( const QString& path, paths )
    {
        QDir dir(path);
        QFileInfoList list(dir.entryInfoList( flt, QDir::Files ));
        foreach( const QFileInfo& fi, list )
        {
            images.append( fi.absoluteFilePath() );
        }
        g_pLog->write(2, Log::MT_I, "Wallpaper directory: %s",  QFile::encodeName(path).constData());
    }

    g_pLog->write(2, Log::MT_I, "Wallpapers count: %d",  images.count());

    // random shuffle
    for( int i = 0 ; i < images.count() ; ++i )
    {
        int random = qrand() % images.count();
        if ( random != i ) images[i].swap(images[random]);
    }
}

void ScreenSaverWidget::nextWallpaper()
{
    QString file;

    int type = g_pConfig->readInt( "wallpaper", 1 );
    switch ( type )
    {
    case 0:
    default:
        break; // blank wallpaper
    case 1:
        file = randomWallpaper();
        break;
    case 2:
        file = get_book_cover_path();
        if ( !QFileInfo(file).isFile() )
        {
            file = randomWallpaper();
        }
    }

    if ( !file.isEmpty() )
    {
        g_pLog->write(2, Log::MT_I, "Screensaver image: %s", QFile::encodeName(file).constData());
        img.load(file);
        int trans = Platform::get()->getOrientation();
        if ( trans != 0 && !img.isNull() )
        {
            QMatrix rm;
            rm.rotate(90*trans);
            img = img.transformed(rm);
        }
    }
}

QString ScreenSaverWidget::randomWallpaper()
{
    QString result;
    if ( !images.isEmpty() )
    {
        result = images.at( currImageNo );
        currImageNo = (currImageNo + 1) % images.size();
    }
    return result;
}

void ScreenSaverWidget::paintEvent(QPaintEvent *)
{
    if ( !img.isNull() )
    {
        QPainter p(this);
        p.drawImage( rect(), img, img.rect() );
    }
}
