#ifndef SCREENSAVERWIDGET_H
#define SCREENSAVERWIDGET_H

#include <QImage>
#include <QWidget>
#include <QProcess>
#include <QTimer>

class ScreenSaverWidget : public QWidget
{
Q_OBJECT

    QImage img;
    QProcess proc;
    QStringList images;
    int currImageNo;

public:
    ScreenSaverWidget( QWidget* parent );
    void nextWallpaper();
    QString randomWallpaper();

protected:
    virtual void paintEvent( QPaintEvent* );
};

#endif // SCREENSAVERWIDGET_H
