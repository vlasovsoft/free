#include "Log.h"
#include "Config.h"
#include "Platform.h"
#if defined(SUSPEND_MANAGER)
#include "SuspendManager.h"
#endif
#include "frontlightdlg.h"
#include "languagedlg.h"

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget *parent, const QMap<QString,QString>& apps)
    : Dialog(parent)
    , initDone(false)
    , ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    init();

    ui->cbOrientation->setCurrentIndex( Platform::get()->getOrientation() );
    ui->cbLogLevel->setCurrentIndex( g_pLog->getLevel() );

    ui->cbAutostart->addItem(tr("Off"));
    QString app(g_pConfig->readQString("autorun", "Off"));
    int index = -1;
    for ( QMap<QString,QString>::const_iterator i = apps.begin(); i != apps.end(); ++i )
    {
        ui->cbAutostart->addItem( i.key() );
        if ( app == i.key() )
        {
            index = ui->cbAutostart->count() - 1;
        }
    }

    ui->cbAutostart->setCurrentIndex( index > 0 ? index:0 );

#if defined(SUSPEND_MANAGER)
    ui->spbSleepTimeout->setValue( SuspendManager::instanse()->getSuspendTimeout() / 1000 / 60 );
    ui->chkSleepCover->setChecked( g_pConfig->readInt("sleepcover", 1) != 0 );
    ui->cbWallpaper->setCurrentIndex( g_pConfig->readInt("wallpaper", 1) );
    ui->gbSuspend->setVisible(SuspendManager::active());
#endif

#if !defined(SUSPEND_MANAGER)
    // only Kobo has own suspend support
    ui->gbSuspend->setVisible(false);
#endif

    initDone = true;
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::changeEvent(QEvent *event)
{
    Dialog::changeEvent(event);
    switch (event->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SettingsDialog::on_cbOrientation_currentIndexChanged(int index)
{
    if ( !initDone ) return;
    g_pConfig->writeInt("orientation", index);
    Platform::get()->setOrientation( index );
}

void SettingsDialog::on_cbLogLevel_currentIndexChanged(int arg1)
{
    if ( !initDone ) return;
    g_pLog->setLevel(arg1);
    g_pConfig->writeInt("log_level", arg1);
}

void SettingsDialog::on_btnFrontlight_clicked()
{
    FrontlightDlg(this).exec();
}

void SettingsDialog::on_pushButton_clicked()
{
    accept();
}

void SettingsDialog::on_cbAutostart_currentIndexChanged(const QString &arg1)
{
    if ( !initDone ) return;
    g_pConfig->writeQString("autorun", arg1);
}

#if defined(SUSPEND_MANAGER)
void SettingsDialog::on_spbSleepTimeout_valueChanged(int arg1)
{
    if ( !initDone ) return;
    g_pConfig->writeInt( "sleep_timeout", arg1 );
    SuspendManager::instanse()->setSuspendTimeout( arg1 * 1000 * 60 );
    g_pLog->write(1, Log::MT_I, "Suspend timeout: %d", arg1);
}

void SettingsDialog::on_chkSleepCover_toggled(bool checked)
{
    if ( !initDone ) return;
    g_pConfig->writeInt("sleepcover", checked? 1:0);
}

void SettingsDialog::on_cbWallpaper_currentIndexChanged(int index)
{
    if ( !initDone ) return;
    g_pConfig->writeInt("wallpaper", index);
}
#endif

void SettingsDialog::on_btnLanguage_clicked()
{
    LanguageDlg(this).exec();
}
