#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "Dialog.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public Dialog
{
    Q_OBJECT

    bool initDone;
    
public:
    SettingsDialog(QWidget *parent, const QMap<QString,QString>& apps);
    ~SettingsDialog();
    
protected:
    void changeEvent ( QEvent * event );

private slots:
    void on_cbOrientation_currentIndexChanged(int index);
    void on_cbLogLevel_currentIndexChanged(int arg1);
    void on_btnFrontlight_clicked();
    void on_pushButton_clicked();
    void on_cbAutostart_currentIndexChanged(const QString &arg1);
#if defined(SUSPEND_MANAGER)
    void on_spbSleepTimeout_valueChanged(int arg1);
    void on_chkSleepCover_toggled(bool checked);
    void on_cbWallpaper_currentIndexChanged(int index);
#endif
    void on_btnLanguage_clicked();

private:
    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
