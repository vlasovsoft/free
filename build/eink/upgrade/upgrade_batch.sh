#!/bin/sh

echo "upgrade_batch.sh started..."

dirBase="$(dirname "$ROOT")"
dirUpg=$dirBase/vlasovsoft.upgrade
dirNew=$dirBase/vlasovsoft.new

if [ -d $dirUpg/.adds/vlasovsoft ]; then
  # kobo special case
  dirUpgContents=$dirUpg/.adds/vlasovsoft
else
  # general case
  dirUpgContents=$dirUpg/vlasovsoft
fi
  
if [ -d $dirUpgContents ]; then

    # ==== remove cr3 cache

    echo "Remove cr3 cache"
    rm -rf $ROOT/cr3/data/cache

    # ==== backup current installation

    echo "Backup current installation"
    cp -r $ROOT $dirNew

    # ==== remove blacklist files and directories

    echo "Remove blacklist files"
    if [ -f $ROOT/upgrade/blacklist.txt ]; then
        for file in `cat $ROOT/upgrade/blacklist.txt`; do
            rm -rf $dirUpgContents/$file
        done
    fi

    # ==== upgrade

    echo "Upgrading"
    cp -r $dirUpgContents/* $dirNew

    # ==== remove

    echo "Cleanup"
    rm -rf $dirUpg
fi

echo "upgrade_batch.sh finished."

