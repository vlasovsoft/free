#!/bin/sh
if [ ! -e /usr/libexec/sftp-server ]; then
    mntroot rw
    mkdir -p /usr/libexec
    ln -sf $ROOT/dropbear/sftp-server /usr/libexec/sftp-server
    mntroot ro
fi
