# swipe minimum distance (mm)
export SWIPE_MIN_DISTANCE=10

# swipe minimum duration (ms)
export GESTURE_MIN_DURATION=500

# wifi inactivity timeout (min). Wifi will be automatically
# turned off if there was no activity during this time since
# last network activity
# 0 - disable inactivity timer
export WIFI_TIMEOUT=20

