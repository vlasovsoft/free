#!/bin/sh

export ROOT=`pwd`
LOG=/dev/null
# Uncomment to see log
# LOG=$ROOT/vlasovsoft.log.txt

export ROOT_BAK=$ROOT/../vlasovsoft.bak
QT=$ROOT/Qt

cd $ROOT

export LANG=en_US.UTF-8 # for launchpad
export TMPDIR=/tmp/vlasovsoft

export LD_LIBRARY_PATH=$QT/lib
export QT_PLUGIN_PATH=$QT/plugins
export QT_QWS_FONTDIR=$ROOT/fonts

export VLASOVSOFT_KEY=$ROOT/key
export VLASOVSOFT_KBD=$ROOT/kbd.txt
export VLASOVSOFT_FIFO1=$TMPDIR/fifo1
export VLASOVSOFT_FIFO2=$TMPDIR/fifo2
export VLASOVSOFT_I18N=$ROOT/i18n
export VLASOVSOFT_DICT=$ROOT/dictionary

export STYLESHEET=$ROOT/eink.qss

. $ROOT/settings.sh

mkdir -p $TMPDIR
mkfifo $VLASOVSOFT_FIFO1
mkfifo $VLASOVSOFT_FIFO2

$ROOT/launcher -qws -display transformed:qvfb:mmWidth=185:mmHeight=246 -stylesheet $STYLESHEET

rm $VLASOVSOFT_FIFO1
rm $VLASOVSOFT_FIFO2

$ROOT/upgrade_finish.sh

